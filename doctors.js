var mysql = require("mysql");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");

module.exports = {
    doctor: function(connection, req, res) {
        if (req.query.doctorid > '') {
            var query = "SELECT * FROM Users WHERE UserID=" + req.query.doctorid;
        } else if (req.query.docname > '') {
            var query = "SELECT * FROM Users WHERE Name='" + req.query.docname + "' AND DisplayID > 0";
        } else if (req.query.ramadancal > '') {
            if (req.query.location) {
                var query = "SELECT * FROM Users WHERE RamadanCal = 1 AND Location='" + req.query.location + "' AND DisplayID > 0 ORDER BY DisplayID, Name";
            } else {
                var query = "SELECT * FROM Users WHERE RamadanCal= 1 AND DisplayID > 0 ORDER BY DisplayID, Name";
            }
        } else if (req.query.location) {
            var query = "SELECT * FROM Users WHERE Location = '" + req.query.location + "' AND DisplayID > 0 ORDER BY DisplayID, Name";
        } else {
            if (req.query.drnames > '') {
                var aDoctors = req.query.drnames.split(',');
                var cDoc = "";
                for (var i=0; i < aDoctors.length; i++) {
                    cDoc += "'" + aDoctors[i] + "'";
                    if (i<aDoctors.length - 1) {
                        cDoc += ",";
                    }
                }
                var query = "SELECT * FROM Users WHERE Name IN (" + cDoc + ") AND DisplayID > 0 ORDER BY DisplayID"
            } else {
                var query = "SELECT * FROM Users WHERE DisplayID > 0 ORDER BY DisplayId, Name";
            }
        }
        this.executeQuery(connection, res, query, 'Succcess', 'Doctors');
    },

    specialty: function(connection, req, res) {
        if (req.query.location) {
            var query = "SELECT * FROM Users WHERE Specialty='" + req.query.specialty + "' AND Location='" + req.query.location + "' AND DisplayID > 0 ORDER BY DisplayID";
        } else {
            var query = "SELECT * FROM Users WHERE Specialty='" + req.query.specialty + "' AND DisplayID > 0 ORDER BY DisplayID";
        }
        this.executeQuery(connection, res, query, 'Success', 'Doctors');
    },

    specialties: function(connection, req, res) {
        var query = "SELECT Distinct(Specialty) FROM Users WHERE DisplayID > 0 AND Specialty IS NOT NULL ORDER BY Specialty";
        this.executeQuery(connection, res, query, 'Success', 'Specialty');
    },
    doctorspecialty:function(connection, req, res) {
        var query = "SELECT * FROM Users WHERE Name='" + req.query.doctorname + "'";
        this.executeQuery(connection, res, query, 'Success', 'Doctor');
    },

    doctimings: function(connection, req, res) {
        var query = "SELECT * FROM timings WHERE Name='" + req.query.doctor + "' ORDER BY timings.Weekday";
        this.executeQuery(connection, res, query, 'Success', 'Timings');
    },

    docholidays: function(connection, req, res) {
        var query = "SELECT * FROM holidays WHERE Name='" + req.query.doctor + "' OR Name='All' ORDER BY DateFrom DESC";
        this.executeQuery(connection, res, query, 'Success', 'Holidays');
    },

    deldocholiday: function(connection, req, res) {
        var query = "DELETE FROM holidays WHERE ID=" + req.query.id;
        this.executeQuery(connection, res, query, 'Holiday Deleted');
    },

    savedocholiday: function(connection, req, res) {
        var query = "INSERT INTO holidays (Name, Holiday, DateTo, DateFrom) VALUES (?,?,?,?)";
        var table = [req.body.Name, req.body.Holiday, formatter.format(new Date(req.body.DateTo)), formatter.format(new Date(req.body.DateFrom))];
        query = mysql.format(query,table);
        this.executeQuery(connection, res, query, 'Holiday Saved');
    },

    docphotoget: function(connection, req, res) {
        var query = "SELECT Photo FROM Users WHERE Name='" + req.query.name + "'";
        this.executeQuery(connection, res, query, 'Success', 'Photo');
    },

    doctimingssave: function(connection, req, res) {
        if (!req.body.ID) {
            var query = 'INSERT INTO timings (Name, Day, Weekday, Start1, End1, Start2, End2, Slot) VALUES(?,?,?,?,?,?,?,?)';
            var table = [req.body.Name, req.body.Day, req.body.Weekday, req.body.Start1, req.body.End1, req.body.Start2, req.body.End2, req.body.Slot];
        } else if(req.body.Start1) {
            var query = "UPDATE timings SET Start1=?, End1=?, Start2=?, End2=?, Slot=? WHERE ID=" + req.body.ID;
            var table = [req.body.Start1, req.body.End1, req.body.Start2, req.body.End2, req.body.Slot, req.body.Name, req.body.Day];
        } else {
            var query = "DELETE FROM timings WHERE ID=" + req.body.ID;
        }
        query = mysql.format(query,table);
        this.executeQuery(connection, res, query, 'Timings Updated');
    },

    
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function(err, rows){
            if(err) {
                res.json({"Error": true, "Message": err.sqlMessage});
            } else {
                res.json({"Error": false, "Message": message, [arrName] : rows });
            }
        });
    }
}