var mysql = require("mysql");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");

module.exports = {
    locations: function(connection, req, res) {
        var query = "SELECT * FROM locations "
        connection.query(query, function(err, rows){
            if(err) {
                res.json({"Error": true, "Message": err});
            } else {
                res.json({"Error": false, "Message": "Success", "locations": rows});
            }
        });
    } ,
    savewaitingpatients: function(connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE locations SET WaitingPatientsReminder= ? ";
            var table = [req.body.WaitingPatientsReminder]
            var message = "WaitingPatientsReminder Updated"
        } 
        query = mysql.format(query,table);
        this.executeQuery(connection, res, query, message);
    },
    saveweekoff: function(connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE locations SET Off1=?, Off2=? ";
            var table = [req.body.Off1,req.body.Off2];
            var message = "weekoff Updated"
        } 
        query = mysql.format(query,table);
        this.executeQuery(connection, res, query, message);
    },
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function(err, rows){
            if(err) {
                res.json({"Error": true, "Message": err.sqlMessage});
            } else {
                res.json({"Error": false, "Message": message, [arrName] : rows });
            }
        });
    }


    
}
