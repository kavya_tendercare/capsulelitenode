var mysql = require("mysql");
var request = require("request");
var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");
var moiurl= 'https://hie.inhealth.ae/';

module.exports = {
    getToken: function(connection, req, res) {
        request.post({
            headers: {'content-type' : 'application/x-www-form-urlencoded', 'authorization':'Basic ' + req.query.btoa},
            url:     moiurl +'cas/oidc/token',
            form:    {grant_type:'client_credentials'}
            }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                res.json(response);
            }
        });
    },

    getcontrolrx: function(connection, req, res) {
        request.get(moiurl +'blaze/MedicationRequest?subject:Patient.identifier=' + req.query.eid +'&_sort=-authoredon', {
            'auth': {
              'bearer': req.query.token
            }
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                res.json(response);
            }
        });
    },

    getMedicationDetails: function(connection, req, res) {
        request.get(moiurl +'blaze/' + req.query.reference, {
            'auth': {
              'bearer': req.query.token
            }
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                res.json(response);
            }
        });
    },

    cancelMedRequest: function(connection, req, res) {
        request.post({
            headers: {'content-type' : 'application/json', 'Authorization':'Bearer ' + req.query.token},
            url:     moiurl +'blaze/MedicationRequest/$cancel',
            body:    JSON.stringify(req.body),
            }, (error, response, body) => {
                if (error) {
                    console.log(error);
                } else {
                    res.json({ "Error": false, "CancelMedRequestResp": response });
                }
            });
    },


    getcontrolledprescripdrugs: function(connection, req, res) {
        request.get(moiurl +'blaze/MedicationRequest/' + req.query.prescID, {
            'auth': {
              'bearer': req.query.token
            }
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                res.json(response);
            }
        });
    },
    checkPatient: function(connection, req, res) {
        request.get(moiurl +'blaze/Patient?identifier=' + req.query.EmiratesID, {
            'auth': {
              'bearer': req.query.token
            }
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                res.json(response);
            }
        });
    },

    getDoctorId: function(connection, req, res) {
        request.get(moiurl +'blaze/Practitioner?identifier=' + req.query.licenseno, {
            'auth': {
              'bearer': req.query.token
            }
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                res.json(response);
            }
        });
    },

    
    getOrganisationDetails: function(connection, req, res) {
        request.get(moiurl +'blaze/Organization?identifier=' + req.query.licenseno, {
            'auth': {
              'bearer': req.query.token
            }
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                res.json(response);
            }
        });
    },
    createResource: function(connection, req, res) {
        request.post({
            headers: {'content-type' : 'application/json', 'Authorization':'Bearer ' + req.query.token},
            url:     moiurl +'blaze/Patient',
            body:    JSON.stringify(req.body),
            }, (error, response, body) => {
                if (error) {
                    console.log(error);
                } else {
                    res.json({ "Error": false, "PatientRef": response.headers.location });
                }
            });
    },

    
    getMedicineDetails: function(connection, req, res) {
        request.get(moiurl +'blaze/Medication?code=' + req.query.code, {
            'auth': {
              'bearer': req.query.token
            }
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                    res.json(response);
            }
        });     
    },

    createMedicationRequest: function(connection, req, res) {
        request.post({
            headers: {'content-type' : 'application/json', 'Authorization':'Bearer ' + req.query.token},
            url:     moiurl +'blaze/MedicationRequest',
            body:    JSON.stringify(req.body),
            }, (error, response, body) => {
                if (error) {
                    console.log(error);
                } else {
                    res.json({ "Error": false, "MedRequestResp": response });
                }
            });
    },

}