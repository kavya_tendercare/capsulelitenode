var express = require("express");
var mysql = require("mysql");
 var bodyParser = require("body-parser");

var md5 = require('MD5');
global.env = require("./environment");
// var cors = require('cors');
var rest = require("./REST");
var multer = require("multer");
var nodemailer = require("nodemailer");
var app = express();
// app.use(cors());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'POST,PUT,GET,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
var store = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads');
    },
    filename: function (req, file, cb) {
        if (file.originalname.includes('#') || file.originalname.includes('%') ||file.originalname.includes('+')) {
            var correctfile = file.originalname.replace("#", "_").replace("%", '_').replace("+", "_");
        } else {
            var correctfile = file.originalname;
         }
   
        cb(null, Date.now() + '.' + correctfile);
    }
   
})

var upload = multer({ storage: store });



var db;



var pool = mysql.createPool({
    connectionLimit: 100,
    host: 'capsule.support',
    //port: '3101',
    user: "capsule",
    password: "plexus",
    database: 'tendercare',
    debug: false,
    multipleStatements: true
  });
  pool.getConnection(function(err, connection) {
    if (err) {
      console.log(err);
    } else {
      console.log("Connected to DB");
      configureExpress1(connection);
    }
  });


  function configureExpress1(connection) {
    app.use(bodyParser.json({ limit: "50mb" }));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

  
    app.get("/liteusers", function(req, res) {
      var query = "SELECT liteusers.*, Clients.* FROM liteusers LEFT JOIN Clients ON Clients.ClientID=liteusers.ClientID WHERE liteusers.Email = '" + req.query.email +"' AND liteusers.Password = '" + req.query.password +"'";
      console.log(query)
      connection.query(query, function(err, rows) {
        if (err) {
          res.json({ Error: true, Message: "Error executing MySQL query" });
        } else {
         db = rows[0].ClientDB;
         if(db) {
            new REST();
         }

           res.json({ Error: false, Message: "Success", 'Users': rows[0] });
        }
      });
    });

}

function REST() {
    var self = this;
    self.connectMysql();
};

app.listen(3000, function() {
    console.log("All right ! I am alive at Port 3000.");
  });

REST.prototype.connectMysql = function () {
    var self = this;
    var pool = mysql.createPool({
        connectionLimit: 100,
        host: 'capsule.support',
        user: 'capsule',
        password: 'plexus',
        //port: '3101',
        database: db,
        debug: false,
        multipleStatements: true
    });
    pool.getConnection(function (err, connection) {
        if (err) {
            self.stop(err);
        } else {
            self.configureExpress(connection);
        }
    });
}

REST.prototype.configureExpress = function (connection) {
    var self = this;
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    var router = express.Router();
    app.use('/api', router);
    app.post("/upload", upload.any(), function (req, res) {
   
        return res.json({ 'originalname': req.files[0].originalname, 'uploadname': req.files[0].filename });
        
    });

    app.get('/email', function (req, res) {
        var transporter = nodemailer.createTransport({
            host: 'smtpout.europe.secureserver.net',
            port: 25,
            secure: false,
            auth: {
                user: 'sales@tendercare.com',
                pass: 'Tender584'
            }
        });
        var mailOptions = {
            from: req.query.SenderName,
            to: req.query.To,
            subject: req.query.Subject,
            text: req.query.Message,
            attachments: JSON.parse(req.query.Attachments)
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                return res.status(500).send({Success: false, Error: error});
            } else {
                return res.status(200).send({Success: true, Message: 'Email sent: ' + info.response});
            }
        });
    });

    app.post('/emailbody', function (req, res) {
        var transporter = nodemailer.createTransport({
            host: 'smtpout.europe.secureserver.net',
            port: 25,
            secure: false,
            auth: {
                user: 'sales@tendercare.com',
                pass: 'Tender584'
            }
        });
        var mailOptions = {
            from: req.query.SenderName,
            to: req.query.To,
            subject: req.query.Subject,
            html: req.body.Message,
            attachments: JSON.parse(req.query.Attachments)
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                return res.status(500).send({Success: false, Error: error});
            } else {
                return res.status(200).send({Success: true, Message: 'Email sent: ' + info.response});
            }
        });
    });
    // updateeidimage: function (connection, req, res) {
        app.post("/updateeidimage", upload.any(), function (req, res) {
        if (req.body.IDType === 'P') {
            var img = req.body.CardNumber;
        } else {
            var img = req.body.IDNumber;
        }
        var base64Datafront = req.body.IDFront.replace(/^data:image\/png;base64,/, "");
        require("fs").writeFile("./uploads/" + img + "_front.png", base64Datafront, 'base64', function (err) {
        });
        var base64Datarear = req.body.IDRear.replace(/^data:image\/png;base64,/, "");
        require("fs").writeFile("./uploads/" + img + "_rear.png", base64Datarear, 'base64', function (err) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                // res.json({ "Error": false, "Message": message});
                var query = "UPDATE patmas SET EIDImage=1 WHERE Regn_No=?";
                var table = [req.query.Regn_No];
                var message = "EID Image Updated"
                query = mysql.format(query, table);
                connection.query(query, [req.query.country], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": "Error executing MySQL query" });
                    } else {
                        res.json({ "Error": false, "Message": "EID Image Updated" });
                    }
                });
            }
        });
    });

    var rest_router = new rest(router, connection, md5);
   // self.startServer();
}

// REST.prototype.startServer = function () {
//     app.listen(3000, function () {
//         console.log("All right ! I am alive at Port 3000.");
//     });
// }

REST.prototype.stop = function (err) {
    console.log("ISSUE WITH MYSQL: " + err);
    process.exit(1);
}
  //  new REST();
