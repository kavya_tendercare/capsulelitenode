var mysql = require("mysql");
var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");

module.exports = {
    transactions: function (connection, req, res) {
        var query = "SELECT * FROM (\
            SELECT Bill_No,OPDBillDate AS DateTime,Regn_No,TransactionType, Consultant, Purpose, Discount, VAT, InsAmount, InsID, round(Amount,2) as Amount, Bill_No AS PayMode, Bill_No AS Cheque, Bill_No AS CheqDt, Bill_No AS Bank, Operator, Remarks, Cancelled,dental,round(Received,2) as Received,NULL AS CreditNote FROM OPDBill WHERE Regn_no='" + req.query.regn_no + "' \
            UNION ALL \
            SELECT Proforma_No,ProformaDate AS DateTime,Regn_No,TransactionType, Consultant, Purpose, Discount, VAT, NULL, NULL, Amount, NULL, NULL, NULL, NULL, Operator, Remarks, Cancelled,dental,NULL,NULL FROM Proforma WHERE Regn_no='" + req.query.regn_no + "' \
            UNION ALL \
            SELECT Rect_No,ReceiptsDate AS DateTime,Regn_No,TransactionType, Consultant, Purpose, DisctAmt, NULL, NULL, Amount, Amount, RectMode, Cheque, CheqDt, Bank, Operator, Remarks, Cancelled,NULL, NULL,NULL  FROM Receipts WHERE Regn_no='" + req.query.regn_no + "' \
            UNION ALL \
            SELECT VR_No,VouchersDate AS DateTime,Regn_No,TransactionType, Consultant, Partic, Amount, VAT, NULL, Amount, Amount, VrMode, Cheque, CheqDt, Bank, Operator, Remarks, Cancelled,NULL,NULL,CreditNote FROM Vouchers WHERE Regn_no='" + req.query.regn_no + "' \
            ) AS x order by DateTime, TransactionType";
        this.executeQuery(connection, res, query, 'Success', 'Transactions');
    },

    invoice: function (connection, req, res) {
        if (req.query.bill_no) {
            var query = "SELECT OPDBill.*, Patcards.Company AS InsCode, Patcards.Policy_No as Policy_No, Patcards.EDate as EDate FROM OPDBill LEFT JOIN Patcards ON OPDBill.InsID = Patcards.ID WHERE OPDBill.bill_no=" + req.query.bill_no + " and OPDBill.Regn_No=" + req.query.regn_no;
        } else {
            var query = "SELECT OPDBill.*, Patcards.Company AS InsCode, Patcards.Policy_No as Policy_No, Patcards.EDate as EDate FROM OPDBill LEFT JOIN Patcards ON OPDBill.InsID = Patcards.ID WHERE OPDBill.regn_no=" + req.query.regn_no + " ORDER BY OPDBill.Bill_No DESC";
        }
        this.executeQuery(connection, res, query, 'Success', 'Invoices');
    },

    invoicesunpaid: function (connection, req, res) {
        var query = "SELECT * FROM OPDBill WHERE OPDBill.regn_no=" + req.query.regn_no + " AND Received < (Amount - Discount) AND Cancelled=0 ORDER BY OPDBill.Bill_No";
        this.executeQuery(connection, res, query, 'Success', 'Invoices');
    },

    saveopdinvoice: function (connection, req, res, method) {
        var self = this;
        if (req.query.loc) {
           // this.getMaxLocNo('InvoiceNo', req.query.loc, connection).then(function (billno) {
            this.getMaxLocNoNew('Bill_No','OPDBill', req.query.loc, connection).then(function (billno) {
                self.savemyinvoice(connection, req, res, method, billno);
            });
        } else {
            this.getMaxNo('Bill_No', 'OPDBill', connection).then(function (billno) {
                self.savemyinvoice(connection, req, res, method, billno);
            });
        }
    },

    savemyinvoice: function (connection, req, res, method, billno) {
        var self = this;
        if (method === 'put') {
            dental = false;
            var query = "UPDATE OPDBill SET Regn_No=?, Amount=?, Consultant=?, Discount=?, InsID=?, InsAmount=?, VAT=?, Net=?, OPDBillDate=?, Purpose=?, Operator=?, Remarks=?, BonusStars=?, Imported=?, BonusGiven=?";
            if (req.body.dental > '') {
                query += ", dental=?"
                dental = true;
            }
            query += " WHERE Bill_No=?; INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + req.body.Bill_No + ", 0,'" + formatter.format(new Date()) + "','Invoice edited','" + req.body.Operator + "')";
        } else {
            dental = false;
            var query = "INSERT INTO OPDBill (Bill_No,Location, Regn_No, Amount, Consultant, Discount, InsID, InsAmount, VAT, Net, OPDBillDate, Purpose, Operator, Remarks, BonusStars, Imported, BonusGiven";
            if (req.body.dental > '') {
                query += ", dental"
                dental = true;
            }
            query += ")VALUES (" + billno + ",'"+ req.body.Location +"',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
            if (dental) {
                query += ", ?"
            }
            query += ");INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + billno + ", 0,'" + formatter.format(new Date()) + "','Invoice created','" + req.body.Operator + "')";
            if (req.body.LabID) {
                query += ";UPDATE LabTests SET InvoiceNo=" + billno + " WHERE ID=" + req.body.LabID;
            }
        }
        var table = [req.body.Regn_No, req.body.Amount, req.body.Consultant, req.body.Discount, req.body.InsID, req.body.InsAmount, req.body.VAT, req.body.Net, formatter.format(new Date(req.body.OPDBillDate)), req.body.Purpose, req.body.Operator, req.body.Remarks, req.body.BonusStars,0, req.body.BonusGiven];
        if (dental) {
            table.push(req.body.dental);
        }
        if (method === 'put') {
            table.push(req.body.Bill_No);
        }
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (method === 'put') {
                    var query2 = "UPDATE SysRect SET Bill_No=0 WHERE Bill_No=" + req.body.Bill_No + " AND Cancelled=0";
                    query2 += ";UPDATE OPDBill SET Received = 0 WHERE Bill_No=" + req.body.Bill_No;
                    connection.query(query2, function (err, result) {
                        if (err) {
                            console.log(err);
                        } else {
                            self.adjustSysRect(connection, req.body.Regn_No, req.body.Bill_No, req.body.Amount, req.body.Consultant);
                        }
                    });
                    self.saveopdinvoiceitems(connection, req.body.invoiceItem, res, req.body.Bill_No);
                    if (req.body.AppID) {
                        self.updateApptRemarks(connection, res, req.body.AppID, 1);
                    }
                } else {
                    if (req.body.AdjustSysrect === 0) {
                        self.adjustSysRect(connection, req.body.Regn_No, billno, req.body.Amount, req.body.Consultant);
                    }
                    self.saveopdinvoiceitems(connection, req.body.invoiceItem, res, billno);
                    if (req.body.AppID) {
                        self.updateApptRemarks(connection, res, req.body.AppID, 0);
                    }
                }
            }
        });
    },
    updateApptRemarks: function (connection, res, appid, edit) {
        var remarks = '';
        if (edit) {
            remarks = '\nInvoice Edited at :' + new Date().getHours() + ':' + new Date().getMinutes();
        } else {
            remarks = '\nInvoice Created at :' + new Date().getHours() + ':' + new Date().getMinutes();
        }
        var query = "UPDATE appoint SET remarks = CONCAT(remarks,'" + remarks + "') WHERE ID=?";
        var table = [appid];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
            }
        });
    },

    adjustSysRect: function (connection, regn_no, bill_no, amount, consultant, res) {
        var received = 0
        var query = "SELECT * FROM SysRect WHERE Regn_No=" + regn_no + " AND Bill_No=0 AND Cancelled=0 ORDER BY SysRect";
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                for (var i = 0; i < rows.length; i++) {
                    console.log(rows[i].Amount + '-' + amount + '-' + received);
                    if (rows[i].Amount > amount - received) {
                        var query2 = "UPDATE SysRect SET Amount=Amount - " + (amount - received) + " WHERE SysRect=" + rows[i].SysRect;
                        query2 += ";INSERT INTO sysrect (SysrectDate, Rect_No, Bill_No, Amount, Mode, Consultant, Regn_No, Cancelled) VALUES(?,?,?,?,?,?,?,0);UPDATE OPDBill SET Received=  " + amount + "  WHERE Bill_No=" + bill_no;
                        var table = [formatter.format(new Date(rows[i].SysRectDate)), rows[i].Rect_No, bill_no, amount - received, rows[i].Mode, consultant, regn_no];
                        query2 = mysql.format(query2, table);
                        connection.query(query2, function (err, result) {
                            if (err) { console.log(err); }
                        });
                        received = amount;
                    } else {
                        var query2 = "UPDATE SysRect SET Bill_No=" + bill_no + ", Consultant='" + consultant + "' WHERE SysRect=" + rows[i].SysRect + ";UPDATE OPDBill SET Received=Received + " + rows[i].Amount + "  WHERE Bill_No=" + bill_no;
                        connection.query(query2, function (err, result) {
                            if (err) { console.log(err); }
                        });
                        received += rows[i].Amount;
                        if (received >= amount) { break; }
                    }
                }
            }
        });
    },
    saveSysRectWithInvNo: function (connection, regn_no, rect_no, amount, mode, invNo, consultant, invAmount) {
        var self = this;
        var query2 = "INSERT INTO sysrect (SysrectDate, Rect_No, Bill_No, Amount, Mode, Consultant, Regn_No, Cancelled) VALUES(?,?,?,?,?,?,?,0)"
        query2 += ";UPDATE OPDBill SET Received=Received + " + amount + " WHERE Bill_No=" + invNo + "";

        var table = [formatter.format(new Date()), rect_no, invNo, amount, mode, consultant, regn_no];
        query2 = mysql.format(query2, table);
        var diff = invAmount - amount;
        if (diff > 0) {
            self.adjustSysRect(connection, regn_no, invNo, diff, consultant);
        }
        connection.query(query2, function (err, result) {
            if (err) { console.log(err); }
        });
    },

    saveSysRect: function (connection, regn_no, rect_no, amount, mode, rectdate, operator) {
        var self = this;
        var query = "SELECT * FROM OPDBill WHERE OPDBill.regn_no=" + regn_no + " AND Received < Amount AND Cancelled=0 ORDER BY OPDBill.Bill_No";
        connection.query(query, function (err, rows) {
            if (err) { console.log(err); }
            for (var i = 0; i < rows.length; i++) {
                var due = rows[i].Amount - rows[i].Received;
                if (due > amount) {
                    var sysrectamt = amount;
                } else {
                    var sysrectamt = due;
                }
                if (rows[i].BonusStars) {
                    var bonusStars = sysrectamt / due * (rows[i].BonusStars - rows[i].BonusGiven);
                } else {
                    var bonusStars = 0
                }
                var query2 = "INSERT INTO sysrect (SysrectDate, Rect_No, Bill_No, Amount, Mode, Consultant, Regn_No, Cancelled) VALUES(?,?,?,?,?,?,?,0)"
                query2 += ";UPDATE OPDBill SET Received=Received + " + sysrectamt + ", BonusGiven=BonusGiven + " + bonusStars + " WHERE Bill_No=" + rows[i].Bill_No;
                var table = [formatter.format(new Date(rectdate)), rect_no, rows[i].Bill_No, sysrectamt, mode, rows[i].Consultant, regn_no];
                query2 = mysql.format(query2, table);

                connection.query(query2, function (err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        if (bonusStars > 0) {
                            self.saveStars(rows[i].Bill_No, rows[i].Amount, rect_no, sysrectamt, bonusStars, operator);
                        }
                    }
                });
                amount -= sysrectamt;
                if (amount <= 0) { break; }
            }
            if (amount > 0) {
                var query2 = "INSERT INTO sysrect (SysrectDate, Rect_No, Bill_No, Amount, Mode, Regn_No, Cancelled) VALUES(?,?,?,?,?,?,0)";

                var table = [formatter.format(new Date(rectdate)), rect_no, 0, amount, mode, regn_no];
                query2 = mysql.format(query2, table);
                connection.query(query2, function (err, result) {
                    if (err) { console.log(err); }
                });
            }
        });
    },

    saveStars: function (invno, invamt, rectno, rectamt, bonus, operator) {
        var stars = {
            TransactionsDate: formatter.format(new Date(rectdate)),
            EstablishmentID: env.estId,
            MemberID: '',
            InvoiceNo: invno,
            InvoiceAmt: invamt,
            ReceiptNo: rectno,
            ReceiptAmt: rectamt,
            Bonus: bonus,
            Operator: operator
        }
        request.post({
            headers: { 'content-type': 'application/json' },
            url: env.mukafa,
            body: stars
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                console.log(response.body);
            }
        });
    },

    updateStars: function (connection, req, res) {
        if (req.body.BonusGiven > 0) {
            var query = "UPDATE OPDBill SET BonusGiven=BonusGiven + " + req.body.BonusGiven + " WHERE Bill_No=" + req.body.Bill_No;
            this.executeQuery(connection, res, query);
        }
    },

    invoiceitems: function (connection, req, res) {
        if (req.query.insid > 0) {
            var query = "SELECT opdcharges.*, opdcharges.VAT as vatamt, insurancerate.ItemCode,insurancerate.ActivityCode, departments.DepName FROM opdcharges LEFT JOIN insurancerate ON opdcharges.rateno=insurancerate.ID LEFT JOIN departments ON departments.DepNo=opdcharges.DepNo WHERE opdcharges.OPD_No='" + req.query.opd_no + "' ORDER BY OPDCharges.DepNo,OPDCharges.Serial"
        } else {
            var query = "SELECT opdcharges.*, ratemaster.ItemCode,ratemaster.ActivityCode,ratemaster.VAT as vatamt, departments.DepName FROM opdcharges LEFT JOIN ratemaster ON opdcharges.rateno=ratemaster.rateno LEFT JOIN departments ON departments.DepNo=opdcharges.DepNo WHERE opdcharges.OPD_No='" + req.query.opd_no + "' ORDER BY OPDCharges.DepNo,OPDCharges.Serial"
        }
        this.executeQuery(connection, res, query, 'Success', 'InvoiceItems');
    },
    receiptitems: function (connection, req, res) {
        if (req.query.regn_no > 0) {
            var query = "SELECT * from sysrect WHERE Regn_No='" + req.query.regn_no + "'and Cancelled = 0 ORDER BY SysRectDate"
        } else if (req.query.rect_no > 0) {
            var query = "SELECT * from sysrect WHERE Rect_No='" + req.query.rect_no + "' and Cancelled = 0 ORDER BY SysRectDate"
        }
        else {
            var query = "SELECT sysrect.*,receipts.Operator from sysrect JOIN receipts ON sysrect.Rect_No = receipts.rect_no WHERE sysrect.Bill_No='" + req.query.bill_no + "' and sysrect.Cancelled = 0 ORDER BY SysRectDate";
        }
        this.executeQuery(connection, res, query, 'Success', 'ReceiptItems');
    },

    saveinvoiceitems: function (connection, req, res) {
        connection.changeUser({ 'database': env.database }, function () { });
        var query = "DELETE FROM OPDCharges WHERE OPD_No=" + req.body[0].OPD_No;
        connection.query(query, function (err, result, fields) {
            if (err) {
                console.log(err);
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO OPDCharges (OPD_No, Serial, DepNo, RateNo, Description, ArabicDescription, Rate, Discount, Qty, VAT, Net, Insurance,ToothNo) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    rec.push(req.body[i].OPD_No);
                    rec.push(req.body[i].Serial);
                    rec.push(req.body[i].DepNo);
                    rec.push(req.body[i].RateNo);
                    rec.push(req.body[i].Description);
                    rec.push(req.body[i].ArabicDescription);
                    rec.push(req.body[i].Rate);
                    rec.push(req.body[i].Discount);
                    rec.push(req.body[i].Qty);
                    rec.push(req.body[i].VAT);
                    rec.push(req.body[i].Net);
                    rec.push(req.body[i].Insurance);
                    rec.push(req.body[i].ToothNo);
                    records.push(rec);
                }
                connection.changeUser({ 'database': env.database }, function () { });

                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Invoice saved', 'Items': rows });
                    }
                });
            }
        });
    },

    saveopdinvoiceitems: function (connection, req, res, opd_no) {
        var self = this;
        connection.changeUser({ 'database': env.database }, function () { });
        var query = "DELETE FROM OPDCharges WHERE OPD_No=" + opd_no;
        connection.query(query, function (err, result, fields) {
            if (err) {
                console.log(err);
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO OPDCharges (OPD_No, Serial, DepNo, RateNo, Description, ArabicDescription, Rate, Discount, Qty, VAT, Net, Insurance,ToothNo) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.length; i++) {
                    var rec = new Array();
                    rec.push(opd_no);
                    rec.push(req[i].Serial);
                    rec.push(req[i].DepNo);
                    rec.push(req[i].RateNo);
                    rec.push(req[i].Description);
                    rec.push(req[i].ArabicDescription);
                    rec.push(req[i].Rate);
                    rec.push(req[i].Discount);
                    rec.push(req[i].Qty);
                    rec.push(req[i].VAT);
                    rec.push(req[i].Net);
                    rec.push(req[i].Insurance);
                    rec.push(req[i].ToothNo);
                    records.push(rec);
                }
                connection.changeUser({ 'database': env.database }, function () { });

                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": opd_no, 'Items': rows });
                    }
                });
            }
        });
    },

    cancelinvoice: function (connection, req, res) {
        var query = "UPDATE OPDBill SET Cancelled = 1, Remarks=? WHERE Bill_No=" + req.body.Bill_No + "; UPDATE OPDCharges SET Cancelled = 1 WHERE OPD_No='" + req.body.Bill_No + "'";
        query += ";UPDATE SysRect SET Bill_No=0 WHERE Bill_No=" + req.body.Bill_No + " AND Cancelled=0;";
        if (req.body.AppID) {
            query += "UPDATE appoint SET remarks = CONCAT(remarks,'\nInvoice Cancelled at :" + new Date().getHours() + ':' + new Date().getMinutes() + "') WHERE ID=" + req.body.AppID + ";";
        }
        query += "INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + req.body.Bill_No + ", 0,'" + formatter.format(new Date()) + "','Invoice cancelled: " + req.body.Remarks + "','" + req.body.Operator + "')"
        var table = [req.body.Remarks];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Invoice cancelled');
    },

    saveinvoicetemplate: function (connection, req, res, method) {
        var self = this;
        if (method === 'put') {
            dental = false;
            var query = "UPDATE invoicetemplates SET TemplateName=?, insuranceID=?";
            if (req.body.dental > '') {
                query += ", dental=?"
                dental = true;
            }
            query += " WHERE ID=?";
        } else {
            dental = false;
            var query = "INSERT INTO invoicetemplates (TemplateName,insuranceID";
            if (req.body.dental > '') {
                query += ", dental"
                dental = true;
            }
            query += ")VALUES (?,?"
            if (dental) {
                query += ", ?"
            }
            query += ")";
        }
        var table = [req.body.TemplateName, req.body.insuranceID];
        if (dental) {
            table.push(req.body.dental);
        }
        table.push(req.body.ID);
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (method === 'post') {
                    res.json({ "Error": false, "Message": result.insertId });
                } else {
                    res.json({ "Error": false, "Message": req.body.ID });
                }
            }
        });
    },
    saveinvoicetemplateitems: function (connection, req, res) {
        var query = "DELETE FROM invoicetemplatesitems WHERE MasterID=" + req.body[0].MasterID;
        connection.query(query, function (err, result, fields) {
            if (err) {
                console.log(err);
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO invoicetemplatesitems (MasterID, Serial, RateNo, InsuranceID) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    rec.push(req.body[i].MasterID);
                    rec.push(req.body[i].Serial);
                    rec.push(req.body[i].RateNo);
                    rec.push(req.body[i].InsuranceID);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Invoice Template Saved', 'Items': rows });
                    }
                });
            }
        });
    },
    getinvoicetemplates: function (connection, req, res) {
        if (req.query.dental > '') {
            var query = "SELECT * FROM invoicetemplates where insuranceID = " + req.query.insuranceid + " and dental = " + req.query.dental;
        } else {
            var query = "SELECT * FROM invoicetemplates where insuranceID = " + req.query.insuranceid;
        }
        this.executeQuery(connection, res, query, 'Success', 'InvTemplates');
    },
    getinvoicetemplateitems: function (connection, req, res) {
        var query = "select * from invoicetemplatesitems where MasterID =? order by Serial"
        var table = [req.query.invtempid];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'InvTemplateItems');
    },
    deleteinvoicetemplates: function (connection, req, res) {
        var query = "DELETE FROM invoicetemplates WHERE ID=" + req.query.invoiceTempID + ";DELETE FROM invoicetemplatesitems WHERE MasterID=" + req.query.invoiceTempID;
        this.executeQuery(connection, res, query, 'Invoice Template Deleted');
    },

    proformaitems: function (connection, req, res) {
        var query = "SELECT proformaitems.*, ratemaster.Description FROM proformaitems LEFT JOIN ratemaster ON proformaitems.RateNo =  ratemaster.RateNo WHERE Proforma_No='" + req.query.proforma_no + "' ORDER BY Serial"
        this.executeQuery(connection, res, query, 'Success', 'ProformaItems');
    },


    saveproforma: function (connection, req, res, method) {
        var self = this;
        if (req.query.loc) {
            this.getMaxLocNoNew('Proforma_No','Proforma', req.query.loc, connection).then(function (proformano) {
                self.savemyproforma(connection, req, res, method, proformano);
            });
        } else {
            this.getMaxNo('Proforma_No', 'Proforma', connection).then(function (proformano) {
                self.savemyproforma(connection, req, res, method, proformano);
            });
        }
    },

    savemyproforma: function (connection, req, res, method, proformano) {
        if (method === 'put') {
            dental = false;
            var query = "UPDATE Proforma SET Regn_No=?, Amount=?, Consultant=?, Discount=?, VAT=?, Net=?, ProformaDate=?, Purpose=?, Operator=?,Remarks=?";
            if (req.body.dental > '') {
                query += ", dental=?"
                dental = true;
            }
            query += " WHERE Proforma_No=" + req.body.Proforma_No;
        } else {
            dental = false;
            var query = "INSERT INTO Proforma (Proforma_No ,Location, TransactionType, Regn_No, Amount, Consultant, Discount, VAT, Net, ProformaDate, Purpose, Operator,Remarks";
            if (req.body.dental > '') {
                query += ", dental"
                dental = true;
            }
            query += ")VALUES ( " + proformano + ",'"+ req.body.Location +"', 3, ?,?,?,?,?,?,?,?,?,?"
            if (dental) {
                query += ", ?"
            }
            query += ")";
        }
        var table = [req.body.Regn_No, req.body.Amount, req.body.Consultant, req.body.Discount, req.body.VAT, req.body.Net, formatter.format(new Date(req.body.ProformaDate)), req.body.Purpose, req.body.Operator, req.body.Remarks];
        if (dental) {
            table.push(req.body.dental);
        }
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (method === 'put') {
                    res.json({ "Error": false, "Message": req.body.Proforma_No });
                } else {
                    res.json({ "Error": false, "Message": proformano });
                }
            }
        });
    },

    saveproformaitems: function (connection, req, res) {
        var query = "DELETE FROM ProformaItems WHERE Proforma_No=" + req.body[0].Proforma_No;
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO ProformaItems (Proforma_No, Serial, DepNo, RateNo, Rate, Discount, Qty, VAT, Net, Validity, Cancelled,ToothNo) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    rec.push(req.body[i].Proforma_No);
                    rec.push(req.body[i].Serial);
                    rec.push(req.body[i].DepNo);
                    rec.push(req.body[i].RateNo);
                    rec.push(req.body[i].Rate);
                    rec.push(req.body[i].Discount);
                    rec.push(req.body[i].Qty);
                    rec.push(req.body[i].VAT);
                    rec.push(req.body[i].Net);
                    rec.push(formatter.format(new Date(req.body[i].Validity)));
                    rec.push(0);
                    rec.push(req.body[i].ToothNo);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Proforma Items saved', 'Items': rows });
                    }
                });
            }
        });
    },

    updateproformaitem: function (connection, req, res) {
        var query = "UPDATE ProformaItems SET Completed=1, ServiceDate=?, Consultant=?, Remarks=?, Operator=? WHERE Proforma_No=? AND Serial=?";
        var table = [formatter.format(new Date()), req.body.Consultant, req.body.Remarks, req.body.Operator, req.body.Proforma_No, req.body.Serial];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Item updated');
    },

    cancelproforma: function (connection, req, res) {
        var query = "UPDATE Proforma SET Cancelled = 1, Remarks=? WHERE Proforma_No=" + req.body.Bill_No + "; UPDATE ProformaItems SET Cancelled = 1 WHERE Proforma_No=" + req.body.Bill_No;
        var table = [req.body.Remarks];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Proforma cancelled');
    },
    saveremarksproforma: function (connection, req, res) {
        var query = "UPDATE Proforma SET  Remarks=? WHERE Proforma_No=" + req.body.Bill_No + "; UPDATE ProformaItems SET Cancelled = 1 WHERE Proforma_No=" + req.body.Bill_No;
        var table = [req.body.Remarks];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Remarks added');
    },

    receipt: function (connection, req, res) {
        var query = "SELECT * FROM receipts WHERE rect_no='" + req.query.rect_no + "'"
        this.executeQuery(connection, res, query, 'Success', 'Receipts');
    },

    savereceipt: function (connection, req, res, method) {
        var self = this;
        if (req.query.loc) {
            //this.getMaxLocNo('ReceiptNo', req.query.loc, connection).then(function (receiptno) {
            this.getMaxLocNoNew('Rect_No','receipts', req.query.loc, connection).then(function (receiptno) {
                self.savemyreceipt(connection, req, res, method, receiptno);
            });
        } else {
            this.getMaxNo('Rect_No', 'receipts', connection).then(function (receiptno) {
                self.savemyreceipt(connection, req, res, method, receiptno);
            });

        }
    },

    savemyreceipt: function (connection, req, res, method, receiptno) {
        var self = this;
        if (method === 'put') {
            var query = "UPDATE Receipts SET ReceiptsDate=?, Amount=?, RectMode=?, Cheque=?, Bank=?, Operator=?, Consultant=?, Purpose=?, Regn_No=?, Remarks=? ,AppID =?,Name =?,Imported=? WHERE Rect_No=" + req.body.Rect_No;
            query += "; INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + req.body.Rect_No + ", 1,'" + formatter.format(new Date()) + "','Receipt edited','" + req.body.Operator + "')"
        } else {
            var query = "INSERT INTO receipts (Rect_No ,Location, ReceiptsDate, Amount, RectMode, Cheque, Bank, Operator, Consultant, Purpose, Regn_No, Remarks,AppID,Name,Imported, TransactionType, Cancelled) VALUES(" + receiptno + ",'"+ req.body.Location +"',?,?,?,?,?,?,?,?,?,?,?,?,?,1,0)";
            query += "; INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + receiptno + ", 1,'" + formatter.format(new Date()) + "','Receipt created','" + req.body.Operator + "')";
        }
        var table = [formatter.format(new Date(req.body.ReceiptsDate)), req.body.Amount, req.body.RectMode, req.body.Cheque, req.body.Bank, req.body.Operator, req.body.Consultant, req.body.Purpose, req.body.Regn_No, req.body.Remarks, req.body.AppID, req.body.NewPatientName,0];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (req.body.Regn_No) {
                    if (method === 'post') {
                        if (req.body.AdjustAdvance && req.body.AdjustAdvance=== 1) {
                            self.saveSysRectWithInvNo(connection, req.body.Regn_No, receiptno, req.body.Amount, req.body.RectMode, req.query.invno, req.body.Consultant, req.body.invAmount);
                            res.json({ "Error": false, "Message": receiptno });
    
                        } else {
                            self.saveSysRect(connection, req.body.Regn_No, receiptno, req.body.Amount, req.body.RectMode, req.body.ReceiptsDate, req.body.Operator);
                            res.json({ "Error": false, "Message": receiptno });
                        }
                    } else {
                        self.cancelSysRect(connection, req.body.Rect_No, req.body.Amount, req.body.RectMode, true, req.body.ReceiptsDate);
                        res.json({ "Error": false, "Message": req.body.Rect_No });
                    }  
                } else {
                    res.json({ "Error": false, "Message":  receiptno});
                }
            }
        });
    },

    cancelreceipt: function (connection, req, res) {
        var self = this;
        var query = "UPDATE receipts SET Cancelled = 1, Remarks=? WHERE Rect_No=" + req.body.Bill_No;
        query += "; INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + req.body.Bill_No + ", 1,'" + formatter.format(new Date()) + "','Receipt cancelled: " + req.body.Remarks + "','" + req.body.Operator + "')";
        var table = [req.body.Remarks];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                self.cancelSysRect(connection, req.body.Bill_No, 0, req.body.PayMode, false);
                res.json({ "Error": false, "Message": 'Receipt cancelled' });
            }
        });
    },

    cancelSysRect(connection, rect_no, amount, mode, edit, rectdate) {
        var self = this;
        var query = "SELECT * FROM SysRect WHERE Rect_No=" + rect_no + " AND Cancelled=0";
        connection.query(query, function (err, rows) {
            if (err) {
                console.log(err);
            } else {
                for (var i = 0; i < rows.length; i++) {
                    var regn_no = rows[i].Regn_no;
                    var query2 = "UPDATE OPDBill SET Received = Received - " + rows[i].Amount + " WHERE Bill_No=" + rows[i].Bill_No;
                    query2 += ";UPDATE SysRect SET Cancelled=1 WHERE SysRect=" + rows[i].SysRect;
                    connection.query(query2, function (err) {
                        if (err) { console.log(err); }
                    });
                }
                if (edit) {
                    self.saveSysRect(connection, regn_no, rect_no, amount, mode, rectdate, '');
                }
            }
        });
    },

    voucher: function (connection, req, res) {
        var query = "SELECT * FROM vouchers WHERE VR_no=" + req.query.vrno;
        this.executeQuery(connection, res, query, 'Success', 'Vouchers');
    },


    savevoucher: function (connection, req, res, method) {
        var self = this;
        if (req.query.loc) {
            //this.getMaxLocNo('RefundNo', req.query.loc, connection).then(function (voucherno) {
            this.getMaxLocNoNew('VR_No','Vouchers', req.query.loc, connection).then(function (voucherno) {
                self.savemyvoucher(connection, req, res, method, voucherno);
            });
        } else {
            this.getMaxNo('VR_No', 'Vouchers', connection).then(function (voucherno) {
                self.savemyvoucher(connection, req, res, method, voucherno);
            });
        }
    },

    savemyvoucher: function (connection, req, res, method, voucherno) {
        var message;
        if (method === 'put') {
            var query = "UPDATE Vouchers SET VouchersDate=?, Amount=?, VrMode=?, Cheque=?, Bank=?, Operator=?, Consultant=?, Partic=?, Regn_No=?, Remarks=?,CreditNote=?,Bill_No=?,VAT =?, Imported=? WHERE VR_No= " + req.body.VR_No;
            if (req.body.CreditNote) {
                message = "Credit Note Updated"
            } else {
                message = "Refund Updated"
            }
            query += "; INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + req.body.VR_No + ", 2,'" + formatter.format(new Date()) + "','Voucher edited','" + req.body.Operator + "')"
        } else {
            var query = "INSERT INTO Vouchers (VR_No,Location,VouchersDate, Amount, VrMode, Cheque, Bank, Operator, Consultant, Partic, Regn_No, Remarks, CreditNote,Bill_No,VAT,Imported,TransactionType, Cancelled) VALUES(" + voucherno + ",'"+ req.body.Location +"',?,?,?,?,?,?,?,?,?,?,?,?,?,?,2,0)";
            if (req.body.CreditNote) {
                message = "Credit Note Saved"
            } else {
                message = "Refund Saved"
            }
            query += "; INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + voucherno + ", 2,'" + formatter.format(new Date()) + "','Voucher created','" + req.body.Operator + "')"
        }
        var table = [formatter.format(new Date(req.body.VouchersDate)), req.body.Amount, req.body.VrMode, req.body.Cheque, req.body.Bank, req.body.Operator, req.body.Consultant, req.body.Partic, req.body.Regn_No, req.body.Remarks, req.body.CreditNote, req.body.Bill_No,req.body.VAT,0];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    cancelvoucher: function (connection, req, res) {
        var query = "UPDATE vouchers SET Cancelled = 1, Remarks=? WHERE VR_No=" + req.body.Bill_No;
        query += "; INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (" + req.body.Bill_No + ", 2,'" + formatter.format(new Date()) + "','Voucher cancelled: " + req.body.Remarks + "','" + req.body.Operator + "')";
        var table = [req.body.Remarks];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Voucher cancelled');
    },

    creditnodeinv: function (connection, req, res) {
        var query = "SELECT * FROM vouchers WHERE cancelled=0  and CreditNote =1 and Bill_No=" + req.query.invno;
        this.executeQuery(connection, res, query, 'Success', 'CreditNote');
    },
    accountslog: function (connection, req, res) {
        var query = "SELECT * FROM AccountsLog WHERE DocumentNo=? AND DocumentType=? ORDER BY AccountsLogDate";
        var table = [req.query.docno, req.query.doctype];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'AccountsLog');
    },
    saveaccountslog: function (connection, req, res, method) {
        var query = "INSERT INTO AccountsLog (DocumentNo, DocumentType, AccountsLogDate, Remarks, Operator) VALUES (?,?,?,?,?)";
        var table = [req.body.DocumentNo, req.body.DocumentType, formatter.format(new Date()), req.body.Remarks, req.body.Operator]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Accounts Log Added');
    },

    paymodes: function (connection, req, res) {
        var query = "SELECT * FROM paymodes ORDER BY Description"
        this.executeQuery(connection, res, query, 'Success', 'PayModes');
    },

    savepaymode: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE paymodes SET Description=?,Charge=? WHERE ID=" + req.body.ID;
            var message = "Paymode Updated"
        } else {
            var query = "INSERT INTO paymodes (Description, Charge) VALUES(?,?)";
            var message = "Paymode Added"
        }
        var table = [req.body.Description, req.body.Charge]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    delpaymode: function (connection, req, res) {
        var query = "DELETE FROM paymodes WHERE id=" + req.query.id;
        this.executeQuery(connection, res, query, 'Paymode Deleted');
    },

    rates: function (connection, req, res) {
        if (req.query.search) {
            if (+req.query.insurance > 0) {
                if (req.query.search.length > 3) {
                    var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceRate.Active =1 and InsuranceID='" + req.query.insurance + "' and (Description LIKE '%" + req.query.search + "%' OR ActivityCode LIKE '%" + req.query.search + "%' OR ItemCode LIKE '%" + req.query.search + "%') ORDER BY Description LIMIT 200";
                } else {
                    var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceRate.Active =1 and InsuranceID='" + req.query.insurance + "' and (ActivityCode = '" + req.query.search + "' or Description = '" + req.query.search + "') ORDER BY Description LIMIT 200";
                }
            } else {
                if (req.query.search.length > 3) {
                    var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE (Description LIKE '%" + req.query.search + "%' OR ActivityCode LIKE '%" + req.query.search + "%' OR ItemCode LIKE '%" + req.query.search + "%') AND Ratemaster.Active = 1 ORDER BY Description LIMIT 200";
                } else {
                    var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE  (ActivityCode = '" + req.query.search + "' or Description = '" + req.query.search + "') AND Ratemaster.Active = 1 ORDER BY Description LIMIT 200";
                }
            }
        } else {
            if (req.query.insurance > 0) {
                var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo where InsuranceRate.InsuranceID='" + req.query.insurance + "' ORDER BY Description LIMIT 200";
            } else {
                var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo ORDER BY Description LIMIT 200";

            }
        }
        this.executeQuery(connection, res, query, 'Success', 'Rates');
    },
    ratesonrateno: function (connection, req, res) {
        if (+req.query.insuranceid > 0) {
            var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceID='" + req.query.insuranceid + "' and ID = '" + req.query.rateno + "'";
        } else {
            var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE Ratemaster.Active=1 and  RateNo = '" + req.query.rateno + "'";
        }
        this.executeQuery(connection, res, query, 'Success', 'RatesonRateno');
    },
    labrates: function (connection, req, res) {
        if (req.query.search) {
            if (req.query.insurance > 0) {
                if (req.query.search.length > 3) {
                    var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceRate.Active =1 and InsuranceID='" + req.query.insurance + "' and activitycode >= 80000 and activitycode < 90000 and (Description LIKE '%" + req.query.search + "%' OR ActivityCode LIKE '%" + req.query.search + "%' OR ItemCode LIKE '%" + req.query.search + "%') ORDER BY Description LIMIT 200";
                } else {
                    var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceRate.Active =1 and InsuranceID='" + req.query.insurance + "' and activitycode >= 80000 and activitycode < 90000 and (ActivityCode = '" + req.query.search + "' or Description = '" + req.query.search + "') ORDER BY Description LIMIT 200";
                }
            } else {
                if (req.query.search.length > 2) {
                    var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE (Description LIKE '%" + req.query.search + "%' OR ActivityCode LIKE '%" + req.query.search + "%') AND Ratemaster.Active = 1 and activitycode >= 80000 and activitycode < 90000 ORDER BY Description LIMIT 200";
                } else {
                    var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE  (ActivityCode = '" + req.query.search + "' or Description = '" + req.query.search + "') AND Ratemaster.Active = 1  and activitycode >= 80000 and activitycode < 90000 ORDER BY Description LIMIT 200";
                }
            }
        }
        this.executeQuery(connection, res, query, 'Success', 'Rates');
    },

    dentalrates: function (connection, req, res) {
        if (req.query.search) {
            if (+req.query.insurance > 0) {
                if (req.query.search.length > 3) {
                    var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceRate.Active =1 and InsuranceID='" + req.query.insurance + "' and activitytype = 6 and (Description LIKE '%" + req.query.search + "%' OR ActivityCode LIKE '%" + req.query.search + "%' OR ItemCode LIKE '%" + req.query.search + "%') ORDER BY Description LIMIT 200";
                } else {
                    var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceRate.Active =1 and InsuranceID='" + req.query.insurance + "' and activitytype = 6 and (ActivityCode = '" + req.query.search + "' or Description = '" + req.query.search + "') ORDER BY Description LIMIT 200";
                }
            } else {
                if (req.query.search.length > 3) {
                    var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE (Description LIKE '%" + req.query.search + "%' OR ActivityCode LIKE '%" + req.query.search + "%' OR ItemCode LIKE '%" + req.query.search + "%') AND Ratemaster.Active = 1 and activitytype = 6 ORDER BY Description LIMIT 200";
                } else {
                    var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE  (ActivityCode = '" + req.query.search + "' or Description = '" + req.query.search + "') AND Ratemaster.Active = 1  and activitytype = 6 ORDER BY Description LIMIT 200";
                }
            }
        } else {
            if (req.query.insurance > 0) {
                var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo where InsuranceRate.InsuranceID='" + req.query.insurance + "' ORDER BY Description LIMIT 200";
            } else {
                var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo ORDER BY Description LIMIT 200";
            }
        }
        this.executeQuery(connection, res, query, 'Success', 'Rates');
    },
    todaysdentalitems: function (connection, req, res) {
        if (req.query.isProforma === 'true') {
            var query = "select ActivityCode,ToothNo from dentalchart  where  date(DentalChartDate) = curdate() and DentalStatus =2 and Regn_No='" + req.query.regn_no + "'";
        } else {
            var query = "select ActivityCode,ToothNo from dentalchart  where  date(DentalChartDate) = curdate() and DentalStatus =1 and Regn_No='" + req.query.regn_no + "'";
        }
        this.executeQuery(connection, res, query, 'Success', 'ActivityCodes');
    },
    deprates: function (connection, req, res) {
        if (req.query.insurance > '0') {
            var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceID=" + req.query.insurance + " AND Departments.DepName='" + req.query.depname + "' ORDER BY Description";
        } else {
            var query = "SELECT Ratemaster.*, Departments.DepName FROM Ratemaster LEFT JOIN Departments ON Ratemaster.DepNo = Departments.DepNo WHERE Departments.DepName='" + req.query.depname + "' ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'Rates');
    },
    insrate: function (connection, req, res) {
        var query = "SELECT InsuranceRate.*, Departments.DepName FROM InsuranceRate LEFT JOIN Departments ON InsuranceRate.DepNo = Departments.DepNo WHERE InsuranceID ='" + req.query.insname + "'  ORDER BY Description";
        this.executeQuery(connection, res, query, 'Success', 'InsuranceRates');
    },

    patcards: function (connection, req, res) {
        if (req.query.cardID) {
            var query = "SELECT patcards.*, OPDBill.Bill_No FROM patcards LEFT JOIN OPDBill ON patcards.ID = OPDBill.INSID WHERE patcards.ID='" + req.query.cardID + "'";
        } else {
            var query = "SELECT patcards.*, OPDBill.Bill_No FROM patcards LEFT JOIN OPDBill ON patcards.ID = OPDBill.INSID WHERE patcards.regn_no='" + req.query.regn_no + "'  group by patcards.policy_no, patcards.edate ORDER BY EDate DESC";
        }
        this.executeQuery(connection, res, query, 'Success', 'PatCards');
    },
    patcardsactive: function (connection, req, res) {
        var query = "SELECT patcards.ID,patcards.Active,patcards.InsuranceID,patcards.Regn_No,patcards.Scheme,patcards.Policy_No,patcards.EDate,patcards.Company,patcards.CompanyID,patcards.PayerName,patcards.PayerID,opdbill.Bill_No FROM patcards LEFT JOIN OPDBill ON patcards.ID = OPDBill.INSID WHERE patcards.regn_no='" + req.query.regn_no + "' and patcards.Active =1 and patcards.edate >= CURDATE() group by patcards.policy_no, patcards.edate ORDER BY EDate DESC";
        this.executeQuery(connection, res, query, 'Success', 'patcardsactive');
    },

    patcardsactiveexp: function (connection, req, res) {
        var query = "SELECT Regn_No, EDate FROM patcards  WHERE patcards.regn_no='" + req.query.regn_no + "' and patcards.Active =1 ORDER BY EDate desc limit 1 ";
        this.executeQuery(connection, res, query, 'Success', 'patcardsactiveexp');
    },
    patcardsave: function (connection, req, res, method) {
        var self = this;
        if (method === 'put') {
            var query = "UPDATE patcards SET Active=?, Company=?, CompanyID=?, InsuranceID=?, PayerName=?, PayerID=?, Regn_No=?, Scheme=?, Policy_No=?, EDate=?, Deductible=?, DeductType=?,DeductOn=?,DeductApply=?, CoInsurance=?, CIType=?, CIApply=?, CopayOn=?, Margin=?, CopayDental=?, CopayDentalType=?, CopayMaternity=?, CopayMaternityType=?, Remarks=? WHERE ID=" + req.body.ID;
        } else {
            var query = "INSERT INTO patcards (Active, Company, CompanyID, InsuranceID, PayerName, PayerID, Regn_No, Scheme, Policy_No, EDate, Deductible, DeductType,DeductOn,DeductApply, CoInsurance, CIType, CIApply, CopayOn, Margin, CopayDental, CopayDentalType, CopayMaternity, CopayMaternityType, Remarks) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Active, req.body.Company, req.body.CompanyID, req.body.InsuranceID, req.body.PayerName, req.body.PayerID, req.body.Regn_No, req.body.Scheme, req.body.Policy_No, formatter.format(new Date(req.body.EDate)), req.body.Deductible, req.body.DeductType, req.body.DeductOn, req.body.DeductApply, req.body.CoInsurance, req.body.CIType, req.body.CIApply, req.body.CopayOn, req.body.Margin, req.body.CopayDental, req.body.CopayDentalType, req.body.CopayMaternity, req.body.CopayMaternityType, req.body.Remarks, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (method === 'put') {
                    self.patcarddepsave(connection, req.body.patdepins, res, req.body.ID);
                } else {
                    self.patcarddepsave(connection, req.body.patdepins, res, result.insertId);
                }
            }
        });
        //this.executeQuery(connection, res, query, 'Card saved');
    },

    patcarddepsave(connection, req, res, patcardsid) {
        var query = "DELETE FROM patdepinsurance WHERE PatCardsID=" + patcardsid;
        connection.query(query, function (err, result, fields) {
            if (err) {
                console.log(err);
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO patdepinsurance (PatCardsID,DepNo, Copay, CopayType, CIApply, Margin) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.length; i++) {
                    var rec = new Array();
                    rec.push(patcardsid);
                    rec.push(req[i].DepNo);
                    rec.push(req[i].Copay);
                    rec.push(req[i].CopayType);
                    rec.push(req[i].CIApply);
                    rec.push(req[i].Margin);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Patcards saved' });
                    }
                });
            }
        });
    },
    patcardactivate: function (connection, req, res, method) {
        var message;
        if (req.body.Active === 1) {
            message = 'Card Activated'
        } else {
            message = 'Card Inactivated'
        }
        var query = "UPDATE patcards SET Active=? WHERE ID=" + req.body.ID
        var table = [req.body.Active, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },


    patcarddelete: function (connection, req, res) {
        var query = "DELETE FROM patcards WHERE ID=" + req.query.id;
        this.executeQuery(connection, res, query, 'Card deleted');
    },

    companies: function (connection, req, res) {
        if (req.query.select === 'payers') {
            var query = "SELECT * FROM insurance WHERE HAADID LIKE 'INS%' or HAADID LIKE 'SP%' or HAADID like 'SF%' or HAADID = 'Company' or HAADID LIKE 'A%' or HAADID LIKE 'D%' or HAADID LIKE 'E%' ORDER BY FullName";
        } else {
            if (req.query.search) {
                var query = "SELECT * FROM insurance WHERE FullName LIKE '%" + req.query.search + "%' ORDER BY FullName";
            } else {
                var query = "SELECT * FROM insurance ORDER BY FullName";
            }
        }
        this.executeQuery(connection, res, query, 'Success', 'Companies');
    },
    insurancedetails: function (connection, req, res) {
        var query = "SELECT insurance.* FROM insurance JOIN patcards on insurance.ID=patcards.InsuranceID WHERE patcards.ID=" + req.query.insid;
        this.executeQuery(connection, res, query, 'Success', 'Insurance');
    },

    dept: function (connection, req, res) {
        var query = "SELECT DepNo, DepName FROM departments ORDER BY DepName"
        this.executeQuery(connection, res, query, 'Success', 'Dept');
    },
    deptactive: function (connection, req, res) {
        if (req.query.depno) {
            var query = "SELECT DepNo, DepName FROM departments where active =1 and DepNo = " + req.query.depno + " ORDER BY depno"
        } else {
            var query = "SELECT DepNo, DepName FROM departments where active =1 ORDER BY depno"
        }
        this.executeQuery(connection, res, query, 'Success', 'DeptActive');
    },

    head: function (connection, req, res) {
        var query = "SELECT CategoryNo, CategoryNm FROM head ORDER BY CategoryNm"
        this.executeQuery(connection, res, query, 'Success', 'Head');
    },

    saverate: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO Ratemaster (Description, ArabicDescription, DepNo, Category,CategoryNo, Rate, VAT, Ac_Code, ItemCode, ActivityCode, ActivityType, Maternity, Lab, Radiology, Active,Remarks,PurchaseRate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            var message = "Rate added";
        } else {
            var query = "UPDATE Ratemaster SET Description=?, ArabicDescription=?, DepNo=?,Category=?, CategoryNo=?, Rate=?, VAT=?, Ac_Code=?, ItemCode=?, ActivityCode=?, ActivityType=?, Maternity=?, Lab=?, Radiology=?, Active=?,Remarks=?, PurchaseRate=? WHERE RateNo=?"
            var message = "Rate updated";
        }
        var table = [req.body.Description, req.body.ArabicDescription, req.body.DepNo, req.body.Category, req.body.CategoryNo, req.body.Rate, req.body.VAT, req.body.Ac_Code, req.body.ItemCode, req.body.ActivityCode, req.body.ActivityType, req.body.Maternity, req.body.Lab, req.body.Radiology, req.body.Active, req.body.Remarks, req.body.PurchaseRate, req.body.RateNo];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    saveinsrate: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO insurancerate (Description, ArabicDescription, DepNo, Category,CategoryNo, Rate, VAT, Ac_Code, ItemCode, ActivityCode, ActivityType, Maternity, Lab, Radiology, Active,Remarks,PurchaseRate,InsuranceID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            var message = "Insurance Rate added";
        } else {
            var query = "UPDATE insurancerate SET Description=?, ArabicDescription=?, DepNo=?,Category=?, CategoryNo=?, Rate=?, VAT=?, Ac_Code=?, ItemCode=?, ActivityCode=?, ActivityType=?, Maternity=?, Lab=?, Radiology=?, Active=?,Remarks=?, PurchaseRate=?, InsuranceID=? WHERE ID=?"
            var message = "Insurance Rate updated";
        }
        var table = [req.body.Description, req.body.ArabicDescription, req.body.DepNo, req.body.Category, req.body.CategoryNo, req.body.Rate, req.body.VAT, req.body.Ac_Code, req.body.ItemCode, req.body.ActivityCode, req.body.ActivityType, req.body.Maternity, req.body.Lab, req.body.Radiology, req.body.Active, req.body.Remarks, req.body.PurchaseRate, req.body.InsuranceID, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    delrate: function (connection, req, res) {
        if (req.query.ID > '0') {
            var query = "DELETE FROM insurancerate WHERE ID=" + req.query.ID;
        } else {
            var query = "DELETE FROM Ratemaster WHERE RateNo=" + req.query.rateno;
        }
        this.executeQuery(connection, res, query, 'Rate deleted');
    },

    savehead: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO Head (CategoryNm) VALUES (?)";
            var message = "Category added";
        } else {
            var query = "UPDATE Head SET CategoryNm=? WHERE CategoryNo=?";
            var message = "Category updated";
        }
        var table = [req.body.CategoryNm, req.body.CategoryNo]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    delhead: function (connection, req, res) {
        var query = "DELETE FROM Head WHERE CategoryNo=" + req.query.categoryno;
        this.executeQuery(connection, res, query, 'Category deleted');
    },
    doctorshares: function (connection, req, res) {
        var query = "SELECT * FROM consultants order by Consultant";
        this.executeQuery(connection, res, query, 'Success', 'DoctorShare');

    },

    itemname: function (connection, req, res) {
        var query = "SELECT * FROM ratemaster where rateno='" + req.query.rateno + "'";
        this.executeQuery(connection, res, query, 'Success', 'Ratemaster');
    },

    deptname: function (connection, req, res) {
        var query = "SELECT * FROM departments where depno='" + req.query.deptno + "'";
        this.executeQuery(connection, res, query, 'Success', 'Department');
    },

    savedoctorshare: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE consultants SET Consultant=?, RateNo=?,DepNo=?,OPD=?,OPD2=? WHERE ID='" + req.body.ID + "'";
            var message = "Doctor Share Updated"
        } else {
            var query = "INSERT INTO consultants (Consultant, RateNo,DepNo, OPD, OPD2) VALUES (?,?,?,?,?)";
            var message = "Doctor Share Added"
        }
        var table = [req.body.Consultant, req.body.RateNo, req.body.DepNo, req.body.OPD, req.body.OPD2]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    doctorsharedel: function (connection, req, res) {
        var query = "DELETE FROM consultants WHERE ID=" + req.query.ID;
        this.executeQuery(connection, res, query, 'Doctor Share Deleted');
    },
    stockitem: function (connection, req, res) {
        var query = "SELECT * FROM stockitems where Cancelled='0' and  RateNo='" + req.query.RateNo + "'";
        this.executeQuery(connection, res, query, 'Success', 'Stockitem');
    },
    stockitemsave: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE stockitems SET RateNo=?, StockItemsDate=?, ReceiveIssue=?, Description=?, Batch=? , Qty=?, Rate=?, Remarks=?, Operator=? "
            if (req.body.Expiry > '') {
                query += ", Expiry =?"
            }
            query += " , Units=?, Cancelled=? WHERE ID=" + req.body.ID;
        } else {
            var query = "INSERT INTO stockitems (RateNo, StockItemsDate, ReceiveIssue, Description, Batch, Qty, Rate, Remarks, Operator"
            if (req.body.Expiry > '') {
                query += ", Expiry "
            }
            query += "  ,Units, Cancelled) VALUES (?,?,?,?,?,?,?,?,?,?,?"
            if (req.body.Expiry > '') {
                query += ", ?"
            }
            query += ")";
        }
        var table = [req.body.RateNo, formatter.format(new Date(req.body.StockItemsDate)), req.body.ReceiveIssue, req.body.Description, req.body.Batch, req.body.Qty, req.body.Rate, req.body.Remarks, req.body.Operator];
        if (req.body.Expiry > '') {
            table.push(formatter.format(new Date(req.body.Expiry)));
        }
        table.push(req.body.Units, req.body.Cancelled, req.body.ID);

        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Stock Item saved');
    },
    cancelstock: function (connection, req, res, method) {
        var query = "UPDATE stockitems SET Cancelled='1' WHERE ID=" + req.body.ID;
        this.executeQuery(connection, res, query, 'Stock Item deleted');
    },
    inslist: function (connection, req, res) {
        var query = "select distinct insurancerate.InsuranceID,insurance.FullName from insurancerate LEFT JOIN insurance ON insurance.ID = insurancerate.InsuranceID ";
        this.executeQuery(connection, res, query, 'Success', 'insurancelist');
    },
    savesoapins: function (connection, req, res, method) {
        var lApprovedDate = false;
        var lExpiryDate = false;
        if (method === 'post') {
            var query = "INSERT INTO soapins (Regn_No, CPTCode, ReqSentdate, AuthCode, Description, Operator, CurrentDate, Remarks "
            if (req.body.ApprovedDate > '') {
                query += ", ApprovedDate"
                lApprovedDate = true;
            }
            if (req.body.ExpiryDate > '') {
                query += ", ExpiryDate"
                lExpiryDate = true;
            }
            query += ")VALUES (?,?,?,?,?,?,?,?"
            if (lApprovedDate) {
                query += ", ?"
            }
            if (lExpiryDate) {
                query += ", ?"
            }
            query += ")";
            var message = "Insurance Rate added";
        } else {
            var query = "UPDATE soapins SET Regn_No=?, CPTCode=?, ReqSentdate=?, AuthCode=?, Description=?, Operator=?, CurrentDate=?, Remarks=?"
            if (req.body.ApprovedDate > '') {
                query += ", ApprovedDate=?"
                lApprovedDate = true;
            }
            if (req.body.ExpiryDate > '') {
                query += ", ExpiryDate=?"
                lExpiryDate = true;
            }
            query += " WHERE ID=?";
            var message = "Insurance Rate updated";
        }
        var table = [req.body.Regn_No, req.body.CPTCode, formatter.format(new Date(req.body.ReqSentdate)), req.body.AuthCode, req.body.Description, req.body.Operator, formatter.format(new Date(req.body.CurrentDate)), req.body.Remarks];
        if (lApprovedDate) {
            table.push(formatter.format(new Date(req.body.ApprovedDate)));
        }
        if (lExpiryDate) {
            table.push(formatter.format(new Date(req.body.ExpiryDate)));
        }
        table.push(req.body.ID);
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    soapinsdelete: function (connection, req, res) {
        var query = "DELETE FROM soapins WHERE ID=" + req.query.ID;
        this.executeQuery(connection, res, query, 'Insurance Approval Details deleted');
    },
    getMaxNo: async function (field, table, conn) {
        var query = "SELECT MAX(" + field + ") + 1 AS ID FROM " + table;
        let promise = new Promise((resolve, reject) => {
            conn.query(query, function (err, result, _max) {
                if (err) { reject(err); }
                resolve(result[0].ID);
            });
        });
        return await promise;
    },
    getMaxLocNoNew: async function (field, table,loc, conn) {
        var query = "SELECT MAX(" + field + ") + 1 AS ID FROM " + table + " WHERE location= '" + loc + "'";
        let promise = new Promise((resolve, reject) => {
            conn.query(query, function (err, result, _max) {
                if (err) { reject(err); }
                resolve(result[0].ID);
            });
        });
        return await promise;
    },
    getMaxLocNo: async function (field, loc, conn) {
        var query = "SELECT " + field + " as ID FROM Locations WHERE LocationName= '" + loc + "'";
        let promise = new Promise((resolve, reject) => {
            conn.query(query, function (err, result, _max) {
                if (err) { reject(err); }
                resolve(result[0].ID);
               var query2 = "UPDATE Locations SET " + field + " = " + (Number(result[0].ID) + 1) + " where LocationName= '" + loc + "'";
                conn.query(query2, function (err, rows) {
                    if (err) {
                        console.log(err.sqlMessage);
                    } else {
                        console.log('Location Updated');
                    }
                });
            });
        });
        return await promise;
    },
    patdepinsurance: function (connection, req, res) {
        var query = "SELECT *  FROM patdepinsurance WHERE PatCardsID= " + req.query.cardID + " order by depno";
        this.executeQuery(connection, res, query, 'Success', 'PatDepInsurance');
    },
    appointreceipts:  function (connection, req, res) {
        var query = "SELECT *  FROM receipts WHERE appid= " + req.query.appid + " order by Rect_No";
        this.executeQuery(connection, res, query, 'Success', 'ApptReceipts');
    },
    getPreviousSales: function (connection, req, res) {
        var query = "SELECT * FROM previoussales WHERE regn_no=" + req.query.regn_no;
        console.log(query)
        this.executeQuery(connection, res, query, 'Success', 'Previoussales');
    },
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.changeUser({ 'database': env.database }, function () { });
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }

}
