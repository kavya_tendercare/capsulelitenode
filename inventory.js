var mysql = require("mysql");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");

module.exports = {
    issuemst: function (connection, req, res) {
        var query = "SELECT * FROM IssueMst WHERE DocNo=" + req.query.docno;
        this.executeQuery(connection, res, query, 'Success', 'IssueMst');
    },

    addissuemst: function (connection, req, res) {
        var query = "INSERT INTO IssueMst (IssueMstDate, DepFrom, DepTo, DocNo, Oper) VALUES (?,?,?,?,?)";
        var table = [formatter.format(new Date()), req.body.DepFrom, req.body.DepTo, req.body.DocNo, req.body.Oper];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'IssueMst added');
    },

    issuetrn: function (connection, req, res) {
        var query = "INSERT INTO IssueTrn (DocNo, ItemCode, Qty, Rate) VALUES (?,?,?,?); UPDATE Dep_Item SET QOH=QOH-" + req.body.Qty + ", Qty_Iss=Qty_Iss+" + req.body.Qty + " WHERE ItemCode='" + req.body.ItemCode + "' AND DepName='" + req.body.Department + "'";
        var table = [req.body.DocNo, req.body.ItemCode, req.body.Qty, req.body.Rate];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'IssueTrn added');
    },

    delissuetrn: function (connection, req, res) {
        var query = "SELECT * FROM IssueTrn WHERE DocNo=" + req.query.docno;
        connection.changeUser({ 'database': env.inventory }, function () { });
        connection.query(query, function (err, rows) {
            if (err) {
                throw err;
            } else {
                for (var i = 0; i < rows.length; i++) {
                    var depquery = "UPDATE Dep_Item SET QOH=QOH+" + rows[i].Qty + ", Qty_Iss=Qty_Iss-" + rows[i].Qty + " WHERE ItemCode='" + rows[i].ItemCode + "' AND DepName='" + req.query.dept + "'";
                    connection.query(depquery, function (err, rows) {
                        if (err) { throw err; }
                    });
                }
                var delquery = "DELETE FROM IssueTrn WHERE DocNo=" + req.query.docno;
                connection.query(delquery, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Deleted', 'Rows': rows });
                    }
                });
            }
        });
    },
    getitemcodewithbarcode: function (connection, req, res) {        
        var query = "SELECT ItemCode FROM Itemmast WHERE Barcode='" + req.query.barcode + "'";
        this.executeQuery(connection, res, query, 'Success', 'Itemcode');
    },

    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.changeUser({ 'database': env.inventory }, function () { });
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }


} // module.exports
