var mysql = require("mysql");
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");

module.exports = {
    users: function (connection, req, res) {
        var query = "SELECT * FROM ?? order by usergroup,displayid,name";
        var table = ["users"];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Users');
    },
    validateuser: function (connection, req, res) {
        var query = "SELECT * FROM Users WHERE Email = '" + req.query.email + "'";
        console.log('hhhh')
        console.log(req.query.email)
        console.log(query)
        this.executeQuery(connection, res, query, 'Success', 'Users');
    },
    chatusers: function (connection, req, res) {
        var query = "SELECT * FROM users WHERE Expiry >= ? order by name";
        var table = [formatter.format(new Date())];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Users');
    },

    usergroups: function (connection, req, res) {
        var query = "SELECT * FROM usergroups ORDER BY GroupName";
        this.executeQuery(connection, res, query, 'Success', 'Usergroups');
    },
    user: function (connection, req, res) {
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=?";
        var table = ["users", "username", req.query.username, "password", req.query.password];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Users');
    },

    updateLogins: function (connection, req, res) {
        var query = "INSERT INTO LOGINS (LoginsDate,Username,Password,IPAddress,LoginSuccess) VALUES(?,?,?,?,?)";
        var table = [formatter.format(new Date()),req.body.username, req.body.password, req.body.ip,req.body.status];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Login saved');
    },

    usersave: function (connection, req, res, method) {
        var token;
        if (req.body.Username && req.body.Password) {
            var hashedPassword = bcrypt.hashSync(req.body.Password);
            var user = { username: req.body.Username, password: hashedPassword };
            token = jwt.sign(user, env.database);
        }
        if (method === "put") {
            var query = "UPDATE users SET Name=?,Title=?,Username=?,Password=?,Token=?,Expiry=?,Email=?,Mobile=?,Photo=?,Specialty=?,Category=?,Gender=?,EMRRoles=?,Qualifications=?,License=?,DisplayID=?,DrTarget=?,Reminder=?,OutsideAccess=?,Theme=?,Registration=?,EMR=?,Modules=?,TemplateType=?,AppointmentsView=?,Appointments=?,Accounts=?,Reports=?, AttachCategoryname=?, PaintImage=?, Timeout=?, TempPermission=?, RamadanCal = ?, Location=?,Settings=?";
            if (req.body.LicenseExpiry) {
                query += ", LicenseExpiry='" + formatter.format(new Date(req.body.LicenseExpiry)) + "'";
            }
            query += " WHERE UserID=" + req.body.UserID;
            var table = [req.body.Name, req.body.Title, req.body.Username, req.body.Password, token, formatter.format(new Date(req.body.Expiry)), req.body.Email, req.body.Mobile, req.body.Photo, req.body.Specialty,req.body.Category,req.body.Gender, req.body.EMRRoles,req.body.Qualifications, req.body.License, req.body.DisplayID, req.body.DrTarget, req.body.Reminder, req.body.OutsideAccess,req.body.Theme, req.body.Registration, req.body.EMR, req.body.Modules, req.body.TemplateType, req.body.AppointmentsView, req.body.Appointments, req.body.Accounts, req.body.Reports, req.body.AttachCategoryname, req.body.PaintImage, req.body.Timeout, req.body.TempPermission, req.body.RamadanCal, req.body.Location, req.body.Settings];

        } else {
            var query = "INSERT INTO users (Name, Title, Username, Password, Token, Expiry, Email, Mobile, Photo, Specialty,Category,Gender,EMRRoles, Qualifications, License, DisplayID, DrTarget, Reminder,OutsideAccess, Theme, Registration, EMR, Modules, TemplateType, AppointmentsView, Appointments,Accounts,Reports,AttachCategoryname,PaintImage, Timeout, TempPermission, RamadanCal, Location,Settings";
            if (req.body.LicenseExpiry) {
                query += ", LicenseExpiry";
            }
            query += ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
            if (req.body.LicenseExpiry) {
                query += ",? ) ";
            }
            else {
                query += ") ";
            }
            var table = [req.body.Name, req.body.Title, req.body.Username, req.body.Password, token, formatter.format(new Date(req.body.Expiry)), req.body.Email, req.body.Mobile, req.body.Photo, req.body.Specialty,req.body.Category,req.body.Gender,req.body.EMRRoles, req.body.Qualifications, req.body.License, req.body.DisplayID, req.body.DrTarget, req.body.Reminder,req.body.OutsideAccess, req.body.Theme, req.body.Registration, req.body.EMR, req.body.Modules, req.body.TemplateType, req.body.AppointmentsView, req.body.Appointments, req.body.Accounts, req.body.Reports, req.body.AttachCategoryname, req.body.PaintImage, req.body.Timeout, req.body.TempPermission, req.body.RamadanCal, req.body.Location,req.body.Settings];
            if (req.body.LicenseExpiry) {
                table.push(formatter.format(new Date(req.body.LicenseExpiry)));
            }

        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'User saved');
    },

    userphoto: function (connection, req, res) {
        var query = "UPDATE users SET Photo=? WHERE UserID=" + req.body.UserID;
        var table = [req.body.Photo]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'User photo saved');
    },
    usertemplate: function (connection, req, res) {
        var query = "UPDATE users SET Template=? WHERE UserID=" + req.body.UserID;
        var table = [req.body.Template]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'User template saved');
    },
    deluser: function (connection, req, res) {
        var query = "DELETE FROM users WHERE UserID=" + req.query.UserID;
        this.executeQuery(connection, res, query, 'User Deleted');
    },

    messages: function (connection, req, res) {
        var query = "SELECT * FROM messages WHERE MsgFrom='" + req.query.user + "' OR MsgTo='" + req.query.user + "' ORDER BY MsgDate";
        connection.query(query, [req.query.search], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Success", "Messages": rows });
            }
        });
    },

    messagesunread: function (connection, req, res) {
        var query = "SELECT MsgFrom, Count(*) AS Unread FROM messages WHERE MsgTo='" + req.query.user + "' AND Status<2 GROUP BY MsgFrom";
        connection.query(query, [req.query.search], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "Unread": rows });
            }
        });
    },

    messagesend: function (connection, req, res) {
        var query = "INSERT INTO messages (MsgFrom, MsgTo, Subject, MessageText, FileDisplay, FileLink, Status, MsgMode, MsgDate) VALUES (?,?,?,?,?,?,?,?,?)";
        var table = [req.body.MsgFrom, req.body.MsgTo, req.body.Subject, req.body.MessageText, req.body.FileDisplay, req.body.FileLink, 0, req.body.MsgMode, formatter.format(new Date(req.body.MsgDate))];
        query = mysql.format(query, table);

        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Message sent" });
            }
        });
    },

    messagestatus: function (connection, req, res) {
        if (req.body.sender > '') {
            var query = "UPDATE messages SET Status=2 WHERE MsgTo='" + req.body.user + "' AND MsgFrom='" + req.body.sender + "'";
        } else {
            var query = "UPDATE messages SET Status=1 WHERE MsgTo='" + req.body.user + "' AND Status=0";
        }
        connection.query(query, [req.query.search], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "Message": rows });
            }
        });
    },

    theme: function (connection, req, res) {
        var query = "UPDATE users SET theme='" + req.body.Theme + "', background='" + req.body.Background + "' where Username='" + req.body.Username + "'";
        connection.query(query, [req.query.search], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "Theme": rows });
            }
        });
    },
    schedule: function (connection, req, res) {
        var query = "SELECT * FROM Schedule  ORDER BY Hours";
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Success", "schedule": rows });
            }
        });
    },
    smsemailadd: function (connection, req, res) {
        var query = "INSERT INTO Schedule (Description, MsgTrigger, Hours, SMS, Email, SMSText, EMailText ) VALUES (?,?,?,?,?,?,?)";
        var table = [req.body.Description, req.body.MsgTrigger, req.body.Hours, req.body.SMS, req.body.Email, req.body.SMSText, req.body.EMailText];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'smsemailadd');
    },
    smsemailedit: function (connection, req, res) {
        var query = "UPDATE Schedule SET Description=?, MsgTrigger=?, Hours=?, SMS=?, Email=?, SMSText=?, EMailText=? WHERE ID=? ";
        var table = [req.body.Description, req.body.MsgTrigger, req.body.Hours, req.body.SMS, req.body.Email, req.body.SMSText, req.body.EMailText, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'smsemailedit');
    },
    smsemaildel: function (connection, req, res) {
        var query = "DELETE FROM Schedule WHERE ID=" + req.query.id;
        this.executeQuery(connection, res, query, 'smsemaildel');
    },

    getPostit: function (connection, req, res) {
        var query = "SELECT * FROM PostIt WHERE User='Administrator' OR User='" + req.query.user + "' ORDER BY PostItDate";
        this.executeQuery(connection, res, query, 'Success', 'PostIt');
    },
    savePostit: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE PostIt SET NoteText=?, PositionTop=?, PositionLeft=?, Closed=? WHERE ID=?";
            var table = [req.body.NoteText, req.body.PositionTop, req.body.PositionLeft, req.body.Closed, req.body.ID];
        } else {
            var query = "INSERT INTO PostIt (NoteText, PositionTop, PositionLeft, User, PostItDate, Closed) VALUES (?,?,?,?,?,0)";
            var table = [req.body.NoteText, req.body.PositionTop, req.body.PositionLeft, req.body.User, formatter.format(new Date(req.body.PostItDate))];
        }
        var query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Note saved');
    },
    showPostit: function (connection, req, res) {
        var query = "UPDATE PostIt SET Closed = 0 WHERE User='" + req.body.user + "'";
        this.executeQuery(connection, res, query, 'Success');
    },

    delPostit: function (connection, req, res) {
        var query = "DELETE From PostIt WHERE ID=" + req.query.postitID;
        this.executeQuery(connection, res, query, 'Note deleted');
    },
    speciality: function (connection, req, res) {
        if (req.query.speciality) {
            var query = "select Description FROM specialty where Code='" + req.query.speciality + "'";
        } else {
            var query = "SELECT Description FROM specialty ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'Specialities');
    },
    userroles: function (connection, req, res) {
        if (req.query.userrole) {
            var query = "select * FROM userroles where Code='" + req.query.userrole + "'";
        } else {
            var query = "SELECT * FROM userroles ORDER BY EMRRoles";
        }
        this.executeQuery(connection, res, query, 'Success', 'Userroles');
    },
    usercategory: function (connection, req, res) {
        if (req.query.usercategory) {
            var query = "select * FROM usercategory where categoryname='" + req.query.categoryname + "'";
        } else {
            var query = "SELECT * FROM usercategory ORDER BY categoryname";
        }
        this.executeQuery(connection, res, query, 'Success', 'UserCategory');
    },

    smstemplates: function (connection, req, res) {
        var query = "SELECT * FROM smstemplates  where active =1 order by id";
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Success", "SMSTemplates": rows });
            }
        });
    },

    savesmstemplates: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE smstemplates SET Description=?, SMSText=?,Active=1 WHERE ID=" + req.body.ID;
            var message = "SMS Template Updated"
        } else {
            var query = "INSERT INTO smstemplates (Description, SMSText ,Active) VALUES(?,?,1)";
            var message = "SMS Template Added"
        }
        var table = [req.body.Description, req.body.SMSText]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    deletesmstemplates: function (connection, req, res) {
        var query = "DELETE FROM smstemplates WHERE id=" + req.query.ID;
        this.executeQuery(connection, res, query, 'SMS Template Deleted');
    },
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }


} // module.exports
