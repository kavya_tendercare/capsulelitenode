var mysql = require("mysql");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};
var formatter = require("./formatter");

module.exports = {
    doctors: function (connection, req, res) {
        var query = "SELECT * FROM ?? order by id";
        var table = ["drmast"];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Doctors');
    },
    presc: function (connection, req, res) {
        var query = "SELECT * FROM prescmaster where establishmentid =? and regn_no= ? and prescmasterdate = ? order by ID";
        var table = [req.query.establishmentid, req.query.regn_no, formatter.format(new Date(req.query.PrescripDate))];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Presc');
    },

    establishment: function (connection, req, res) {
        var query = "select * from establishment where Establishmentid = '" + req.query.establishmentID + "'"
        this.executeQuery(connection, res, query, 'Success', 'Establishment');
    },
    clinician: function (connection, req, res) {
        var query = "select * from drmast where Establishmentid = '" + req.query.establishmentID + "' and ProfessionalLicence = '" + req.query.signatoryLicenseno + "'"
        this.executeQuery(connection, res, query, 'Success', 'Clinician');
    },

    diagnosiscode: function (connection, req, res) {
        var query = "select * from icd10diag where Code = '" + req.query.code + "'"
        this.executeQuery(connection, res, query, 'Success', 'Diagnosis');
    },

    eclaimlinkpayers: function (connection, req, res) {
        var query = "select * from eclaimlinkpayers where eClaimLinkID = '" + req.query.companyID + "'"
        this.executeQuery(connection, res, query, 'Success', 'Eclaimlinkpayers');
    },

    drugs: function (connection, req, res) {
        var query = "select TRADE_NAME as Drugname,SCIENTIFIC_NAME as ScientificName,DDC_CODE as DrugCode,ROUTE_OF_ADMIN as Route,DOSAGE_FORM_PACKAGE as Packing,GRANULAR_UNIT as GranularUnit, substr(INGREDIENT_STRENGTH,instr(INGREDIENT_STRENGTH,'[')+1, abs((instr(INGREDIENT_STRENGTH,'[')+1)-instr(INGREDIENT_STRENGTH,']'))) as Strength from dubaidrugcodes where (Status='Active' OR Status like 'Grace%' )and (TRADE_NAME LIKE '%" + req.query.search + "%' OR SCIENTIFIC_NAME LIKE '%" + req.query.search + "%' OR DDC_CODE LIKE '%" + req.query.search + "%') ORDER BY TRADE_NAME,SCIENTIFIC_NAME limit 50"
        this.executeQuery(connection, res, query, 'Success', 'Drugs');
    },
    quantity: function (connection, req, res) {
        var query = "SELECT * FROM ddc_qty WHERE ddc_code='" + req.query.code + "'";
        this.executeQuery(connection, res, query, 'Success', 'Quantity');
    },
    prescription: function (connection, req, res) {
        var query = "SELECT * FROM prescmaster WHERE ID= '" + req.query.PrescID + "'";
        this.executeQuery(connection, res, query, 'Success', 'Prescription');
    },
    prescdrugs: function (connection, req, res) {
        var query = "select * from prescdrugs where PrescID =? order by ID";
        var table = [req.query.id];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'prescdrugs');
    },

    routeofadmin: function (connection, req, res) {
        var query = "SELECT * FROM routeofadmin WHERE Description= '" + req.query.routedescription + "'";
        this.executeQuery(connection, res, query, 'Success', 'RouteofAdmin');
    },

    loginsave: function (connection, req, res) {
        var query = "INSERT INTO Login (EstablishmentID, View, VisitID, PrescID, Regn_No, EmiratesID, EmiratesIDExp, OtherID, OtherIDExp, Mobile, Name, DOB, Sex, Weight, Allergies, ICDCodes, SignatoryLicenseNo, PolicyNo, PayerID, Panel, CompanyID, Theme) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        var table = [req.body.EstablishmentID, req.body.View, req.body.VisitID, req.body.PrescID, req.body.Regn_No, req.body.EmiratesID, formatter.format(new Date(req.body.EmiratesIDExp)), req.body.OtherID, formatter.format(new Date(req.body.OtherIDExp)), req.body.Mobile, req.body.Name, formatter.format(new Date(req.body.DOB)), req.body.Sex, req.body.Weight, req.body.Allergies, req.body.ICDCodes, req.body.SignatoryLicenseNo, req.body.PolicyNo, req.body.PayerID, req.body.Panel, req.body.CompanyID, req.body.Theme];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": result.insertId });
            }
        });
    },

    loginread: function (connection, req, res) {
        var query = "SELECT * FROM Login WHERE ID=" + req.query.id;
        this.executeQuery(connection, res, query, 'Success', 'PrescLogin');
    },

    saveprescription: function (connection, req, res, method) {
        var self = this;
        if (method === 'post') {
            var query = "INSERT INTO prescmaster (Regn_no, Prescmasterdate, Emiratesid,Signatory,Diagnosis,ICDcodes,Weight,PayerID,ReceiverID,Inscardno,Panel,VisitID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            var table = [req.body.Regn_no, formatter.format(new Date()), req.body.Emiratesid, req.body.Signatory, req.body.Diagnosis, req.body.ICDcodes, req.body.Weight, req.body.Payerid, req.body.ReceiverID, req.body.Inscardno, req.body.Panel, req.body.VisitID];
        } else {
            var query = "UPDATE prescmaster SET Status=?,RefNo=?,Emiratesid=?,Signatory=?,Diagnosis=?,ICDcodes=?,Weight=?,Payerid=?,ReceiverID=?,Inscardno=?,Panel=?,VisitID=? WHERE ID = '" + req.body.ID + "'"
            var table = [req.body.Status, req.body.RefNo, req.body.Emiratesid, req.body.Signatory, req.body.Diagnosis, req.body.ICDcodes, req.body.Weight, req.body.PayerID, req.body.ReceiverID, req.body.Inscardno, req.body.Panel, req.body.VisitID];
        }
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (method === 'post') {
                    self.saveprescriptionDrugs(connection, req.body.prescdrugs, res, result.insertId,req.body.VisitID,req.body.Signatory);
                } else {
                   self.saveprescriptionDrugs(connection, req.body.prescdrugs, res, req.body.ID,0,req.body.Signatory);
                }
            }
        });
    },

    saveprescriptionDrugs: function (connection, req, res,prescid,insertvisit,signatory) {
        var query = "DELETE FROM prescdrugs WHERE PrescID=" + prescid;
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO prescdrugs (PrescID, DrugCode,Drugname,ScientificName, Regn_no,Strength, Dose,Route,RouteofAdmin,Frequency,FrequencyName,Frequencymode,Unit,Days,GranularUnit,Quantity,Refill,Instructions,Packing) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.length; i++) {
                    var rec = new Array();
                    rec.push(prescid);
                    rec.push(req[i].DrugCode);
                    rec.push(req[i].Drugname);
                    rec.push(req[i].ScientificName);
                    rec.push(req[i].Regn_no);
                    rec.push(req[i].Strength);
                    rec.push(req[i].Dose);
                    rec.push(req[i].Route);
                    rec.push(req[i].RouteofAdmin);
                    rec.push(req[i].Frequency);
                    rec.push(req[i].FrequencyName);
                    rec.push(req[i].Frequencymode);
                    rec.push(req[i].Unit);
                    rec.push(req[i].Days);
                    rec.push(req[i].GranularUnit);
                    rec.push(req[i].Quantity);
                    rec.push(req[i].Refill);
                    rec.push(req[i].Instructions);
                    rec.push(req[i].Packing);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else { 
                        if (insertvisit) {
                            var query3 = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, PrescID, TemplateType, Operator) VALUES (?,?,?,?,?,?)";
                            var table3 = [insertvisit, formatter.format(new Date()), 'Prescription', prescid, 99, signatory];
                            query3 = mysql.format(query3, table3);
                            connection.query(query3, function (err, result, fields) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                        res.json({ "Error": false, "Message": prescid});
                                }
                            });
                        }
                    }
                });
            }
        });
    },
    allprescription: function (connection, req, res) {
        var query = "SELECT * FROM prescmaster WHERE Regn_No = '" + req.query.regn_no + "' order by Prescmasterdate desc"
        this.executeQuery(connection, res, query, 'Success', 'Prescriptions');
    },
    presctemplate: function (connection, req, res) {
        var query = "SELECT * FROM templatemaster WHERE User= '" + req.query.user + "' ORDER BY Description";
        this.executeQuery(connection, res, query, 'Success', 'PrescTemplates');
    },
    presctemplatedrugs: function (connection, req, res) {
        var query = "select * from templatedrugs where PrescID =? order by ID"
        var table = [req.query.tempid];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'PrescTemplateDrugs');
    },

    saveprescriptionTemplate: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO templatemaster (User, Description) VALUES (?,?)";
            var table = [req.body.User, req.body.Description];
        } else {
            var query = "UPDATE templatemaster SET Description=? WHERE ID = '" + req.body.ID + "'"
            var table = [req.body.Description];
        }
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (method === 'post') {
                    res.json({ "Error": false, "Message": result.insertId });
                } else {
                    res.json({ "Error": false, "Message": req.body.ID });
                }
            }
        });
    },
    saveprescriptionTemplateDrugs: function (connection, req, res) {
        var query = "DELETE FROM templatedrugs WHERE PrescID=" + req.body[0].PrescID
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO templatedrugs (PrescID, DrugCode,Drugname,ScientificName,Strength, Dose,Route,RouteofAdmin,Frequency,FrequencyName,Frequencymode,Unit,Days,GranularUnit,Quantity,Instructions,Packing) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    rec.push(req.body[i].PrescID);
                    rec.push(req.body[i].DrugCode);
                    rec.push(req.body[i].Drugname);
                    rec.push(req.body[i].ScientificName);
                    rec.push(req.body[i].Strength);
                    rec.push(req.body[i].Dose);
                    rec.push(req.body[i].Route);
                    rec.push(req.body[i].RouteofAdmin);
                    rec.push(req.body[i].Frequency);
                    rec.push(req.body[i].FrequencyName);
                    rec.push(req.body[i].Frequencymode);
                    rec.push(req.body[i].Unit);
                    rec.push(req.body[i].Days);
                    rec.push(req.body[i].GranularUnit);
                    rec.push(req.body[i].Quantity);
                    rec.push(req.body[i].Instructions);
                    rec.push(req.body[i].Packing);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Template saved', 'PrescDrugs': rows });
                    }
                });
            }
        });
    },
    patprescdrugs: function (connection, req, res) {
        var query = "select max(Days) as Days, prescmaster.Prescmasterdate from prescdrugs left join prescmaster on prescdrugs.PrescID = prescmaster.ID where prescdrugs.Regn_no = '" + req.query.regn_no + "' and prescdrugs.PrescID in (select max(prescmaster.ID) from prescmaster where prescmaster.regn_no = '" + req.query.regn_no + "' and prescmaster.EstablishmentID = '" + req.query.EstablishmentID + "'  order by prescmaster.ID desc )"
        this.executeQuery(connection, res, query, 'Success', 'patprescdrugs');
    },

    cliniclicno: function (connection, req, res) {
        var query = "select Dhalicence,MoIlicence from establishment where Establishmentid in(select Establishmentid from drmast where ProfessionalLicence='" + req.query.Signatorylicenseno + "')"
        this.executeQuery(connection, res, query, 'Success', 'clinicLicence');
    },
    delTemplate: function (connection, req, res) {
        var query = "delete from templatemaster where ID ='" + req.query.id + "'"
        this.executeQuery(connection, res, query, 'Template deleted', 'delTemplate');
    },
    allprescriptiondrugs: function (connection, req, res) {
        var query = "select * from prescmaster join prescdrugs  on prescmaster.ID = prescdrugs.PrescID where prescmaster.Regn_no = '" + req.query.regn_no + "' order by prescmaster.Prescmasterdate,prescdrugs.ID"
        this.executeQuery(connection, res, query, 'Success', 'PrescriptionDrugs');
    },

    sendSoapRequest: function (connection, req, res) {
        const soapRequest = require('easy-soap-request');
        const url = 'https://dhpo.eclaimlink.ae/eRxValidateTransaction.asmx';
        const headers = {
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': 'http://www.eClaimLink.ae/UploadERxRequest',
        };
        const xml = `<?xml version="1.0" encoding="utf-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ecl="http://www.eClaimLink.ae/">
        <soapenv:Header/>
        <soapenv:Body>
        <ecl:UploadERxRequest>
        <ecl:facilityLogin>` + req.body.FacilityLogin + `</ecl:facilityLogin>
        <ecl:facilityPwd>` + req.body.FacilityPwd + `</ecl:facilityPwd>
        <ecl:clinicianLogin>` + req.body.ClinicianLogin + `</ecl:clinicianLogin>
        <ecl:clinicianPwd>` + req.body.ClinicianPwd + `</ecl:clinicianPwd>
        <ecl:fileContent>` + req.body.XmlBase64 + `</ecl:fileContent>
        <ecl:fileName>` + req.body.Xmlfilename + `</ecl:fileName>
        </ecl:UploadERxRequest>
        </soapenv:Body>
        </soapenv:Envelope>`;
        (async () => {
            const { response } = await soapRequest(url, headers, xml);
            const { body, statusCode } = response;
            console.log(body);
            if (statusCode === 200) {
                res.json({ "Message": body });
            } else {
                console.log(body);
            }
        })();

    },
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }
}