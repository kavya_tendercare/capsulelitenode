var mysql = require("mysql");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");
var dtFrom, dtTo

module.exports = {
    invoices: function(connection, req, res) {
        this.getFromTo(req);
        dtTo.setHours(23,59,59);
        var query = "SELECT OPDBill.*, Patmas.Name FROM OPDBill LEFT JOIN Patmas ON OPDBill.Regn_No = Patmas.Regn_No WHERE OPDBillDate >= ? AND OPDBillDate <= ? AND OPDBill.Cancelled = 0 ORDER BY Bill_No";
        var table = [formatter.format(dtFrom), formatter.format(dtTo)];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Invoices');
    },

    invsummary: function(connection, req, res) {
        this.getFromTo(req);
        dtTo.setHours(23,59,59);
        if(!req.query.location || req.query.location === "Location") {
            var query = "SELECT " + req.query.group + ", COUNT(*) AS Nos, SUM(Amount+InsAmount) AS Amount, SUM(VAT) AS VAT,Sum(Discount) as Discount FROM OPDBill WHERE OPDBillDate>=? AND OPDBillDate<=? AND Cancelled=0 GROUP BY " + req.query.group ;
        } else {
            var query = "SELECT " + req.query.group + ", COUNT(*) AS Nos, SUM(Amount+InsAmount) AS Amount, SUM(VAT) AS VAT,Sum(Discount) as Discount FROM OPDBill WHERE OPDBillDate>=? AND OPDBillDate<=? AND Location=? AND Cancelled=0 GROUP BY " + req.query.group;
        }
        var table = [formatter.format(dtFrom), formatter.format(dtTo), req.query.location];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Summary');
    },

    receipts: function(connection, req, res) {
        this.getFromTo(req);
        var query = "SELECT Receipts.*, Patmas.Name FROM Receipts LEFT JOIN Patmas ON Receipts.Regn_No = Patmas.Regn_No WHERE ReceiptsDate>=? AND ReceiptsDate<=? AND Cancelled=0 ORDER BY Rect_No";
        var table = [formatter.format(dtFrom), formatter.format(dtTo)];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Receipts');
    },
    
    rectsummary: function(connection, req, res) {
        this.getFromTo(req);
        dtTo.setHours(23,59,59);
        if(!req.query.location || req.query.location === "Location") {
            var query = "SELECT " + req.query.group + ", COUNT(*) AS Nos, SUM(Amount) AS Amount FROM Receipts WHERE ReceiptsDate>=? AND ReceiptsDate<=? AND Cancelled=0 GROUP BY " + req.query.group ;
        } else {
            var query = "SELECT " + req.query.group + ", COUNT(*) AS Nos, SUM(Amount) AS Amount FROM Receipts WHERE ReceiptsDate>=? AND ReceiptsDate<=? AND Location=? AND Cancelled=0 GROUP BY " + req.query.group;
        }
        var table = [formatter.format(dtFrom), formatter.format(dtTo), req.query.location];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Summary');
    },

    birthdays: function(connection, req, res) {
        this.getFromTo(req);
        if (req.query.fromdate || req.query.todate) {
            var query = "SELECT * FROM PatMas WHERE Month(DOB) =month('"+formatter.format(new Date(req.query.fromdate))+"') AND DayOfMonth(DOB)>=DayOfMonth('"+formatter.format(new Date(req.query.fromdate))+"') AND DayOfMonth(DOB) <=DayOfMonth('"+formatter.format(new Date(req.query.todate))+"') ORDER BY DayOfMonth(DOB), Name";  
        } else {
            var query = "SELECT * FROM PatMas WHERE Month(DOB) =? AND DayOfMonth(DOB)>=? AND DayOfMonth(DOB) <=? ORDER BY DayOfMonth(DOB), Name";
            var table = [dtFrom.getMonth() + 1, dtFrom.getDate(), dtTo.getDate()];
            var query = mysql.format(query, table);
        }
        this.executeQuery(connection, res, query, 'Patients');
    },

    getFromTo(req) {
        if (req.query.dtFrom > '') {
            dtFrom = new Date(req.query.dtFrom);
        } else {
            dtFrom = new Date();
        }
        dtFrom.setHours(0, 0, 0);
        if (req.query.dtTo > '') {
            dtTo = new Date(req.query.dtTo);
        } else {
            dtTo = new Date();
        }
        dtTo.setHours(0, 0, 0);
    },

    executeQuery(connection, res, query, arrName) {
        connection.query(query, function(err, rows){
            if(err) {
                res.json({"Error": true, "Message": err.sqlMessage});
            } else {
                res.json({"Error": false, "Message": "Success", [arrName] : rows });
            }
        });
    }

}