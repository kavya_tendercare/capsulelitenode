var mysql = require("mysql");
var users = require("./users");
var translate = require("./translate");
var patients = require("./patients");
var doctors = require("./doctors");
var calendar = require("./calendar");
var billing = require("./billing");
var reports = require("./reports");
var emr = require("./emr");
var controlrx = require("./controlrx");
var locations = require("./locations");
var inventory = require("./inventory");
var epresc = require("./epresc");
var multer = require("multer");
var path = require("path");
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var verifyToken = require("./verifytoken");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};


var formatter = new Intl.DateTimeFormat([], options);

var store = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.normalize(__dirname) + '/uploads');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '_' + file.originalname);
    }
});
var upload = multer({ storage: store });
var smsconnection = mysql.createConnection({
    host: env.smshost,
    user: 'tenderdxb',
    password: 'kn5L0ybEYfNL',
    database: env.smsdb
});

smsconnection.connect(function (err) {
    if (err) throw err;
    console.log('Connected');

});

function REST_ROUTER(router, connection, md5) {
    var self = this;
    self.handleRoutes(router, connection, md5);
}

REST_ROUTER.prototype.handleRoutes = function (router, connection, md5) {
    router.post("/", function (req, res) {
        if (req.body.Username && req.body.Password) {
            var hashedPassword = bcrypt.hashSync(req.body.Password);
            var user = {username: req.body.Username, password: hashedPassword};
            var token = jwt.sign(user, env.database);
            return res.status(200).send({auth: true, token});
        } else {
            res.status(403).send({message: 'No username or password'});
        }
    });

    router.get("/getphoto/:photo", function (req, res) {
        res.send("./uploads/" + req.params.photo);
    })
    
    // Users
    router.get("/users", function (req, res) { users.users(connection, req, res) });
    router.get("/validateuser", function (req, res) { users.validateuser(connection, req, res) });
    router.post("/logins/save", function (req, res) { users.updateLogins(connection, req, res) });

    router.get("/users/chat", function (req, res) { users.chatusers(connection, req, res) });
    router.get("/users/groups", verifyToken, function (req, res) { users.usergroups(connection, req, res) });
    router.put("/theme", function (req, res) { users.theme(connection, req, res) });

    router.get("/user", function (req, res) { users.user(connection, req, res) });
    router.post("/user/save", function (req, res) { users.usersave(connection, req, res, 'post') });
    router.put("/user/save", function (req, res) { users.usersave(connection, req, res, 'put') });
    router.put("/user/photo", function (req, res) { users.userphoto(connection, req, res) });
    router.put("/user/template", function (req, res) { users.usertemplate(connection, req, res) });
    router.delete("/user/delete", function (req, res) { users.deluser(connection, req, res) });
    router.get("/speciality", function (req, res) { users.speciality(connection, req, res) });
    router.get("/userroles", function (req, res) { users.userroles(connection, req, res) });
    router.get("/usercategory", function (req, res) { users.usercategory(connection, req, res) });
    



    router.get("/postit", function (req, res) { users.getPostit(connection, req, res) });
    router.post("/postit", function (req, res) { users.savePostit(connection, req, res, 'post') });
    router.put("/postit", function (req, res) { users.savePostit(connection, req, res, 'put') });
    router.put("/postit/show", function (req, res) { users.showPostit(connection, req, res) });
    router.delete("/postit", function (req, res) { users.delPostit(connection, req, res) });

    // Messages
    router.get("/messages", function (req, res) { users.messages(connection, req, res); });
    router.get("/messagesunread", function (req, res) { users.messagesunread(connection, req, res) });
    router.post("/messagesend", function (req, res) { users.messagesend(connection, req, res) });
    router.put("/messagestatus", function (req, res) { users.messagestatus(connection, req, res) });
    router.get("/schedule", function (req, res) { users.schedule(connection, req, res); });
    router.post("/smsemailadd", function (req, res) { users.smsemailadd(connection, req, res) });
    router.put("/smsemailedit", function (req, res) { users.smsemailedit(connection, req, res) });
    router.delete("/smsemaildel", function (req, res) { users.smsemaildel(connection, req, res) });
    router.get("/smstemplates", function (req, res) { users.smstemplates(connection, req, res); });
    router.put("/updateSMSApptRemarks", function (req, res) { calendar.updateSMSApptRemarks(connection, req, res) });
    router.post("/smstemplates", function (req, res) { users.savesmstemplates(connection, req, res, 'post') });
    router.put("/smstemplates", function (req, res) { users.savesmstemplates(connection, req, res, 'put') });
    router.delete("/smstemplates", function (req, res) { users.deletesmstemplates(connection, req, res) });


    // Patients
    router.get("/patient", function (req, res) { patients.patient(connection, req, res) });
    router.get("/aamcpatient", function (req, res) { patients.aamcpatient(connection, req, res) });
    router.get("/patients", function (req, res) { patients.patients(connection, req, res) });
    router.get("/aamcpatients", function (req, res) { patients.aamcpatients(connection, req, res) });
    router.put("/togglepin", function (req, res) { patients.togglepin(connection, req, res) });

    router.get("/contacts/get", function (req, res) { patients.contacts(connection, req, res) });
    router.get("/nationality/get", function (req, res) { patients.nationality(connection, req, res) });
    router.get("/nationalityCode/get", function (req, res) { patients.nationalityCode(connection, req, res) });
    router.get("/refby/get", function (req, res) { patients.refby(connection, req, res) });
    router.post("/refby/save", function (req, res) { patients.saverefby(connection, req, res, 'post') });
    router.put("/refby/save", function (req, res) { patients.saverefby(connection, req, res, 'put') });
    router.delete("/delrefby/delete", function (req, res) { patients.delrefby(connection, req, res) });
    router.get("/refto/get", function (req, res) { patients.refto(connection, req, res) });
    router.post("/refto/save", function (req, res) { patients.saverefto(connection, req, res, 'post') });
    router.put("/refto/save", function (req, res) { patients.saverefto(connection, req, res, 'put') });
    router.delete("/delrefto/delete", function (req, res) { patients.delrefto(connection, req, res) });
    router.get("/patcomm/get", function (req, res) { patients.patcomm(connection, req, res) });
    router.get("/attachments/get", function (req, res) { patients.attachments(connection, req, res) });
    router.get("/visitattachments/get", function (req, res) { patients.visitattachments(connection, req, res) });
    router.post("/patcommsave", function (req, res) { patients.patcommsave(connection, req, res) });
    router.post("/patientform/save", function (req, res) { patients.savepatientform(connection, req, res, 'post') });
    router.put("/patientform/save", function (req, res) { patients.savepatientform(connection, req, res, 'put') });
    router.post("/patientformPriory/save", function (req, res) { patients.savepatientformPriory(connection, req, res, 'post') });
    router.put("/patientformPriory/save", function (req, res) { patients.savepatientformPriory(connection, req, res, 'put') });
    router.post("/patientformDDY/save", function (req, res) { patients.savepatientformDDY(connection, req, res, 'post') });
    router.put("/patientformDDY/save", function (req, res) { patients.savepatientformDDY(connection, req, res, 'put') });
    router.post("/contact/save", function (req, res) { patients.savepatcontact(connection, req, res, 'post') });
    router.put("/contact/save", function (req, res) { patients.savepatcontact(connection, req, res, 'put') });
    router.delete("/contactdel/delete", function (req, res) { patients.contactdel(connection, req, res) });
    router.put("/patphoto", function (req, res) { patients.patphoto(connection, req, res) });
    router.put("/patient/balance", function (req, res) { patients.patbalance(connection, req, res) });
    router.put("/patient/mukafa", function (req, res) { patients.patmukafa(connection, req, res) });
    router.put("/patient/mukafadel", function (req, res) { patients.patmukafadel(connection, req, res) });
    router.get("/tablets", function(req,res){ patients.tablets(connection, req, res) });
    router.post("/tablets", function(req,res){ patients.tabupdate(connection, req, res) });
    router.post("/logShowContact", function(req,res){ patients.logShowContact(connection, req, res) });


    router.put("/saveFiles", function (req, res) { patients.saveFiles(connection, req, res) });
    router.post("/patattachment/save", function (req, res) { patients.savepatattachment(connection, req, res, 'post') });
    router.put("/patattachment/save", function (req, res) { patients.savepatattachment(connection, req, res, 'put') });
    router.post("/patvisitattachment/save", function (req, res) { patients.savepatvisitattachment(connection, req, res, 'post') });
    router.put("/patvisitattachment/save", function (req, res) { patients.savepatvisitattachment(connection, req, res, 'put') });
    router.delete("/deleteAttachment/delete", function (req, res) { patients.deleteAttachment(connection, req, res) });
    router.get("/attachcategories/get", function (req, res) { patients.attachcategories(connection, req, res) });
    router.post("/attachcategory/save", function (req, res) { patients.saveattachcategory(connection, req, res, 'post') });
    router.put("/attachcategory/save", function (req, res) { patients.saveattachcategory(connection, req, res, 'put') });
    router.delete("/deleteAttachCategory/delete", function (req, res) { patients.deleteAttachCategory(connection, req, res) });
    router.get("/patcategory/get", function (req, res) { patients.patcategory(connection, req, res) });
    router.post("/category/save", function (req, res) { patients.savecategory(connection, req, res, 'post') });
    router.put("/category/save", function (req, res) { patients.savecategory(connection, req, res, 'put') });
    router.delete("/deleteCategory/delete", function (req, res) { patients.deleteCategory(connection, req, res) });
    router.post("/updvisitpage/save", function (req, res) { patients.updvisitpage(connection, req, res, 'post') });

    router.get("/similarpatfiles/get", function (req, res) { patients.similarpatfiles(connection, req, res) });
    router.put("/mergePatFiles", function (req, res) { patients.mergePatFiles(connection, req, res, 'put') });
    router.get("/eid/get", function(req,res){ patients.eid(connection, req, res) });
    router.get("/pateid/get", function(req,res){ patients.pateid(connection, req, res) });
    router.get("/patdetailseid/get", function(req,res){ patients.patdetailseid(connection, req, res) });
    router.get("/compnames/get", function(req,res){ patients.compnames(connection, req, res) });
    router.post("/updateeidimage/save", function(req,res){ patients.updateeidimage(connection, req, res) });
    router.put("/updateeidimage", function(req,res){ patients.updateeidimagefld(connection, req, res) });
    router.put("/updeidexpiry", function (req, res) { patients.updeidexpiry(connection, req, res, 'put') });
    router.get("/linkfamily/get", function(req,res){ patients.linkfamily(connection, req, res) });
    router.post("/patLink/save", function (req, res) { patients.savepatLink(connection, req, res, 'post') });
    router.delete("/deletePatLink/delete", function (req, res) { patients.deletePatLink(connection, req, res) });
    router.get("/soapins/get", function (req, res) { patients.getsoapins(connection, req, res) });
    router.get("/patreminders/current", function (req, res) { patients.getcurrentreminders(connection, req, res) });
    router.get("/patreminders", function (req, res) { patients.getpatreminders(connection, req, res) });
    router.post("/patreminders", function (req, res) { patients.savepatreminders(connection, req, res) });
    router.put("/patreminders", function (req, res) { patients.changeCompleted(connection, req, res) });
    router.delete("/patreminders", function (req, res) { patients.deletepatreminder(connection, req, res) });
    router.get("/checkmobile", function (req, res) { patients.checkmobile(connection, req, res) });
    router.get("/checkemail", function (req, res) { patients.checkemail(connection, req, res) });
    router.get("/scanned/get", function(req,res){ patients.scannedcard(connection, req, res) });
    router.get("/eligibility", function (req, res) { patients.geteligibilitys(connection, req, res) });
    router.post("/eligibility", function (req, res) { patients.saveeligibility(connection, req, res,'post') });
    router.put("/eligibility", function (req, res) { patients.saveeligibility(connection, req, res, 'put') });
    router.put("/updateEligibiltyInvNo", function (req, res) { patients.updateEligibiltyInvNo(connection, req, res) });

    router.get("/marital", function (req, res) { patients.marital(connection, req, res) });
    router.get("/relation", function (req, res) { patients.relation(connection, req, res) });
    router.get("/religion", function (req, res) { patients.religion(connection, req, res) });





    // Dr.Elsa clinic
    router.get("/patdetailseidwithname/get", function(req,res){ patients.patdetailseidwithname(connection, req, res) });

    // Doctors
    router.get("/doctor", function (req, res) { doctors.doctor(connection, req, res) });
    router.get("/specialty", function (req, res) { doctors.specialty(connection, req, res) });
    router.get("/specialties", function (req, res) { doctors.specialties(connection, req, res) });
    router.get("/doctorspecialty", function (req, res) { doctors.doctorspecialty(connection, req, res) });
    router.get("/doctimings", function (req, res) { doctors.doctimings(connection, req, res) });
    router.get("/docholidays", function (req, res) { doctors.docholidays(connection, req, res) });
    router.put("/doctorsave", function (req, res) { doctors.doctorsave(connection, req, res) });
    router.get("/docphoto", function (req, res) { doctors.docphotoget(connection, req, res) });
    router.put("/docphoto", function (req, res) { doctors.docphoto(connection, req, res) });
    router.post("/doctoradd", function (req, res) { doctors.doctoradd(connection, req, res) });
    router.post("/doctimingssave", function (req, res) { doctors.doctimingssave(connection, req, res) });
    router.post("/savedocholiday", function (req, res) { doctors.savedocholiday(connection, req, res) });
    router.delete("/deldocholiday", function (req, res) { doctors.deldocholiday(connection, req, res) });
    router.delete("/deldoctor", function (req, res) { doctors.deldoctor(connection, req, res) });

    // Appointments
    router.get("/timings", function (req, res) { calendar.timings(connection, req, res) });
    router.get("/daytimings", function (req, res) { calendar.daytimings(connection, req, res) });
    router.get("/room/get", function (req, res) { calendar.rooms(connection, req, res) });
    router.post("/room/save", function (req, res) { calendar.saveroom(connection, req, res, 'post') });
    router.put("/room/save", function (req, res) { calendar.saveroom(connection, req, res, 'put') });
    router.get("/room/appointments", function (req, res) { calendar.roomappointments(connection, req, res) });
    router.delete("/room/delete", function (req, res) { calendar.delroom(connection, req, res) });
    router.get("/appointments", function (req, res) { calendar.appointments(connection, req, res) });
    router.get("/appointment", function (req, res) { calendar.appointment(connection, req, res) });
    router.get("/checkdocappoint", function (req, res) { calendar.checkdocappoint(connection, req, res) });
    router.get("/waitinglist", function (req, res) { calendar.waitinglist(connection, req, res) });
    router.get("/checkslot",  function (req, res) { calendar.checkslot(connection, req, res) });
    router.get("/checkroom", function (req, res) { calendar.checkroom(connection, req, res) });
    router.get("/appointhistory", function (req, res) { calendar.appointhistory(connection, req, res) });
    router.get("/appointlog", function (req, res) { calendar.appointlog(connection, req, res) });
    router.get("/jobtypes", function (req, res) { calendar.jobtypes(connection, req, res) });
    router.get("/apptpurposes/get",  function (req, res) { calendar.apptpurposes(connection, req, res) });
    router.put("/appstatus", function (req, res) { calendar.appstatus(connection, req, res) });
    router.get("/appointstatus", function (req, res) { calendar.appointstatus(connection, req, res) });
    router.put("/appedit", function (req, res) { calendar.appedit(connection, req, res) });
    router.post("/appadd", function (req, res) { calendar.appadd(connection, req, res) });
    router.delete("/appdel", function (req, res) { calendar.appdel(connection, req, res) });
    router.get("/holiday/get", function (req, res) { calendar.holiday(connection, req, res) });
    router.post("/apptpurpose/save", function (req, res) { calendar.saveapptpurpose(connection, req, res, 'post') });
    router.put("/apptpurpose/save", function (req, res) { calendar.saveapptpurpose(connection, req, res, 'put') });
    router.delete("/apptpurposedel/delete", function (req, res) { calendar.apptpurposedel(connection, req, res) });
    router.post("/holiday/save", function (req, res) { calendar.saveholiday(connection, req, res, 'post') });
    router.put("/holiday/save", function (req, res) { calendar.saveholiday(connection, req, res, 'put') });
    router.delete("/delholiday/delete", function (req, res) { calendar.delholiday(connection, req, res) });
    router.get("/appttgroup", function (req, res) { calendar.groupmembers(connection, req, res) });
    router.post("/groupmember", function (req, res) { calendar.membersave(connection, req, res) });
    router.put("/groupmember", function (req, res) { calendar.memberupdate(connection, req, res) });
    router.delete("/groupmember", function (req, res) { calendar.memberdelete(connection, req, res) });
    router.get("/doctitle/get", function (req, res) { calendar.doctitle(connection, req, res) });
    router.put("/apptsms/edit", function (req, res) { calendar.updapptsms(connection,smsconnection, req, res) });
    router.put("/linkAppoint/save", function (req, res) { calendar.linkAppoint(connection, req, res) });
    router.put("/updateAppointment/edit", function (req, res) { calendar.updateAppointment(connection, req, res) });
    router.get("/smsuser/get", function(req,res){ calendar.getsmsuser(connection,smsconnection, req, res) });
    router.get("/appointcount", function(req,res){ calendar.appointcount(connection, req, res) });
    // Billing
    router.get("/transactions", function (req, res) { billing.transactions(connection, req, res) });
    router.get("/invoice/get", function (req, res) { billing.invoice(connection, req, res) });

    //**************Need to remove this code after updating all the client branches*******************/
    router.put("/invoice/save", function (req, res) { billing.saveinvoice(connection, req, res, 'put') });
    router.post("/invoice/save", function (req, res) { billing.saveinvoice(connection, req, res, 'post') });
    /**************************************************************************************************/

    router.put("/opdinvoice", function (req, res) { billing.saveopdinvoice(connection, req, res, 'put') });
    router.post("/opdinvoice", function (req, res) { billing.saveopdinvoice(connection, req, res, 'post') });
    router.get("/invoice/items", function (req, res) { billing.invoiceitems(connection, req, res) });
    router.get("/receipt/items", function (req, res) { billing.receiptitems(connection, req, res) });

    router.post("/invoice/items", function (req, res) { billing.saveinvoiceitems(connection, req, res) });
    router.put("/invoice/cancel", function (req, res) { billing.cancelinvoice(connection, req, res) });
    router.get("/invoice/unpaid", function (req, res) { billing.invoicesunpaid(connection, req, res) });

    router.put("/invoicetemplate/save", function (req, res) { billing.saveinvoicetemplate(connection, req, res, 'put') });
    router.post("/invoicetemplate/save", function (req, res) { billing.saveinvoicetemplate(connection, req, res, 'post') });
    router.post("/invoicetemplateitems/save", function (req, res) { billing.saveinvoicetemplateitems(connection, req, res) });
    router.get("/invoicetemplate/get", function (req, res) { billing.getinvoicetemplates(connection, req, res) });
    router.get("/invoicetemplateitems/get", function (req, res) { billing.getinvoicetemplateitems(connection, req, res) });
    router.delete("/invoicetemplates/delete", function (req, res) { billing.deleteinvoicetemplates(connection, req, res) });
    router.get("/accountslog", function (req, res) { billing.accountslog(connection, req, res) });
    router.post("/accountslog", function (req, res) { billing.saveaccountslog(connection, req, res) });

    router.put("/proforma/save", function (req, res) { billing.saveproforma(connection, req, res, 'put') });
    router.post("/proforma/save", function (req, res) { billing.saveproforma(connection, req, res, 'post') });
    router.post("/proforma/items", function (req, res) { billing.saveproformaitems(connection, req, res) });
    router.get("/proforma/items", function (req, res) { billing.proformaitems(connection, req, res) });
    router.put("/proforma/item", function (req, res) { billing.updateproformaitem(connection, req, res) });
    router.put("/proforma/cancel", function (req, res) { billing.cancelproforma(connection, req, res) });
    router.put("/proforma/saveremarks", function (req, res) { billing.saveremarksproforma(connection, req, res) });
    
    router.get("/receipt/get", function (req, res) { billing.receipt(connection, req, res) });
    router.put("/receipt/save", function (req, res) { billing.savereceipt(connection, req, res, 'put') });
    router.post("/receipt/save", function (req, res) { billing.savereceipt(connection, req, res, 'post') });
    router.put("/receipt/cancel", function (req, res) { billing.cancelreceipt(connection, req, res) });
    router.post("/sysrect", function (req, res) { billing.savesysrect(connection, req, res) });
    router.get("/sysrect/advances", function (req, res) { billing.sysrectadvance(connection, req, res) });

    router.get("/voucher/get", function (req, res) { billing.voucher(connection, req, res, 'put') });
    router.put("/voucher/save", function (req, res) { billing.savevoucher(connection, req, res, 'put') });
    router.post("/voucher/save", function (req, res) { billing.savevoucher(connection, req, res, 'post') });
    router.put("/voucher/cancel", function (req, res) { billing.cancelvoucher(connection, req, res) });

    router.get("/creditnodeinv", function (req, res) { billing.creditnodeinv(connection, req, res, 'put') });


    router.get("/paymode/get", function (req, res) { billing.paymodes(connection, req, res) });
    router.post("/paymode/save", function (req, res) { billing.savepaymode(connection, req, res, 'post') });
    router.put("/paymode/save", function (req, res) { billing.savepaymode(connection, req, res, 'put') });
    router.delete("/paymode/delete", function (req, res) { billing.delpaymode(connection, req, res) });

    router.get("/rates/get", function (req, res) { billing.rates(connection, req, res) });
    router.get("/rates/lab", function (req, res) { billing.labrates(connection, req, res) });
    router.get("/dentalrates/get", function (req, res) { billing.dentalrates(connection, req, res) });
    router.get("/ratesonratesno/get", function (req, res) { billing.ratesonrateno(connection, req, res) });
    router.get("/rates/dept", function (req, res) { billing.deprates(connection, req, res) });
    router.post("/rates/save", function (req, res) { billing.saverate(connection, req, res, 'post') });
    router.put("/rates/save", function (req, res) { billing.saverate(connection, req, res, 'put') });
    router.delete("/rates/delete", function (req, res) { billing.delrate(connection, req, res) });
    router.get("/todaysdentalitems/get", function (req, res) { billing.todaysdentalitems(connection, req, res) });

    router.get("/dept", function (req, res) { billing.dept(connection, req, res) });
    router.get("/deptactive", function (req, res) { billing.deptactive(connection, req, res) });
    router.get("/head/get", function (req, res) { billing.head(connection, req, res) });
    router.post("/head/save", function (req, res) { billing.savehead(connection, req, res, 'post') });
    router.put("/head/save", function (req, res) { billing.savehead(connection, req, res, 'put') });
    router.delete("/head/delete", function (req, res) { billing.delhead(connection, req, res) });
    router.get("/doctorshare/get", function (req, res) { billing.doctorshares(connection, req, res) });
    router.post("/doctorshare/save", function (req, res) { billing.savedoctorshare(connection, req, res, 'post') });
    router.put("/doctorshare/save", function (req, res) { billing.savedoctorshare(connection, req, res, 'put') });
    router.delete("/doctorshare/delete", function (req, res) { billing.doctorsharedel(connection, req, res) });
    router.get("/itemname/get", function (req, res) { billing.itemname(connection, req, res) });
    router.get("/deptname/get", function (req, res) { billing.deptname(connection, req, res) });

    router.get("/insurance/patcards/get", function (req, res) { billing.patcards(connection, req, res) });
    router.get("/insurance/patcardsactive/get", function (req, res) { billing.patcardsactive(connection, req, res) });
    router.get("/patcardsactiveexp/get", function (req, res) { billing.patcardsactiveexp(connection, req, res) });
    router.put("/insurance/patcard/save", function (req, res) { billing.patcardsave(connection, req, res, 'put') });
    router.post("/insurance/patcard/save", function (req, res) { billing.patcardsave(connection, req, res, 'post') });
    router.delete("/insurance/patcard/delete", function (req, res) { billing.patcarddelete(connection, req, res) });
    router.put("/insurance/patcard/activate", function (req, res) { billing.patcardactivate(connection, req, res) });
    router.get("/insurance/companies/get", function (req, res) { billing.companies(connection, req, res) });
    router.get("/insurancedetails", function (req, res) { billing.insurancedetails(connection, req, res) });
    router.get("/stockitem/get", function (req, res) { billing.stockitem(connection, req, res) });
    router.put("/stockitem/save", function (req, res) { billing.stockitemsave(connection, req, res, 'put') });
    router.post("/stockitem/save", function (req, res) { billing.stockitemsave(connection, req, res, 'post') });
    router.put("/cancelstock/save", function (req, res) { billing.cancelstock(connection, req, res, 'put') });
    router.get("/inslist/get", function (req, res) { billing.inslist(connection, req, res) });
    router.get("/insrates/get", function (req, res) { billing.insrate(connection, req, res) });
    router.post("/insrates/save", function (req, res) { billing.saveinsrate(connection, req, res, 'post') });
    router.put("/insrates/save", function (req, res) { billing.saveinsrate(connection, req, res, 'put') });
    router.delete("/insrates/delete", function (req, res) { billing.delinsrate(connection, req, res) });
    router.post("/soapins/save", function (req, res) { billing.savesoapins(connection, req, res, 'post') });
    router.put("/soapins/save", function (req, res) { billing.savesoapins(connection, req, res, 'put') });
    router.delete("/soapins/delete", function (req, res) { billing.soapinsdelete(connection, req, res) });
    router.put("/stars", function (req, res) { billing.updateStars(connection, req, res)});
    router.get("/insurance/patdepinsurance/get", function (req, res) { billing.patdepinsurance(connection, req, res) });
    router.get("/appointreceipts", function (req, res) { billing.appointreceipts(connection, req, res) });

    // EMR
    router.get("/visits", function (req, res) { emr.visits(connection, req, res) });
    router.get("/selectedvisits", function (req, res) { emr.selectedvisits(connection, req, res) });
    router.get("/visitsOnPageFilter", function (req, res) { emr.visitsOnPageFilter(connection, req, res) });
    router.get("/visitsOnDurationFilter", function (req, res) { emr.visitsOnDurationFilter(connection, req, res) });
    router.get("/visits/appid", function (req, res) { emr.visitsfromappt(connection, req, res) });
    router.post("/visits", function (req, res) { emr.createvisit(connection, req, res) });
    router.delete("/visits", function (req, res) { emr.deletevisit(connection, req, res) });
    router.get("/lastvisit", function (req, res) { emr.lastVisit(connection, req, res) });
    router.get("/visitlog", function (req, res) { emr.visitlog(connection, req, res) });
    router.get("/visitpages", function (req, res) { emr.visitPages(connection, req, res) });
    router.get("/lastdiagnosisvisit", function (req, res) { emr.lastdiagnosisvisit(connection, req, res) });
    router.put("/emr/notes/save", function (req, res) { emr.savedocnotes(connection, req, res) });
    router.get("/pagegroups", function (req, res) { emr.pagegroups(connection, req, res) });
    router.get("/emr/addpage", function (req, res) { emr.getpages(connection, req, res) });
    router.post("/emr/addpage/save", function (req, res) { emr.savepage(connection, req, res, 'post') });
    router.put("/emr/addpage/save", function (req, res) { emr.savepage(connection, req, res, 'put') });
    router.delete("/emr/addpage/delete", function (req, res) { emr.deletepage(connection, req, res) });
    router.get("/emr/templates", function (req, res) { emr.templates(connection, req, res) });
    router.get("/emr/images", function (req, res) { emr.getimages(connection, req, res) });
    router.get("/emr/templates/get", function (req, res) { emr.templatesget(connection, req, res) });
    router.get("/emr/templatename/get", function (req, res) { emr.templatename(connection, req, res) });
    router.get("/emr/templatebytitle", function (req, res) { emr.templatebytitle(connection, req, res) });
    router.post("/emr/templates/save", function (req, res) { emr.templatesave(connection, req, res) });
    router.delete("/emr/templates/delete", function (req, res) { emr.temlatedelete(connection, req, res) });
    router.get("/complaints/get", function (req, res) { emr.complaints(connection, req, res) });
    router.get("/complaintsonSameDay/get", function (req, res) { emr.complaintsonSameDay(connection, req, res) });
    router.put("/complaints/save", function (req, res) { emr.savecomplaints(connection, req, res, 'put') });
    router.post("/complaints/save", function (req, res) { emr.savecomplaints(connection, req, res, 'post') });
    router.get("/lastweight", function (req, res) { emr.lastWeight(connection, req, res) });
    router.get("/pain/get", function (req, res) { emr.pain(connection, req, res) });
    router.put("/pain/save", function (req, res) { emr.savepain(connection, req, res, 'put') });
    router.post("/pain/save", function (req, res) { emr.savepain(connection, req, res, 'post') });
    router.get("/abuse/get", function (req, res) { emr.abuse(connection, req, res) });
    router.put("/abuse/save", function (req, res) { emr.saveabuse(connection, req, res, 'put') });
    router.post("/abuse/save", function (req, res) { emr.saveabuse(connection, req, res, 'post') });
    router.post("/visitlog/save", function (req, res) { emr.savevisitlog(connection, req, res, 'post') });
    router.get("/dentalchart", function (req, res) { emr.getdental(connection, req, res) });
    router.post("/dentalchart", function (req, res) { emr.savedental(connection, req, res, 'post') });
    router.put("/dentalchart", function (req, res) { emr.savedental(connection, req, res, 'put') });
    router.delete("/dentalchart", function (req, res) { emr.deletedental(connection, req, res) });
    router.get("/dentalprocedures", function (req, res) { emr.getdentalprocedures(connection, req, res) });
    router.get("/dentalprocedure", function (req, res) { emr.getdentalprocedure(connection, req, res) });
    router.post("/dentalprocedure", function (req, res) { emr.savedentalprocedure(connection, req, res, 'post') });
    router.put("/dentalprocedure", function (req, res) { emr.savedentalprocedure(connection, req, res, 'put') });
    router.delete("/dentalprocedure", function (req, res) { emr.deletedentalprocedure(connection, req, res) });
    router.get("/dentaloptions", function (req, res) { emr.dentaloptions(connection, req, res) });
    router.get("/dentalcategory", function (req, res) { emr.dentalcategory(connection, req, res) });
    router.get("/ophthalmicexam/get", function (req, res) { emr.ophthalmicexam(connection, req, res) });
    router.post("/ophthalmicpost/save", function (req, res) { emr.saveophthalmic(connection, req, res, 'post') });
    router.put("/ophthalmicput/save", function (req, res) { emr.saveophthalmic(connection, req, res, 'put') });
    router.get("/glassespresc", function (req, res) { emr.glassespresc(connection, req, res) });
    router.post("/glassespost", function (req, res) { emr.saveglasses(connection, req, res, 'post') });
    router.put("/glassesput", function (req, res) { emr.saveglasses(connection, req, res, 'put') });
    router.get("/ophbaseexam", function (req, res) { emr.ophbaseexam(connection, req, res) });
    router.post("/ophbaseexampost", function (req, res) { emr.saveophbaseexam(connection, req, res, 'post') });
    router.put("/ophbaseexamput", function (req, res) { emr.saveophbaseexam(connection, req, res, 'put') });



    router.get("/diagnosis/get", function(req,res){ emr.diagnosis(connection, req, res) });
    router.get("/diagnosis/code", function(req,res){ epresc.diagnosiscode(connection, req, res) });
    router.get("/diagnosis/last", function(req,res){ emr.lastdiagnosis(connection, req, res) });
    router.post("/diagnosis/save", function(req,res){ emr.savediagnosis(connection, req, res) });
    router.delete("/diagnosis/delete", function(req,res){ emr.deletediagnosis(connection, req, res) });
    router.put("/diagnosis/cancel", function(req,res){ emr.canceldiagnosis(connection, req, res) });
    router.put("/diagnosis/resolve", function(req,res){ emr.resolvediagnosis(connection, req, res) });
    router.put("/diagnosis/typeupdate", function(req,res){ emr.typeupdatediagnosis(connection, req, res) });
    router.get("/icd", function(req,res){ emr.getICD(connection, req, res) });
    router.post("/favicd", function(req,res){ emr.favICDsave(connection, req, res) });
    router.delete("/favicd", function(req,res){ emr.favICDdelete(connection, req, res) });    
    router.post("/prescid/save", function(req,res){ emr.saveprescriptionid(connection, req, res, 'post') }); 

    router.get("/cpt", function(req,res){ emr.getCPT(connection, req, res) });
    router.get("/insapprovalcpt", function(req,res){ emr.getInsAppCPT(connection, req, res) });
    router.get("/cpt/procedures", function(req,res){ emr.procedurescpt(connection, req, res) });
    router.post("/cpt/procedures", function(req,res){ emr.saveprocedurescpt(connection, req, res) });
    router.delete("/cpt/procedures", function(req,res){ emr.delprocedurescpt(connection, req, res) });
    router.get("/cpt/visit", function(req,res){ emr.visitcpt(connection, req, res) });
    router.post("/cpt/visit", function(req,res){ emr.savevisitcpt(connection, req, res) });
    router.delete("/cpt/visit", function(req,res){ emr.delvisitcpt(connection, req, res) });

    router.get("/emr/procedure", function(req,res){ emr.getProcedure(connection, req, res) });
    router.put("/emr/procedure", function(req,res){ emr.saveProcedure(connection, req, res, 'put') });
    router.post("/emr/procedure", function(req,res){ emr.saveProcedure(connection, req, res, 'post') });

    router.get("/prescription/get", function(req,res){ epresc.prescription(connection, req, res) });
    router.get("/prescripdrugs/get", function(req,res){ epresc.prescripdrugs(connection, req, res) });
    router.get("/routeofadmin/get", function(req,res){ emr.routeofadmin(connection, req, res) });
    router.get("/drugs/get", function(req,res){ emr.drugs(connection, req, res) });
    router.get("/controlRx/get", function(req,res){ controlrx.getcontrolrx(connection, req, res) });
    router.get("/controlRx/token", function(req,res){ controlrx.getToken(connection, req, res) });
    router.get("/controlRxprescripdrugs/get", function(req,res){ controlrx.getcontrolledprescripdrugs(connection, req, res) });
    router.get("/controlRx/checkPatient", function(req,res){ controlrx.checkPatient(connection, req, res) });
    router.get("/controlRx/getDoctorId", function(req,res){ controlrx.getDoctorId(connection, req, res) });
    router.get("/controlRx/getOrganisationDetails", function(req,res){ controlrx.getOrganisationDetails(connection, req, res) });
    router.post("/controlRx/createResource", function(req,res){ controlrx.createResource(connection, req, res) });
    router.post("/controlRx/createMedicationRequest", function(req,res){ controlrx.createMedicationRequest(connection, req, res) });
    router.get("/medicineDetails/get", function(req,res){ controlrx.getMedicineDetails(connection, req, res) });
    router.get("/medicationDetails/get", function(req,res){ controlrx.getMedicationDetails(connection, req, res) });
    router.post("/controlRx/cancelMedRequest", function(req,res){ controlrx.cancelMedRequest(connection, req, res) });  


    
    router.get("/presctemplates", function(req,res){ epresc.presctemplate(connection, req, res) });
    router.get("/presctemplatedrugs", function(req,res){ epresc.presctemplatedrugs(connection, req, res) });
    router.put("/presctemplate", function(req,res){ epresc.saveprescriptionTemplate(connection, req, res, 'put') });
    router.post("/presctemplate", function(req,res){ epresc.saveprescriptionTemplate(connection, req, res, 'post') });
    router.post("/prescripTemplateDrugs", function(req,res){ epresc.saveprescriptionTemplateDrugs(connection, req, res, 'post') });

    router.get("/allprescriptions", function(req,res){ epresc.allprescription(connection, req, res) });
    router.get("/prescription", function(req,res){ epresc.prescription(connection, req, res) });
    router.get("/prescripdrugs", function(req,res){ epresc.prescdrugs(connection, req, res) });

    router.put("/prescription", function(req,res){ epresc.saveprescription(connection, req, res, 'put') });
    router.post("/prescription", function(req,res){ epresc.saveprescription(connection, req, res, 'post') });  
    router.post("/prescriptionsDrugs", function(req,res) { epresc.saveprescriptionDrugs(connection, req, res) });

    router.get("/allprescriptiondrugs", function(req,res){ epresc.allprescriptiondrugs(connection, req, res) });
    router.post("/sendSoapRequest/save", function(req,res){ epresc.sendSoapRequest(connection, req, res) });


    router.get("/consents/get", function(req,res){ emr.consentform(connection, req, res) });
    router.get("/consentdetails/get", function(req,res){ emr.consentdetails(connection, req, res) });
    router.post("/emr/patientconsent/save", function(req,res){ emr.patientconsentsave(connection, req, res) });
                           
    router.put("/pasthistory/save", function(req,res){ emr.pasthistorysave(connection, req, res, 'put') });
    router.post("/pasthistory/save", function(req,res){ emr.pasthistorysave(connection, req, res, 'post') });
    router.get("/pasthistory/get", function(req,res){ emr.pasthistory(connection, req, res) });
    router.put("/drugallergy/save", function(req,res){ emr.drugallergysave(connection, req, res, 'put') });
    router.put("/medication/save", function(req,res){ emr.medicationsave(connection, req, res, 'put') });
    router.get("/symptoms/get", function(req,res){ emr.symptoms(connection, req, res) });
    router.get("/systems/get", function(req,res){ emr.systems(connection, req, res) });
    router.get("/systemsfrench/get", function(req,res){ emr.systemsfrench(connection, req, res) });
    router.get("/symptompatdata/get", function(req,res){ emr.symptompatdata(connection, req, res) });
    router.post("/sysreview/save", function(req,res) { emr.savesysreview(connection, req, res) });
   
    router.get("/immun/get", function(req,res){ emr.immun(connection, req, res) });
    router.get("/stdimmun/get", function(req,res){ emr.stdimmun(connection, req, res) });
    router.post("/saveImmun/save", function(req,res) { emr.saveImmun(connection, req, res) });
    router.post("/saveStdImmun/save", function(req,res) { emr.saveStdImmun(connection, req, res) });
    router.post("/saveRegenerateStd/save", function(req,res) { emr.saveRegenerateStd(connection, req, res) });
    router.put("/saveStdmaster/save", function(req,res){ emr.saveStdmaster(connection, req, res, 'put') });
    router.post("/saveStdmaster/save", function(req,res){ emr.saveStdmaster(connection, req, res, 'post') });  

    router.delete("/stdimmun/delete", function(req,res){ emr.delstdimmun(connection, req, res) });
    router.get("/paedgrowth/get", function(req,res){ emr.paedgrowth(connection, req, res) });
    router.post("/savegrowchart/save", function(req,res){ emr.savegrowchart(connection, req, res, 'post') });  
    router.get("/visitdropdown/get", function(req,res){ emr.visitdropdown(connection, req, res) });
    router.get("/gynaecology/get", function(req,res){ emr.gynaecology(connection, req, res) });
    router.get("/gynaeExam", function(req,res){ emr.getGynaeExam(connection, req, res) });
    router.post("/gynaeExam", function (req, res) { emr.savegynaeExam(connection, req, res, 'post') });
    router.put("/gynaeExam", function (req, res) { emr.savegynaeExam(connection, req, res, 'put') });
    router.get("/antenatalChart/get", function(req,res){ emr.antenatalChart(connection, req, res) });
    router.get("/pregriskfactor/get", function(req,res){ emr.pregriskfactor(connection, req, res) });
    router.put("/antechart/save", function(req,res){ emr.saveAntechart(connection, req, res, 'put') });
    router.post("/antechart/save", function(req,res){ emr.saveAntechart(connection, req, res, 'post') }); 
    router.post("/gynaecology/save", function(req,res){ emr.saveGynaecology(connection, req, res, 'post') });
    router.put("/gynaecology/save", function(req,res){ emr.saveGynaecology(connection, req, res, 'put') });
    router.get("/autocomplete", function(req,res){ emr.getautocomplete(connection, req, res) });
    router.post("/autocomplete", function(req,res){ emr.insertautocomplete(connection, req, res) });
    router.put("/autocomplete", function(req,res){ emr.updateautocomplete(connection, req, res) });
    router.get("/autocompleteall/get", function(req,res){ emr.getallautocomplete(connection, req, res) });
    router.post("/autocomplete/save", function (req, res) { emr.saveautocomplete(connection, req, res, 'post') });
    router.put("/autocomplete/save", function (req, res) { emr.saveautocomplete(connection, req, res, 'put') });
    router.delete("/delAutocomplete/delete", function (req, res) { emr.delAutocomplete(connection, req, res) });
    router.get("/surgicalreqform/get", function(req,res){ emr.getsurgicalreqform(connection, req, res) });
    router.post("/SurgicalReqForm/save", function (req, res) { emr.savesurgicalreqform(connection, req, res, 'post') });
    router.put("/SurgicalReqForm/save", function (req, res) { emr.savesurgicalreqform(connection, req, res, 'put') });
    router.get("/fallrisk/get", function(req,res){ emr.getfallrisk(connection, req, res) });
    router.post("/fallrisk/save", function (req, res) { emr.savefallrisk(connection, req, res, 'post') });
    router.put("/fallrisk/save", function (req, res) { emr.savefallrisk(connection, req, res, 'put') });
    router.get("/contraceptives/get", function(req,res){ emr.getcontraceptives(connection, req, res) });
    router.get("/MobileNotes/get", function(req,res){ emr.getMobileNotes(connection, req, res) });
    router.post("/mobilenotes/save", function (req, res) { emr.savemobilenotes(connection, req, res, 'post') });
    router.put("/mobilenotes/save", function (req, res) { emr.savemobilenotes(connection, req, res, 'put') });
    router.get("/medications/get", function (req, res) { emr.medications(connection, req, res) });
    router.put("/medications/save", function(req,res){ emr.saveMedications(connection, req, res, 'put') });
    router.post("/medications/save", function(req,res){ emr.saveMedications(connection, req, res, 'post') }); 
    router.delete("/medication/delete", function (req, res) { emr.delmedication(connection, req, res) });
    router.get("/consentsigned", function (req, res) { emr.consentsigned(connection, req, res) });

    router.get("/notes/get", function (req, res) { emr.commonnotes(connection, req, res) });
    router.put("/commonNotes/save", function(req,res){ emr.saveCommonNotes(connection, req, res, 'put') });
    router.post("/commonNotes/save", function(req,res){ emr.saveCommonNotes(connection, req, res, 'post') }); 
    router.delete("/commonNotes/delete", function (req, res) { emr.deleteCommonNotes(connection, req, res) });

    router.get("/allergyseverity", function (req, res) { emr.allergyseverity(connection, req, res) });
    router.get("/allergytype", function (req, res) { emr.allergytype(connection, req, res) });

    router.get("/allergy/get", function (req, res) { emr.allergy(connection, req, res) });
    router.put("/allergy/save", function(req,res){ emr.saveAllergy(connection, req, res, 'put') });
    router.post("/allergy/save", function(req,res){ emr.saveAllergy(connection, req, res, 'post') }); 
    router.delete("/allergy/delete", function (req, res) { emr.delallergy(connection, req, res) });

    router.get("/diagcheck", function (req, res) { emr.diagcheck(connection, req, res) });





    // Lab
    router.get("/labtests", function (req, res) { emr.getLab(connection, req, res) });
    router.put("/labtests", function(req,res){ emr.saveLab(connection, req, res, 'put') });
    router.post("/labtests", function(req,res){ emr.saveLab(connection, req, res, 'post') }); 
    router.get("/labstatus", function(req,res){ emr.getLabStatus(connection, req, res) });
    router.post("/labstatus", function(req,res){ emr.updateLabStatus(connection, req, res) });
    router.put("/labattach", function(req,res){ emr.saveLabAttachment(connection, req, res) });
    router.get("/labattach", function(req,res){ emr.getLabAttachment(connection, req, res) });
    router.get("/labtemplates", function(req,res){ emr.getLabTemplates(connection, req, res) });
    router.post("/labtemplates", function(req,res){ emr.saveLabTemplate(connection, req, res) });
    router.delete("/labtemplates", function(req,res){ emr.deleteLabTemplate(connection, req, res) });
    router.get("/labtemplates/items", function(req,res){ emr.getLabTemplateItems(connection, req, res) });
    router.get("/labitems", function(req,res){ emr.getLabItems(connection, req, res) });

    
    router.get("/cptcode", function (req, res) { emr.cptcode(connection, req, res) });
    router.get("/encounter", function (req, res) { emr.encounter(connection, req, res) });

    router.get("/sickleave", function(req,res){ emr.getSickleave(connection, req, res) });
    router.post("/sickleave", function(req,res){ emr.saveSickleave(connection, req, res, 'post') }); 
    router.put("/sickleave", function(req,res){ emr.saveSickleave(connection, req, res, 'put') }); 
    
    router.get("/allergyseverity", function (req, res) { emr.allergyseverity(connection, req, res) });
    router.get("/complaintsMore", function (req, res) { emr.complaintsMore(connection, req, res) });
    router.post("/complaintsMore", function(req,res){ emr.savecomplaintsMore(connection, req, res, 'post') }); 
    router.put("/complaintsMore", function(req,res){ emr.savecomplaintsMore(connection, req, res, 'put') });
    router.delete("/deleteComplaintsMore", function (req, res) { emr.deleteComplaintsMore(connection, req, res) });
    router.put("/visitprepared", function(req,res){ emr.visitprepared(connection, req, res) }); 
    router.get("/printallvisits", function (req, res) { emr.printallvisits(connection, req, res) });
    router.put("/openvisit", function (req, res) { emr.openvisit(connection, req, res) });
    router.get("/imageattachments", function (req, res) { patients.imageattachments(connection, req, res) });
    router.get("/mchatmaster", function(req,res){ emr.mchatmaster(connection, req, res) });
    router.post("/mchatdata", function(req,res) { emr.savemchatdata(connection, req, res) });
    router.get("/patmchatdata", function(req,res){ emr.patmchatdata(connection, req, res) });


    router.get("/patpasthistory", function(req,res){ emr.patpasthistory(connection, req, res) });
    router.get("/covidquestionaire", function(req,res){ emr.getCovidQuestionaire(connection, req, res) });
    router.get("/patpasthistory/maris", function(req,res){ emr.patpasthistorymaris(connection, req, res) });
    router.put("/patpasthistorymaris/save", function(req,res){ emr.patpasthistorymarissave(connection, req, res, 'put') });
    router.post("/patpasthistorymaris/save", function(req,res){ emr.patpasthistorymarissave(connection, req, res, 'post') });

    router.get("/previoussales/lucia", function(req,res){ billing.getPreviousSales(connection, req, res) });





    // Reports
    router.get("/reports/invoices", function (req, res) { reports.invoices(connection, req, res) });
    router.get("/reports/invsummary", function (req, res) { reports.invsummary(connection, req, res) });
    router.get("/reports/receipts", function (req, res) { reports.receipts(connection, req, res) });
    router.get("/reports/rectsummary", function (req, res) { reports.rectsummary(connection, req, res) });
    router.get("/reports/birthdays", function (req, res) { reports.birthdays(connection, req, res) });

    // locations
    router.get("/locations/get", function (req, res) { locations.locations(connection, req, res) });
    router.put("/addwaitingpatients/save", function (req, res) { locations.savewaitingpatients(connection, req, res, 'put') });
    router.put("/editWeekoff/save", function (req, res) { locations.saveweekoff(connection, req, res, 'put') });

    //Inventory
    router.get("/issuemst", function(req, res) { inventory.issuemst(connection, req, res) });
    router.post("/issuemst", function(req, res) { inventory.addissuemst(connection, req, res) });
    router.post("/issuetrn", function(req, res) { inventory.issuetrn(connection, req, res) });
    router.delete("/issuetrn", function(req, res) { inventory.delissuetrn(connection, req, res) });
    router.get("/itemcode/get", function(req, res) { inventory.getitemcodewithbarcode(connection, req, res) });
    
    router.get("/translate/languages", function(req, res) { translate.languages(connection, req, res) });

}

module.exports = REST_ROUTER;