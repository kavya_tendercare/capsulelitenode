module.exports = {

    format: function(date) {
        var tzOffset = (new Date()).getTimezoneOffset() * 60000;
        var newDate = new Date(date.getTime() - tzOffset);
        if (date.getHours() === 0 && date.getMinutes() === 0 && date.getSeconds() === 0) {
            return newDate.getFullYear() + '-' + (newDate.getMonth() + 1) + '-' + newDate.getDate();
        } else {
            return newDate.toISOString().replace(/T/, ' ').replace(/\..+/, '');
        }
    }
}