var mysql = require("mysql");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");

module.exports = {
    patient: function (connection, req, res) {
        var query = "SELECT * FROM patmas WHERE Regn_No='" + req.query.regn_no + "'";
        connection.query(query, [req.query.search], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "Patients": rows });
            }
        });
    },

    aamcpatient: function (connection, req, res) {
        var query = "SELECT patmas.*,categories.DiscPercent FROM patmas left JOIN categories on patmas.Category = categories.Category WHERE Regn_No='" + req.query.regn_no + "'";
        connection.query(query, [req.query.search], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "Patients": rows });
            }
        });
    },

    patients: function (connection, req, res) {
        if (req.query.search > '') {
            if (!isNaN(req.query.search) && req.query.search.length < 4) {
                var query = "SELECT * FROM patmas WHERE Regn_No='" + req.query.search + "'";
            } else if (req.query.search.indexOf(' ') > 0) {
                var aSearch = req.query.search.split(" ");
                var query = "SELECT *  FROM patmas WHERE NAME LIKE '%" + aSearch[0] + "%' ";
                for (var i = 1; i < aSearch.length; i++) {
                    query += "AND NAME LIKE '%" + aSearch[i] + "%' ";
                }
            } else {
                var query = "SELECT *  FROM patmas WHERE Name LIKE '%" + req.query.search + "%' OR Mobile LIKE '%" + req.query.search + "%' OR Regn_No LIKE '%" + req.query.search + "%' OR EmiratesID LIKE '%" + req.query.search + "%' OR Email LIKE '%" + req.query.search + "%' ORDER BY Name DESC LIMIT 10";
            }
        } else {
            var query = "SELECT * FROM patmas ORDER BY Pinned DESC, PatmasDate DESC LIMIT 10";
        }
        this.executeQuery(connection, res, query, 'Success', 'Patients');
    },

    aamcpatients: function (connection, req, res) {
        if (req.query.search > '') {
            if (!isNaN(req.query.search) && req.query.search.length < 4) {
                var query = "SELECT patmas.*,categories.DiscPercent FROM patmas left JOIN categories on patmas.Category = categories.Category WHERE Regn_No='" + req.query.search + "'";
            } else if (req.query.search.indexOf(' ') > 0) {
                var aSearch = req.query.search.split(" ");
                var query = "SELECT patmas.*,categories.DiscPercent FROM patmas left JOIN categories on patmas.Category = categories.Category WHERE NAME LIKE '%" + aSearch[0] + "%' ";
                for (var i = 1; i < aSearch.length; i++) {
                    query += "AND NAME LIKE '%" + aSearch[i] + "%' ";
                }
            } else {
                var query = "SELECT patmas.*,categories.DiscPercent FROM patmas left JOIN categories on patmas.Category = categories.Category WHERE Name LIKE '%" + req.query.search + "%' OR Mobile LIKE '%" + req.query.search + "%' OR Regn_No LIKE '%" + req.query.search + "%' OR EmiratesID LIKE '%" + req.query.search + "%' OR Email LIKE '%" + req.query.search + "%' ORDER BY Name DESC LIMIT 10";
            }
        } else {
            var query = "SELECT patmas.*,categories.DiscPercent FROM patmas left JOIN categories on patmas.Category = categories.Category ORDER BY Pinned DESC, PatmasDate DESC LIMIT 10";
        }
        this.executeQuery(connection, res, query, 'Success', 'Patients');
    },

    togglepin:function (connection, req, res) {
        var query = "UPDATE Patmas SET Pinned = IF(Pinned=1,0,1) WHERE Regn_No='" + req.body.regn + "'"
        this.executeQuery(connection, res, query, 'Pin updated');
    },

    patbalance: function (connection, req, res) {
        var query = "UPDATE Patmas SET Balance=" + req.body.balance + " WHERE Regn_No='" + req.body.regn_no + "'"
        this.executeQuery(connection, res, query, 'Balance updated');
    },
    
    patmukafa: function (connection, req, res) {
        var query = "UPDATE Patmas SET MukafaCardNo='" + req.body.mukafa + "' WHERE Regn_No='" + req.body.regn_no + "'"
        this.executeQuery(connection, res, query, 'Mukafa updated');
    },

    patmukafadel: function (connection, req, res) {
        var query = "UPDATE Patmas SET MukafaCardNo = null WHERE Regn_No='" + req.body.regn_no + "'"
        this.executeQuery(connection, res, query, 'Mukafa removed');
    },

    
    contacts: function (connection, req, res) {
        var query = "SELECT * FROM addcont WHERE Regn_No='" + req.query.regn_no + "' ORDER BY LegalGuardian DESC, ID";
        connection.query(query, [req.query.search], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "Contacts": rows });
            }
        });
    },

    nationality: function (connection, req, res) {
        if (req.query.eidnational > '') {
            var query = "select Description FROM nationality where countrycode='" + req.query.eidnational + "'"
        } else {
            var query = "SELECT Description FROM Nationality ORDER BY Description";
        }

        connection.query(query, [req.query.country], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "Nationality": rows });
            }
        });

    },
    nationalityCode: function (connection, req, res) {
        var query = "select CPQ_Code FROM nationality where Description='" + req.query.nationality + "'"
        connection.query(query, [req.query.country], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "NationalityCode": rows });
            }
        });
    },

    tablets: function (connection, req, res) {
        var query = "SELECT * FROM Tablets ORDER BY TabNo";
        this.executeQuery(connection, res, query, 'Success', 'Tablets');
    },

    tabupdate: function (connection, req, res) {
        var query = "UPDATE Tablets SET Regn_No=?, TabletsDate=?, Operator=? WHERE TabNo=?";
        var table = [req.body.regn_no, formatter.format(new Date()), req.body.operator, req.body.tabno];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Tab Updated');
    },

    logShowContact: function (connection, req, res) {
        var query = "INSERT INTO ContactLog (Regn_No, ContactLogDate, ContactLogType,Operator) VALUES (?,?,?,?)";
        var table = [req.body.regn_no, formatter.format(new Date()), req.body.source, req.body.operator];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Contact Log Added');
    },

    refby: function (connection, req, res) {
        var query = "SELECT * FROM Refby ORDER BY DrName";
        connection.query(query, [req.query.country], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "RefBy": rows });
            }
        });
    },
    saverefby: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE refby SET DrTitle=?,DrName=?,DrTel=?,DrMobile=?,DrUnit=?,DrEmail=?,DrAdd=? WHERE ID=" + req.body.ID;
            var table = [req.body.DrTitle, req.body.DrName, req.body.DrTel, req.body.DrMobile, req.body.DrUnit, req.body.DrEmail, req.body.DrAdd]
            var message = "RefBy Updated"
        } else {
            var query = "INSERT INTO refby (DrTitle, DrName, DrTel, DrMobile, DrUnit, DrEmail,DrAdd) VALUES(?,?,?,?,?,?,?)";
            var table = [req.body.DrTitle, req.body.DrName, req.body.DrTel, req.body.DrMobile, req.body.DrUnit, req.body.DrEmail, req.body.DrAdd]
            var message = "RefBy  Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    delrefby: function (connection, req, res) {
        var query = "DELETE FROM refby WHERE ID=" + req.query.ID;
        connection.query(query, [req.query.ID], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Success", "Message": "Ref By Deleted" });
            }
        });
    },
    refto: function (connection, req, res) {
        var query = "SELECT distinct Hospital FROM Refto ORDER BY Doctor";
        connection.query(query, [req.query.country], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Success", "RefTo": rows });
            }
        });
    },
    saverefto: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE Refto SET DrTitle=?,Doctor=?,Hospital=?,Address=?,Tel1=?,Tel2=?,Mobile=?,Fax=?,Email=?,Remarks=? WHERE ID=" + req.body.ID;
            var table = [req.body.DrTitle, req.body.Doctor, req.body.Hospital, req.body.Address, req.body.Tel1, req.body.Tel2, req.body.Mobile, req.body.Fax, req.body.Email, req.body.Remarks]
            var message = "RefTo Updated"
        } else {
            var query = "INSERT INTO refto (DrTitle, Doctor, Hospital, Address, Tel1, Tel2,Mobile,Fax,Email,Remarks) VALUES(?,?,?,?,?,?,?,?,?,?)";
            var table = [req.body.DrTitle, req.body.Doctor, req.body.Hospital, req.body.Address, req.body.Tel1, req.body.Tel2, req.body.Mobile, req.body.Fax, req.body.Email, req.body.Remarks]
            var message = "RefTo  Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    delrefto: function (connection, req, res) {
        var query = "DELETE FROM refto WHERE ID=" + req.query.ID;
        connection.query(query, [req.query.ID], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Success", "Message": "Ref To Deleted" });
            }
        });
    },
    patcomm: function (connection, req, res) {
        var query = "SELECT * FROM patcomm WHERE Regn_No='" + req.query.regn_no + "' ORDER BY PatCommDate DESC"
        connection.query(query, [req.query.regn_no], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err });
            } else {
                res.json({ "Error": false, "Message": "Success", "PatComm": rows });
            }
        });
    },

    patcommsave: function (connection, req, res) {
        var query = "INSERT INTO patcomm (Regn_No, PatCommDate, CommType, Notes, User, Attachments) VALUES (?,?,?,?,?,?)";
        var table = [req.body.Regn_No, formatter.format(new Date(req.body.PatCommDate)), req.body.CommType, req.body.Notes, req.body.User, req.body.Attachments];
        query = mysql.format(query, table);

        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Message saved" });
            }
        });
    },

    savepatattachment: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE attachments SET  Category=?, Description=?,Interpretation=?  WHERE Attach_ID=?";
            var table = [req.body.Category, req.body.Description,req.body.Interpretation, req.body.Attach_ID];
            var message = "Attachments Updated"
            query = mysql.format(query, table);
            this.executeQuery(connection, res, query, message);
        } else {
            var query = "INSERT INTO attachments (Regn_No, Operator, AttachmentsDate, Category, Description,Interpretation, Filename, CurrentDate,VisitID, AppID, LabID) VALUES ?";
            var records = new Array();
            for (var i = 0; i < req.body.length; i++) {
                var rec = new Array();
                rec.push(req.body[i].Regn_No);
                rec.push(req.body[i].Operator);
                req.body[i].AttachmentsDate = formatter.format(new Date(req.body[i].AttachmentsDate));
                rec.push(req.body[i].AttachmentsDate);
                rec.push(req.body[i].Category);
                rec.push(req.body[i].Description);
                rec.push(req.body[i].Interpretation);
                rec.push(req.body[i].Filename);
                req.body[i].CurrentDate = formatter.format(new Date(req.body[i].CurrentDate));
                rec.push(req.body[i].CurrentDate);
                rec.push(req.body[i].VisitID);
                rec.push(req.body[i].AppID);
                rec.push(req.body[i].LabID);
                records.push(rec);
            }
            var message = "Attachments  Added"

            query = mysql.format(query, [records]);
            connection.query(query, function (err, result) {
                if (err) {
                    res.json({ "Error": true, "Message": "Error executing MySQL query" });
                } else {
                    if (req.body[0].VisitID > '') {
                        var query2 = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, PageContent, TemplateType, Operator) VALUES ?";
                        var records = new Array();
                        var attach_id = result.insertId;
                        for (var j = 0; j < req.body.length; j++) {
                            var rec = new Array();
                            rec.push(req.body[j].VisitID);
                            req.body[j].AttachmentsDate = formatter.format(new Date(req.body[j].AttachmentsDate));
                            rec.push(req.body[j].AttachmentsDate);
                            rec.push(req.body[j].Description);
                            rec.push(attach_id);
                            rec.push(98);
                            rec.push(req.body[j].Operator);
                            records.push(rec);
                            attach_id = attach_id + 2;
                        }
                        query2 = mysql.format(query2, [records]);
                        connection.query(query2, function (err, rows) {
                            if (err) {
                                res.json({ "Error": true, "Message": "Error executing MySQL query" });
                            } else {
                                res.json({ "Error": false, "Message": " Attachments  Added" });
                            }
                        });
                    } else if (req.body[0].AppID > '') {

                        var query2 = "UPDATE appoint SET  Attachments='1' WHERE ID=?";
                        var table2 = [req.body[0].AppID];

                        query2 = mysql.format(query2, table2);
                        connection.query(query2, function (err, rows) {
                            if (err) {
                                res.json({ "Error": true, "Message": "Error executing MySQL query" });
                            } else {
                                res.json({ "Error": false, "Message": "Attachments Updated" });
                            }
                        });
                    }
                    else {
                        res.json({ "Error": false, "Message": "Attachments Updated" });
                    }
                }

            });
        }

    },
    savepatvisitattachment: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE attachments SET  Category=?, Description=?,Interpretation=?  WHERE Attach_ID=?";
            var table = [req.body.Category, req.body.Description,req.body.Interpretation, req.body.Attach_ID];
            var message = "Attachments Updated"
            query = mysql.format(query, table);
            this.executeQuery(connection, res, query, message);
        } else {
            var query = "INSERT INTO attachments (Regn_No, Operator, AttachmentsDate, Category, Description,Interpretation, Filename, CurrentDate,VisitID, AppID) VALUES (?,?,?,?,?,?,?,?,?,?)";
            var table = [req.body.Regn_No, req.body.Operator, formatter.format(new Date(req.body.AttachmentsDate)), req.body.Category, req.body.Description,req.body.Interpretation, req.body.Filename, formatter.format(new Date(req.body.CurrentDate)), req.body.VisitID, req.body.AppID];
            var message = "Attachments  Added"
            query = mysql.format(query, table);
            connection.query(query, function (err, result, fields) {
                if (err) {
                    res.json({ "Error": true, "Message": "Error executing MySQL query" });
                } else {
                    if (req.body.VisitID > '') {
                        var query2 = 'INSERT INTO visitpages (VisitID, PageDate, PageTitle, PageContent, TemplateType, Operator) VALUES (?,?,?,?,98,?)'
                        var table2 = [req.body.VisitID, formatter.format(new Date(req.body.AttachmentsDate)), req.body.Description, result.insertId, req.body.Operator];

                        query2 = mysql.format(query2, table2);
                        connection.query(query2, function (err, rows) {
                            if (err) {
                                res.json({ "Error": true, "Message": "Error executing MySQL query" });
                            } else {
                                res.json({ "Error": false, "Message": " Attachments  Added" });
                            }
                        });
                    } else if (req.body.AppID > '') {
                        var query2 = "UPDATE appoint SET  Attachments='1' WHERE ID=?";
                        var table2 = [req.body.AppID];

                        query2 = mysql.format(query2, table2);
                        connection.query(query2, function (err, rows) {
                            if (err) {
                                res.json({ "Error": true, "Message": "Error executing MySQL query" });
                            } else {
                                res.json({ "Error": false, "Message": "Attachments Updated" });
                            }
                        });
                    }
                    else {
                        res.json({ "Error": false, "Message": "Attachments Updated" });
                    }
                }
            });
        }

    },
    patphoto: function (connection, req, res) {
        var query = "UPDATE patmas SET Photo=? WHERE  Regn_No=?";
        var table = [req.body.Photo, req.body.Regn_No]
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Photo saved" });
            }
        });
    },
    savepatcontact: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE addcont SET Oper=?, Relation=?, Name=?, Mobile=?, Phone=?, Emails=?, Remarks=?, LegalGuardian=? WHERE ID=?";
            var table = [req.body.Oper, req.body.Relation, req.body.Name, req.body.Mobile, req.body.Phone, req.body.Emails, req.body.Remarks, req.body.LegalGuardian, req.body.ID];
            var message = "Contact Updated"
        } else {
            var query = "INSERT INTO addcont (Regn_No, Oper, Relation, Name, Mobile, Phone, Emails, Remarks, LegalGuardian) VALUES (?,?,?,?,?,?,?,?,?)";
            var table = [req.body.Regn_No, req.body.Oper, req.body.Relation, req.body.Name, req.body.Mobile, req.body.Phone, req.body.Emails, req.body.Remarks, req.body.LegalGuardian];
            var message = "Contact  Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    contactdel: function (connection, req, res) {
        var query = "DELETE FROM addcont WHERE ID=" + req.query.ID;
        connection.query(query, [req.query.ID], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Contact Deleted" });
            }
        });
    },
    attachments: function (connection, req, res) {
        if (req.query.VisitID > '') {
            var query = "SELECT * from attachments  where attachments.VisitID='" + req.query.VisitID + "'   order by attachments.category,attachments.AttachmentsDate"
        } else if (req.query.category > '') {
            var query = "SELECT * from attachments  where Category='" + req.query.category + "' and attachments.Regn_No='" + req.query.regn_no + "' order by attachments.AttachmentsDate"
        }
        else if (req.query.apptid) {
            var query = "SELECT * from attachments  where  AppID ='" + req.query.apptid + "' order by attachments.AttachmentsDate"
        }
        else {
            var query = "SELECT * from attachments  where attachments.Regn_No='" + req.query.regn_no + "'  order by attachments.category,attachments.AttachmentsDate"
        }
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'Attachments');
    },
    visitattachments: function (connection, req, res) {
        var query = "SELECT * from attachments  where attachments.Attach_ID='" + req.query.pageContent + "'"
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'VisitAttachments');

    },
    saveFiles: function (connection, req, res) {
        var query = "UPDATE attachments SET Filename=? WHERE  Attach_ID=?";
        var table = [req.body.Filename, req.body.Attach_ID]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'File saved');
    },

    savepatientform: function (connection, req, res, method) {
        var self = this;
        this.getMaxNo('Regn_No', 'patmas', connection).then(function (regn) {
            self.savemypatientform(connection, req, res, method, regn);
        });
    },
    savepatientformPriory: function (connection, req, res, method) {
        var self = this;
        this.getMaxNo('Regn_No', 'patmas', connection).then(function (regn) {
            self.savemypatientformPriory(connection, req, res, method, regn);
        });
    },
    savepatientformDDY: function (connection, req, res, method) {
        var self = this;
        this.getMaxNo('Regn_No', 'patmas', connection).then(function (regn) {
            self.savemypatientformDDY(connection, req, res, method, regn);
        });
    },
    savemypatientform: function (connection, req, res, method, regn) {
        if (method === 'put') {
            var leidexpiry = false;
            var lidexpiry = false;
            var query = "UPDATE patmas SET CustomField1Show=0,Active=?, FirstDate=?, Category=?, Language=?, Title=?, Name=?, Fname=?, Lname=?, DOB=?, Sex=?, Emirate=?, Email=?, HomeTel=?, Mobile=?, Nationality=?, EmiratesID=?, Marital=?, IDNumber=?, School =?,Refby=?, Allergies=?, Comments=?, Visitor=?, PatmasDate=?, Operator=?, Address=?, Occupation=?,Religion=?,Country=?,CustomField1Value=?,CustomField2Value=?,CustomField3Value=?"
            if (req.body.EIDExpiry > '') {
                query += ", EIDExpiry=?"
                leidexpiry = true;
            }
            if (req.body.IDExpiryDate > '') {
                query += ", IDExpiryDate=?"
                lidexpiry = true;
            }
            query += " WHERE Regn_No=" + req.body.Regn_No;
            var table = [req.body.Active, formatter.format(new Date(req.body.FirstDate)), req.body.Category, req.body.Language, req.body.Title, req.body.Name,req.body.Fname,req.body.Lname, formatter.format(new Date(req.body.DOB)), req.body.Sex, req.body.Emirate, req.body.Email, req.body.HomeTel, req.body.Mobile, req.body.Nationality, req.body.EmiratesID, req.body.Marital, req.body.IDNumber, req.body.School, req.body.Refby, req.body.Allergies, req.body.Comments, req.body.Visitor, formatter.format(new Date(req.body.PatmasDate)), req.body.Operator, req.body.Address, req.body.Occupation,req.body.Religion,req.body.Country, req.body.CustomField1Value, req.body.CustomField2Value, req.body.CustomField3Value];
            if (leidexpiry) {
                table.push(formatter.format(new Date(req.body.EIDExpiry)));
            }
            if (lidexpiry) {
                table.push(formatter.format(new Date(req.body.IDExpiryDate)));
            }
            // table.push(req.body.Regn_No);
            query = mysql.format(query, table);
            if (req.query.appointid > 0) {
                query += ";UPDATE appoint SET Name='"+req.body.Name+"'  where ID = "+req.query.appointid;
            }
            var message = req.body.Regn_No
            this.executeQuery(connection, res, query, message);
        } else {
            var leidexpiry = false;
            var lidexpiry = false;
            var query = "INSERT INTO patmas (Regn_No, CustomField1Show,Active,Location, FirstDate, Category, Language, Title, Name,Fname,Lname, DOB, Sex, Emirate, Email, HomeTel, Mobile, Nationality, EmiratesID, Marital, IDNumber,School, Refby, Allergies, Comments,Visitor, PatmasDate,Operator, Photo,Address,Occupation,Religion,Country,CustomField1Value,CustomField2Value,CustomField3Value "
            if (req.body.EIDExpiry > '') {
                query += ", EIDExpiry"
                leidexpiry = true;
            }
            if (req.body.IDExpiryDate > '') {
                query += ", IDExpiryDate"
                lidexpiry = true;
            }
            query += ")VALUES (" + regn + ",0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
            if (leidexpiry) {
                query += ", ?"
            }
            if (lidexpiry) {
                query += ", ?"
            }
            query += ")";
            var table = [req.body.Active, req.body.Location,formatter.format(new Date(req.body.FirstDate)), req.body.Category, req.body.Language, req.body.Title, req.body.Name,req.body.Fname,req.body.Lname, formatter.format(new Date(req.body.DOB)), req.body.Sex, req.body.Emirate, req.body.Email, req.body.HomeTel, req.body.Mobile, req.body.Nationality, req.body.EmiratesID, req.body.Marital, req.body.IDNumber, req.body.School, req.body.Refby, req.body.Allergies, req.body.Comments, req.body.Visitor, formatter.format(new Date(req.body.PatmasDate)), req.body.Operator, req.body.Photo, req.body.Address, req.body.Occupation,req.body.Religion,req.body.Country,req.body.CustomField1Value,req.body.CustomField2Value,req.body.CustomField3Value];
            if (leidexpiry) {
                table.push(formatter.format(new Date(req.body.EIDExpiry)));
            }
            if (lidexpiry) {
                table.push(formatter.format(new Date(req.body.IDExpiryDate)));
            }
            query = mysql.format(query, table);
            connection.query(query, function (err, result, fields) {
                if (err) {
                    res.json({ "Error": true, "Message": err.sqlMessage });
                } else {
                    if (req.query.appointid > 0) {
                        var query1 = "UPDATE appoint SET Name='"+ req.body.Name +"',Phone='"+ req.body.Mobile +"',Regn_No='"+ regn +"' where ID = "+ req.query.appointid;
                        query1 += ";UPDATE receipts set Regn_No=" + regn+ " where appID = "+req.query.appointid;
                        var regno = regn;
 
                        connection.query(query1, function (err, result, fields) {
                            if (err) {
                                res.json({ "Error": true, "Message": err.sqlMessage });
                            } else {
                                if (req.query.attach) {
                                    var query1 = "UPDATE attachments SET Regn_No=? where AppID = ?";
                                    var table1 = [regno, req.query.appointid];
                                    var regno1 = regno;
                                    query = mysql.format(query1, table1);
                                    connection.query(query, function (err, result, fields) {
                                        if (err) {
                                            res.json({ "Error": true, "Message": err.sqlMessage });
                                        } else {
                                            res.json({ "Error": false, "Message": regno });
                                        }
                                    });
                                } else {
                                    res.json({ "Error": false, "Message": regno });
                                }
                            }
                        });
                    } else {
                        res.json({ "Error": false, "Message": regn });
                    }
                }
            });
        }
    },
    savemypatientformPriory: function (connection, req, res, method, regn) {
        if (method === 'put') {
            var leidexpiry = false;
            var lidexpiry = false;
            var query = "UPDATE patmas SET CustomField1Show=0,Active=?, FirstDate=?, Category=?, Language=?, Title=?, Name=?,Fname=?, Lname=?, DOB=?, Sex=?, Emirate=?, Email=?, HomeTel=?, Mobile=?, Nationality=?, EmiratesID=?, Marital=?, IDNumber=?, School =?,Refby=?, Allergies=?, Comments=?, Visitor=?, PatmasDate=?, Operator=?, Address=?, Occupation=?,Religion=?,Country=?,Psychiatrist=?,  Counsellor=?"
            if (req.body.EIDExpiry > '') {
                query += ", EIDExpiry=?"
                leidexpiry = true;
            }
            if (req.body.IDExpiryDate > '') {
                query += ", IDExpiryDate=?"
                lidexpiry = true;
            }
            query += " WHERE Regn_No=" + req.body.Regn_No;
            var table = [req.body.Active, formatter.format(new Date(req.body.FirstDate)), req.body.Category, req.body.Language, req.body.Title, req.body.Name,req.body.Fname,req.body.Lname, formatter.format(new Date(req.body.DOB)), req.body.Sex, req.body.Emirate, req.body.Email, req.body.HomeTel, req.body.Mobile, req.body.Nationality, req.body.EmiratesID, req.body.Marital, req.body.IDNumber, req.body.School, req.body.Refby, req.body.Allergies, req.body.Comments, req.body.Visitor, formatter.format(new Date(req.body.PatmasDate)), req.body.Operator, req.body.Address, req.body.Occupation,req.body.Religion,req.body.Country,req.body.Psychiatrist,req.body. Counsellor];
            if (leidexpiry) {
                table.push(formatter.format(new Date(req.body.EIDExpiry)));
            }
            if (lidexpiry) {
                table.push(formatter.format(new Date(req.body.IDExpiryDate)));
            }
            // table.push(req.body.Regn_No);
            query = mysql.format(query, table);
            if (req.query.appointid > 0) {
                query += ";UPDATE appoint SET Name='"+req.body.Name+"'  where ID = "+req.query.appointid;
            }
            var message = req.body.Regn_No
            this.executeQuery(connection, res, query, message);
        } else {
            var leidexpiry = false;
            var lidexpiry = false;
            var query = "INSERT INTO patmas (Regn_No,CustomField1Show, Active, FirstDate, Category, Language, Title, Name,Fname,Lname, DOB, Sex, Emirate, Email, HomeTel, Mobile, Nationality, EmiratesID, Marital, IDNumber,School, Refby, Allergies, Comments,Visitor, PatmasDate,Operator, Photo,Address,Occupation,Religion,Country,Psychiatrist, Counsellor "
            if (req.body.EIDExpiry > '') {
                query += ", EIDExpiry"
                leidexpiry = true;
            }
            if (req.body.IDExpiryDate > '') {
                query += ", IDExpiryDate"
                lidexpiry = true;
            }
            query += ")VALUES (" + regn + ",0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
            if (leidexpiry) {
                query += ", ?"
            }
            if (lidexpiry) {
                query += ", ?"
            }
            query += ")";
            var table = [req.body.Active, formatter.format(new Date(req.body.FirstDate)), req.body.Category, req.body.Language, req.body.Title, req.body.Name,req.body.Fname,req.body.Lname, formatter.format(new Date(req.body.DOB)), req.body.Sex, req.body.Emirate, req.body.Email, req.body.HomeTel, req.body.Mobile, req.body.Nationality, req.body.EmiratesID, req.body.Marital, req.body.IDNumber, req.body.School, req.body.Refby, req.body.Allergies, req.body.Comments, req.body.Visitor, formatter.format(new Date(req.body.PatmasDate)), req.body.Operator, req.body.Photo, req.body.Address, req.body.Occupation,req.body.Religion,req.body.Country, req.body.Psychiatrist,req.body. Counsellor];
            if (leidexpiry) {
                table.push(formatter.format(new Date(req.body.EIDExpiry)));
            }
            if (lidexpiry) {
                table.push(formatter.format(new Date(req.body.IDExpiryDate)));
            }
            query = mysql.format(query, table);
            connection.query(query, function (err, result, fields) {
                if (err) {
                    res.json({ "Error": true, "Message": err.sqlMessage });
                } else {
                    if (req.query.appointid > 0) {
                        var query1 = "UPDATE appoint SET Name=?,Phone=?,Regn_No=? where ID = ?";
                        var table1 = [req.body.Name, req.body.Mobile, regn, req.query.appointid];
                        var regno = regn;
                        query = mysql.format(query1, table1);
                        connection.query(query, function (err, result, fields) {
                            if (err) {
                                res.json({ "Error": true, "Message": err.sqlMessage });
                            } else {
                                if (req.query.attach) {
                                    var query1 = "UPDATE attachments SET Regn_No=? where AppID = ?";
                                    var table1 = [regno, req.query.appointid];
                                    var regno1 = regno;
                                    query = mysql.format(query1, table1);

                                    connection.query(query, function (err, result, fields) {
                                        if (err) {
                                            res.json({ "Error": true, "Message": err.sqlMessage });
                                        } else {
                                            res.json({ "Error": false, "Message": regno });
                                        }
                                    });
                                } else {
                                    res.json({ "Error": false, "Message": regno });
                                }
                            }
                        });
                    } else {
                        res.json({ "Error": false, "Message": regn });
                    }
                }
            });
        }
    },
    savemypatientformDDY: function (connection, req, res, method, regn) {
        if (method === 'put') {
            var leidexpiry = false;
            var lidexpiry = false;
            var query = "UPDATE patmas SET CustomField1Show=0,Active=?, FirstDate=?, Category=?, Language=?, Title=?, Name=?,Fname=?, Lname=?, DOB=?, Sex=?, Emirate=?, Email=?, HomeTel=?, Mobile=?, Nationality=?, EmiratesID=?, Marital=?, IDNumber=?, School =?,Refby=?, Allergies=?, Comments=?, Visitor=?, PatmasDate=?, Operator=?, Address=?, Occupation=?,Religion=?,Country=?,CustomField1Value=?,CustomField2Value=?,CustomField3Value=?"
            if (req.body.EIDExpiry > '') {
                query += ", EIDExpiry=?"
                leidexpiry = true;
            }
            if (req.body.IDExpiryDate > '') {
                query += ", IDExpiryDate=?"
                lidexpiry = true;
            }
            query += " WHERE Regn_No=" + req.body.Regn_No;
            var table = [req.body.Active, formatter.format(new Date(req.body.FirstDate)), req.body.Category, req.body.Language, req.body.Title, req.body.Name,req.body.Fname,req.body.Lname, formatter.format(new Date(req.body.DOB)), req.body.Sex, req.body.Emirate, req.body.Email, req.body.HomeTel, req.body.Mobile, req.body.Nationality, req.body.EmiratesID, req.body.Marital, req.body.IDNumber, req.body.School, req.body.Refby, req.body.Allergies, req.body.Comments, req.body.Visitor, formatter.format(new Date(req.body.PatmasDate)), req.body.Operator, req.body.Address, req.body.Occupation,req.body.Religion,req.body.Country, req.body.CustomField1Value, req.body.CustomField2Value, req.body.CustomField3Value];
            if (leidexpiry) {
                table.push(formatter.format(new Date(req.body.EIDExpiry)));
            }
            if (lidexpiry) {
                table.push(formatter.format(new Date(req.body.IDExpiryDate)));
            }
            // table.push(req.body.Regn_No);
            query = mysql.format(query, table);
            if (req.query.appointid > 0) {
                query += ";UPDATE appoint SET Name='"+req.body.Name+"'  where ID = "+req.query.appointid;
            }
            var message = req.body.Regn_No
            this.executeQuery(connection, res, query, message);
        } else {
            var leidexpiry = false;
            var lidexpiry = false;
            var query = "INSERT INTO patmas (Regn_No, CustomField1Show,Active, FirstDate, Category, Language, Title, Name,Fname,Lname, DOB, Sex, Emirate, Email, HomeTel, Mobile, Nationality, EmiratesID, Marital, IDNumber,School, Refby, Allergies, Comments,Visitor, PatmasDate,Operator, Photo,Address,Occupation,Religion,Country,CustomField1Value,CustomField2Value,CustomField3Value "
            if (req.body.EIDExpiry > '') {
                query += ", EIDExpiry"
                leidexpiry = true;
            }
            if (req.body.IDExpiryDate > '') {
                query += ", IDExpiryDate"
                lidexpiry = true;
            }
            query += ")VALUES (" + regn + ",0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
            if (leidexpiry) {
                query += ", ?"
            }
            if (lidexpiry) {
                query += ", ?"
            }
            query += ")";
            var table = [req.body.Active, formatter.format(new Date(req.body.FirstDate)), req.body.Category, req.body.Language, req.body.Title, req.body.Name,req.body.Fname,req.body.Lname, formatter.format(new Date(req.body.DOB)), req.body.Sex, req.body.Emirate, req.body.Email, req.body.HomeTel, req.body.Mobile, req.body.Nationality, req.body.EmiratesID, req.body.Marital, req.body.IDNumber, req.body.School, req.body.Refby, req.body.Allergies, req.body.Comments, req.body.Visitor, formatter.format(new Date(req.body.PatmasDate)), req.body.Operator, req.body.Photo, req.body.Address, req.body.Occupation,req.body.Religion,req.body.Country,req.body.CustomField1Value,req.body.CustomField2Value,req.body.CustomField3Value];
            if (leidexpiry) {
                table.push(formatter.format(new Date(req.body.EIDExpiry)));
            }
            if (lidexpiry) {
                table.push(formatter.format(new Date(req.body.IDExpiryDate)));
            }
            query = mysql.format(query, table);
            connection.query(query, function (err, result, fields) {
                if (err) {
                    res.json({ "Error": true, "Message": err.sqlMessage });
                } else {
                    var query3 = "INSERT INTO messages (MsgTo,MessageText,MsgFrom,Status, MsgDate) VALUES ('Administrator','New Registraion done with Regn no: " + regn +"("+ req.body.Name +")',?,?,?)";
                    var table3 = [req.body.Operator, 0, formatter.format(new Date(req.body.FirstDate))];
                    query3 = mysql.format(query3, table3);
                    connection.query(query3, function (err, result, fields) {
                        if (err) {
                           console.log(err.sqlMessage);
                        } else {
                        }
                    });
                    if (req.query.appointid > 0) {
                        var query1 = "UPDATE appoint SET Name=?,Phone=?,Regn_No=? where ID = ?";
                        var table1 = [req.body.Name, req.body.Mobile, regn, req.query.appointid];
                        var regno = regn;
                        query = mysql.format(query1, table1);
                        connection.query(query, function (err, result, fields) {
                            if (err) {
                                res.json({ "Error": true, "Message": err.sqlMessage });
                            } else {
                                if (req.query.attach) {
                                    var query1 = "UPDATE attachments SET Regn_No=? where AppID = ?";
                                    var table1 = [regno, req.query.appointid];
                                    var regno1 = regno;
                                    query = mysql.format(query1, table1);
                                    connection.query(query, function (err, result, fields) {
                                        if (err) {
                                            res.json({ "Error": true, "Message": err.sqlMessage });
                                        } else {
                                            res.json({ "Error": false, "Message": regno });
                                        }
                                    });
                                } else {
                                    res.json({ "Error": false, "Message": regno });
                                }
                            }
                        });
                    } else {
                        res.json({ "Error": false, "Message": regn });
                    }
                }
            });
        }
    },
    deleteAttachment: function (connection, req, res) {
        var query = "DELETE FROM attachments WHERE Attach_ID=" + req.query.ID;
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                if (req.query.attachdel === 'true') {

                    var query2 = "DELETE FROM visitpages WHERE PageContent=" + req.query.ID;
                    query2 = mysql.format(query2);
                    connection.query(query2, function (err, rows) {
                        if (err) {
                            res.json({ "Error": true, "Message": "Error executing MySQL query" });
                        } else {
                            res.json({ "Error": false, "Message": "Attachment Deleted" });
                        }
                    });
                } else {
                    res.json({ "Error": false, "Message": "Attachment Deleted" });

                }
            }
        });
    },
    attachcategories: function (connection, req, res) {
        var query = "SELECT * FROM attachmentscategory ";
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'AttchCategory');
    },
    saveattachcategory: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE attachmentscategory SET Category=?, Usergroups=? WHERE id=?";
            var table = [req.body.Category, req.body.Usergroups, req.body.ID];
            var message = "Attachments category Updated"
        } else {
            var query = "INSERT INTO attachmentscategory ( Category, Usergroups) VALUES (?,?)";
            var table = [req.body.Category, req.body.Usergroups];
            var message = "Attachments category Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    deleteAttachCategory: function (connection, req, res) {
        var query = "DELETE FROM attachmentscategory WHERE ID=" + req.query.ID;
        this.executeQuery(connection, res, query, 'Attachmentcategory Deleted');
    },
    patcategory: function (connection, req, res) {
        var message = "Success"
        if (req.query.category) {
            var query = "SELECT * FROM categories where Category ='" + req.query.category + "'";
        } else {
            var query = "SELECT * FROM categories ORDER BY ID";
        }
        this.executeQuery(connection, res, query, message, 'patient-category');
    },
    savecategory: function (connection, req, res, method) {
        if (method === 'put') {
            if (req.body.DiscPercent !== null && req.body.DiscPercent !== undefined) {
                var query = "UPDATE categories SET Category=?, DiscPercent =? WHERE ID=?";
                var table = [req.body.Category,req.body.DiscPercent, req.body.ID];    
            } else {
                var query = "UPDATE categories SET Category=? WHERE ID=?";
                var table = [req.body.Category, req.body.ID];    
            }
            var message = "Category Updated"
        } else {
            if (req.body.DiscPercent !== null && req.body.DiscPercent !== undefined) {
                var query = "INSERT INTO categories (Category,DiscPercent) VALUES (?,?)";
                var table = [req.body.Category,req.body.DiscPercent];
            } else {
                var query = "INSERT INTO categories (Category) VALUES (?)";
                var table = [req.body.Category];
            }
            var message = "Category  Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    deleteCategory: function (connection, req, res) {
        var query = "DELETE FROM categories WHERE ID=" + req.query.ID;
        this.executeQuery(connection, res, query, 'patient-category-deleted');
    },
    updvisitpage: function (connection, req, res, method) {
        var query = "insert into visitpages(VisitID, PageDate, PageTitle, PageContent, TemplateType, Operator) VALUES ?";
        var records = new Array();
        for (var i = 0; i < req.body.length; i++) {
            var rec = new Array();
            req.body[i].PageDate = formatter.format(new Date());
            req.body[i].TemplateType = '98';
            rec.push(req.body[i].VisitID);
            rec.push(req.body[i].PageDate);
            rec.push(req.body[i].Description);
            rec.push(req.body[i].Attach_ID);
            rec.push(req.body[i].TemplateType);
            rec.push(req.body[i].Operator);

            records.push(rec);
        }
        query = mysql.format(query, [records]);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": 'aaa', 'Items': rows });
            }
        });
    },
    similarpatfiles: function (connection, req, res) {
        var message = "Success"
        var query = "SELECT * FROM patmas where Mobile='" + req.query.mobile + "'and Email='" + req.query.email + "'and EmiratesID='" + req.query.eid + "' and Active='1' and Regn_No <> '" + req.query.regno + "'   ";
        this.executeQuery(connection, res, query, message, 'similarPatient');
    },
    mergePatFiles: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE patmas SET Name=?, Active='0' WHERE Regn_No=?";
            req.body.Name += '_Merged';
            var table = [req.body.Name, req.body.Regn_No];
            var message = "similar files"
        }
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query3 = "update appoint set appoint.Name ='" + req.query.orginalname + "' WHERE appoint.Regn_No ='" + req.body.Regn_No + "'"
                query3 = mysql.format(query3);
                connection.query(query3, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": "Error executing MySQL query" });
                    } else {
                        var table2 = ['attachments', 'patcomm', 'opdbill', 'receipts', 'vouchers', 'visits', 'addcont', 'appoint', 'proforma', 'procedures'];
                        for (var i = 0; i < table2.length; i++) {
                            var query2 = "update " + table2[i] + " set " + table2[i] + ".Regn_No ='" + req.query.orginalregn_no + "' WHERE " + table2[i] + ".Regn_No ='" + req.body.Regn_No + "'"
                            query2 = mysql.format(query2);
                            connection.query(query2, function (err, rows) {
                                if (err) {
                                    res.json({ "Error": true, "Message": "Error executing MySQL query" });
                                } else {
                                }
                            });
                        }
                    }
                });
            }
        });

    },
    eid: function (connection, req, res) {
        var query = "select * from eid where IDType in('identity card','IL','P','ID','IR') AND ReaderID='" + req.query.compName + "' order by readdate desc LIMIT 1"
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'EID');
    },
    pateid: function (connection, req, res) {
        if (req.query.eid > '') {
            var query = "select * from patmas where EmiratesID='" + req.query.eid + "'"
        } else {
            var query = "select * from patmas where IDNumber='" + req.query.cardno + "'"
        }
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'PatEID');
    },
    patdetailseid: function (connection, req, res) {
        var query = "select * from patmas where dob=Date(?) and sex=? and nationality=?"
        var table = [formatter.format(new Date(req.query.dob)), req.query.gender, req.query.national];
        query = mysql.format(query, table);
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'PatEID');
    },
    compnames: function (connection, req, res) {
        var query = "select distinct ReaderID from eid"
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'compnames');
    },
    // Dr.Elsa clinic
    patdetailseidwithname: function (connection, req, res) {
        var query = "select * from patmas where dob=Date(?) and sex=? and nationality=? and Name like '" + req.query.name + "%'"
        var table = [formatter.format(new Date(req.query.dob)), req.query.gender, req.query.national];
        query = mysql.format(query, table);
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'PatEID');
    },

    linkfamily: function (connection, req, res) {
        var query = "SELECT Family.*, PatMas.Title, PatMas.Name, PatMas.Balance FROM Family LEFT JOIN PatMas ON Family.Other_No = PatMas.Regn_No WHERE Family.Regn_No ='" + req.query.Regn_No + "'"
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'Link');
    },
    updeidexpiry: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE patmas SET EIDExpiry=? WHERE Regn_No=?";
            var table = [formatter.format(new Date(req.query.expiry)), req.body.Regn_No];
            query = mysql.format(query, table);
            var message = "EID Expiry Updated"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    updateeidimagefld: function (connection, req, res, method) {
        var query = "UPDATE patmas SET EIDImage=? WHERE Regn_No=?";
        var table = [req.body.eidimage,req.body.Regn_No];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query);
    },
    savepatLink: function (connection, req, res) {
        var query = "DELETE FROM Family WHERE Regn_No=" + req.body[0].Regn_No + " and Other_No = " + req.body[0].Other_No + "";
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query = "DELETE FROM Family WHERE Regn_No=" + req.body[1].Regn_No + " and Other_No = " + req.body[1].Other_No + "";
                connection.query(query, function (err, result, fields) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        var query2 = "INSERT INTO Family (Regn_No, Other_No, Remarks) VALUES ?";
                        var records = new Array();
                        for (var i = 0; i < req.body.length; i++) {
                            var rec = new Array();
                            rec.push(req.body[i].Regn_No);
                            rec.push(req.body[i].Other_No);
                            rec.push(req.body[i].Remarks);

                            records.push(rec);
                        }
                        connection.query(query2, [records], function (err, rows) {
                            if (err) {
                                res.json({ "Error": true, "Message": err.sqlMessage });
                            } else {
                                res.json({ "Error": false, "Message": 'patient file linked', 'Items': rows });
                            }
                        });
                    }
                });


            }
        });
    },
    deletePatLink: function (connection, req, res) {
        var query = "DELETE FROM Family WHERE ID=" + req.query.ID;
        connection.query(query, [req.query.ID], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Success", "Message": "Link Deleted" });
            }
        });
    },
    getsoapins: function (connection, req, res) {
        var query = "SELECT * FROM soapins WHERE Regn_No='" + req.query.regn_no + "' ORDER BY  ID";
        this.executeQuery(connection, res, query, 'Success', 'soapins');
    },
    getcurrentreminders: function (connection, req, res) {
        if (req.query.regn_no) {
            var query = "SELECT * FROM patreminders WHERE DueDate > CURDATE() - INTERVAL 5 DAY AND DueDate <= CURDATE() + INTERVAL 2 DAY AND Regn_No=" + req.query.regn_no + " AND Completed = 0 AND Cancelled = 0";
        } else {
            var query = "SELECT * FROM patreminders WHERE DueDate = CURDATE() AND Completed = 0 AND Cancelled = 0";
        }
        this.executeQuery(connection, res, query, 'Success', 'PatReminders');
    },

    changeCompleted: function (connection, req, res) {
        var complete = 1;
        if (req.body.Completed) { complete = 0 };
        var query = "UPDATE patreminders SET Completed=" + complete + " WHERE ID=" + req.body.ID;
        this.executeQuery(connection, res, query, 'Reminder updated');
    },

    getpatreminders: function (connection, req, res) {
        if (req.query.regn_no) {
            var query = "SELECT * FROM patreminders WHERE Regn_No='" + req.query.regn_no + "' AND Cancelled = 0 ORDER BY DueDate";
        } else {
            var query = "SELECT patreminders.*,patmas.Name FROM patreminders join patmas on patreminders.Regn_No = patmas.Regn_No and  Cancelled = 0 and DueDate =  CURDATE() order by patreminders.PatRemindersDate";
        }
        this.executeQuery(connection, res, query, 'Success', 'PatReminders');
    },
    savepatreminders: function (connection, req, res) {
        var query = "INSERT INTO patreminders (Regn_No, Description, DueDate, Operator, PatRemindersDate) VALUES (?,?,?,?,?)";
        var table = [req.body.Regn_No, req.body.Description, formatter.format(new Date(req.body.DueDate)), req.body.Operator, formatter.format(new Date(req.body.PatRemindersDate))];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Reminder added');

    },

    deletepatreminder: function (connection, req, res) {
        var query = "UPDATE patreminders SET Cancelled = 1 WHERE ID=" + req.query.ID;
        this.executeQuery(connection, res, query, 'Reminder cancelled');
    },

    checkmobile: function (connection, req, res) {
        var query = "SELECT * FROM patmas WHERE Mobile = '" + req.query.mobile + "' ";
        this.executeQuery(connection, res, query, 'Success', 'Patients');
    },
    checkemail: function (connection, req, res) {
        var query = "SELECT * FROM patmas WHERE Email = '" + req.query.email + "' ";
        this.executeQuery(connection, res, query, 'Success', 'Patients');
    },
    getMaxNo: async function (field, table, conn) {
        var query = "SELECT MAX(" + field + ") + 1 AS ID FROM " + table;
        let promise = new Promise((resolve, reject) => {
            conn.query(query, function (err, result, _max) {
                if (err) { reject(err); }
                resolve(result[0].ID);
            });
        });
        return await promise;
    },
    scannedcard: function (connection, req, res) {
        var query = "select * from eid where (IDType  = '' or IDTYPE= 'UNKNOWN DOCUMENT') AND ReaderID='" + req.query.compName + "' order by readdate desc LIMIT 1";
        var message = "Success"
        this.executeQuery(connection, res, query, message, 'scannedcard');
    },
    geteligibilitys: function (connection, req, res) {
        var query = "SELECT * FROM eligibility WHERE Regn_No='" + req.query.regn_no + "'";
        if (req.query.noInvNo === 'true') {
            query += " AND InvoiceNo IS null";   
        }
        query += " ORDER BY ID desc"; 
        this.executeQuery(connection, res, query, 'Success', 'Eligibilitys');
    },

    saveeligibility: function (connection, req, res,method) {
        if (method === 'put') {
            var query = "UPDATE eligibility SET Regn_No=?, EligibilityDate=?, Operator=?, EligibilityNo=? WHERE ID=?";
            var message = "Eligibility Updated"
        } else {
            var query = "INSERT INTO eligibility (Regn_No, EligibilityDate, Operator, EligibilityNo) VALUES (?,?,?,?)";
            var message = "Eligibility Added"
        }
        var table = [req.body.Regn_No, formatter.format(new Date(req.body.EligibilityDate)), req.body.Operator, req.body.EligibilityNo, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    updateEligibiltyInvNo: function (connection, req, res) {
        var query = "UPDATE eligibility SET InvoiceNo=? WHERE ID=?";
        var table = [req.body.invno, req.body.id];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Eligibilty Updated');
    },

    marital: function (connection, req, res) {
        if (req.query.marital) {
            var query = "select Description FROM Marital where Code='" + req.query.marital + "'";
        } else {
            var query = "SELECT Description FROM Marital ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'Marital');

    },

    relation: function (connection, req, res) {
        if (req.query.relation) {
            var query = "select Description FROM relation where Code='" + req.query.relation + "'";
        } else {
            var query = "SELECT Description FROM relation ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'Relation');
    },
    religion: function (connection, req, res) {
        if (req.query.religion) {
            var query = "select Description FROM religion where Code='" + req.query.religion + "'";
        } else {
            var query = "SELECT Description FROM religion ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'Religion');

    },
	imageattachments: function (connection, req, res) {
        var query ="SELECT patmas.name,attachments.* from attachments JOIN appoint ON  attachments.regn_no = appoint.regn_no JOIN patmas ON attachments.Regn_No = patmas.regn_no WHERE appoint.cancelled = 0 and appoint.STATUS >= 10 and appoint.DrName='" + req.query.doctor + "' AND (filename LIKE '%.jpg' OR filename LIKE '%.png' OR filename LIKE '%.jpeg' OR filename LIKE '%.gif')"
        if (req.query.fromdate) {
            query += " and DATE(apptime) >= date('" + formatter.format(new Date(req.query.fromdate)) + "')"
        } 
        if (req.query.todate) {
            query += " and DATE(apptime) <= date('" + formatter.format(new Date(req.query.todate)) + "')"
        }
        if (req.query.filter) {
            query += " AND (description NOT LIKE '%consent%'  and  description NOT LIKE '%form%')" 
        }
        query += " order by attachments.Regn_no,attachments.AttachmentsDate DESC";
         var message = "Success"
        this.executeQuery(connection, res, query, message, 'imageAttachments');
    },


    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }

}
