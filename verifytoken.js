var jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
    var token = req.headers['x-access-token'];
    if (!token) {
        return res.status(500).send({ auth: false, message: 'No token provided'});
    }
    jwt.verify(token, env.database, function(err, decoded) {
        if (err) {
            return res.status(500).send({auth: false, message: 'Token authentication failed'});
        }
        req.user = decoded;
        next();
    });

}
module.exports = verifyToken;
