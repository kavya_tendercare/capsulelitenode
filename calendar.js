var mysql = require("mysql");
var request = require("request");
var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};
// var formatter = new Intl.DateTimeFormat(['en-GB'], options);
var formatter = require("./formatter");
var dtFrom, dtTo


module.exports = {

    timings: function (connection, req, res) {
        var query = "SELECT * FROM Holidays WHERE DateFrom <= '" + req.query.date + "' AND DateTo >='" + req.query.date + "' AND (Name='" + req.query.doctor + "' OR Name='All')"
        connection.query(query, function (err, result) {
            if (err) throw err;
            if (result.length) {
                res.json({ "Error": false, "Message": "Holiday", "Holiday": [{ "Holiday": result[0].Holiday }] });
            } else {
                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                var nDay = new Date(req.query.date).getDay();
                query = "SELECT * FROM timings WHERE Name='" + req.query.doctor + "' AND Day='" + days[nDay] + "'"
                connection.query(query, [req.query.doctor], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err });
                    } else {
                        res.json({ "Error": false, "Message": "Success", "Timings": rows });
                    }
                });
            }
        });
    },

    daytimings: function (connection, req, res) {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var nDay = new Date(req.query.date).getDay();
        query = "SELECT Min(Start1) AS Start1, Max(End1) AS End1, Max(End2) AS End2, Min(Slot) AS Slot FROM timings WHERE Day='" + days[nDay] + "'"
        connection.query(query, [req.query.doctor], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err });
            } else {
                res.json({ "Error": false, "Message": "Success", "Timings": rows });
            }
        });
    },

    rooms: function (connection, req, res) {
        var query = "SELECT * FROM rooms WHERE Equipment=" + req.query.equipment + " ORDER BY Name"
        this.executeQuery(connection, res, query, 'Success', 'Rooms');
    },

    saveroom: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE rooms SET Name=?, Equipment=? WHERE ID=" + req.body.ID;
            var message = "Room or Equipment Updated"
        } else {
            var query = "INSERT INTO Rooms (Name, Equipment) VALUES(?,?)";
            var message = "Room or Equipment Added"
        }
        var table = [req.body.Name, req.body.Equipment]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    delroom: function (connection, req, res) {
        var query = "DELETE FROM Rooms WHERE id=" + req.query.ID;
        this.executeQuery(connection, res, query, 'Room Deleted');
    },

    appointments: function (connection, req, res) {
        if (req.query.search) {
            var query = "SELECT appoint.*, patmas.Category, patmas.EIDExpiry, patmas.Balance,patmas.DOB FROM appoint left join patmas on appoint.regn_no=patmas.regn_no WHERE appoint.Name LIKE'%" + req.query.search + "%' OR appoint.Phone LIKE '%" + req.query.search + "%' ORDER BY AppTime, Status";
        } else if (req.query.all) {
            var query = "SELECT appoint.*, patmas.Category, patmas.EIDExpiry, patmas.Balance,patmas.DOB FROM appoint left join patmas on appoint.regn_no=patmas.regn_no WHERE appoint.AppTime LIKE '" + req.query.date + "%' AND appoint.Cancelled=" + req.query.cancelled + " ORDER BY AppTime, Status";
        }else if (req.query.fromdatetime) {
            var query = "SELECT appoint.*, patmas.Category, patmas.EIDExpiry, patmas.Balance,patmas.DOB FROM appoint left join patmas on appoint.regn_no=patmas.regn_no WHERE appoint.DrName='" + req.query.doctor + "' AND appoint.AppTime >= '" + req.query.fromdatetime + "' AND appoint.AppTime <= '" + req.query.todatetime + "'  AND appoint.Cancelled=" + req.query.cancelled + " ORDER BY AppTime, Status";

        } else {
            var query = "SELECT appoint.*, patmas.Category, patmas.EIDExpiry, patmas.Balance,patmas.DOB FROM appoint left join patmas on appoint.regn_no=patmas.regn_no WHERE appoint.DrName='" + req.query.doctor + "' AND appoint.AppTime LIKE '" + req.query.date + "%' AND appoint.Cancelled=" + req.query.cancelled + " ORDER BY AppTime, Status";
        }
        this.executeQuery(connection, res, query, 'Success', 'Appointments');
    },

    appointment: function (connection, req, res) {
        var query = "SELECT * FROM appoint WHERE Regn_No=" + req.query.regn_no + " AND AppTime LIKE '" + req.query.date + "%' AND Cancelled = 0";
        this.executeQuery(connection, res, query, 'Success', 'Appointments');
    },

    checkdocappoint: function (connection, req, res) {
        if (req.query.date) {
            var query = "SELECT * FROM appoint WHERE Regn_No=" + req.query.regn_no + " AND DrName='" + req.query.doc + "' and  AppTime LIKE '" + req.query.date + "%' AND Cancelled = 0";
        } else {
            var query = "SELECT * FROM appoint WHERE Regn_No=" + req.query.regn_no + " AND DrName='" + req.query.doc + "' AND Cancelled = 0";
        }
        this.executeQuery(connection, res, query, 'Success', 'Appointments');
    },

    appointhistory: function (connection, req, res) {
        var query = "SELECT * FROM appoint WHERE Regn_No='" + req.query.regn_no + "'"
        connection.query(query, [req.query.regn_no], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err });
            } else {
                res.json({ "Error": false, "Message": "Success", "Appointments": rows });
            }
        });
    },

    appointlog: function (connection, req, res) {
        var query = "SELECT * FROM appointmentLog WHERE AppID=" + req.query.appid + " ORDER BY appointmentlogdate"
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err });
            } else {
                res.json({ "Error": false, "Message": "Success", "Log": rows });
            }
        });
    },

    jobtypes: function (connection, req, res) {
        if (req.query.dept > '') {
            var query = "SELECT * FROM jobtypes WHERE department='" + req.query.dept + "' OR department='All' ORDER BY Purpose"
        } else {
            var query = "SELECT * FROM jobtypes WHERE department='All' ORDER BY Purpose"
        }
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err });
            } else {
                res.json({ "Error": false, "Message": "Success", "Jobtypes": rows });
            }
        });
    },
    apptpurposes: function (connection, req, res) {
        if (req.query.dept > '') {
            var query = "SELECT * FROM jobtypes  WHERE department='" + req.query.dept + "'ORDER BY Purpose";
        } else {
            var query = "SELECT * FROM jobtypes  ORDER BY Purpose"
        }
        this.executeQuery(connection, res, query, 'Success', 'Jobtypes');
    },

    appstatus: function (connection, req, res) {
        var cancel = 0;
        var description = '';
        var self = this;
        if (req.body.Cancelled) { 
            cancel = 1;
            description = 'Cancelled';
         } else {
            cancel = 0;
            description = 'Status Change';
        }
        var query = "UPDATE appoint SET oper=?, status=?, cancelled=" + cancel + ", remarks=? WHERE ID=?"
        query += ';INSERT INTO appointmentlog (AppointmentLogDate, AppID, Description, Status, Oper) VALUES(?,?,?,?,?)';
        var table = [req.body.Oper, req.body.Status, req.body.Remarks, req.body.ID,formatter.format(new Date(req.body.CurrentDate)), req.body.ID, description, req.body.Status, req.body.Oper];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Appointment Updated" });
                if (req.body.CreatedBy === 'OkaDoc' && cancel === 1) {
                    self.okadocCancel(req.body, res);
                }
            }
        });
    },
    okadocCancel: function (req, res) {
        var reason = req.Remarks;
        var okabody =  '{"reason":  "'+ reason.replace("\n", "")+ '"}'
        request.post({
            headers: {'content-type' : 'application/json', 'Authorization':'Basic NE5HdXRlZ3E3RzdZWWNWRzozNlYzOlduals7YjRLYmpH'},
            url:     'https://service.okadoc.com/appointment/v1/3rdparty/cancel/'+ env.estId + '-' + req.ID,
            body:    okabody
            }, (error, response, body) => {
                if (error) {
                    console.log(error);
                } else {
                  console.log(response.body);
                }
            });
    },
    appointstatus: function (connection, req, res) {
        var query = "SELECT * FROM appointstatus ORDER BY ID";
        this.executeQuery(connection, res, query, 'Success', 'AppointStatus');
    },

    appedit: function (connection, req, res) {
        var query = "UPDATE appoint SET DrName=?, Room=?, Equipment=?, AppTime=?, Upto=?, Regn_No=?, Name=?, Phone=?, Purpose=?, Remarks=?, Cancelled=0, Status=?, GroupApptt=?, Oper=?,Fees=? WHERE ID=?";
        var table = [req.body.DrName, req.body.Room, req.body.Equipment, formatter.format(new Date(req.body.AppTime)), formatter.format(new Date(req.body.Upto)), req.body.Regn_No, req.body.Name, req.body.Phone, req.body.Purpose, req.body.Remarks, req.body.Status, req.body.GroupApptt, req.body.Oper,req.body.Fees, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                var query = 'INSERT INTO appointmentlog (AppointmentLogDate, AppID, Description, Status, Oper) VALUES(?,?,?,?,?)'
                var table = [formatter.format(new Date(req.body.CurrentDate)), req.body.ID, 'Edited', req.body.Status, req.body.Oper];
                query = mysql.format(query, table);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": "Error executing MySQL query" });
                    } else {
                        res.json({ "Error": false, "Message": "Appointment Updated" });
                    }
                });
            }
        });
    },

    appadd: function (connection, req, res) {
        var query = "INSERT INTO appoint (DrName, Room, Equipment, AppTime, Upto, Regn_No, Name, Phone, Purpose, Remarks, Cancelled, Status, GroupApptt, Oper,Fees) VALUES (?,?,?,?,?,?,?,?,?,?,0,?,?,?,?)";
        var table = [req.body.DrName, req.body.Room, req.body.Equipment, formatter.format(new Date(req.body.AppTime)), formatter.format(new Date(req.body.Upto)), req.body.Regn_No, req.body.Name, req.body.Phone, req.body.Purpose, req.body.Remarks, req.body.Status, req.body.GroupApptt, req.body.Oper,req.body.Fees];
        query = mysql.format(query, table);

        if (req.body.GroupApptt) {

        }
        connection.query(query, function (err, result, fields) {
            if (err) {
                console.log(err);
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                var query2 = 'INSERT INTO appointmentlog (AppointmentLogDate, AppID, Description, Status, Oper) VALUES(?,?,?,?,?)'
                var table2 = [formatter.format(new Date(req.body.CurrentDate)), result.insertId, 'Created', req.body.Status, req.body.Oper];
                query2 = mysql.format(query2, table2);
                connection.query(query2, function (err, rows) {
                    if (err) {
                        console.log(err);
                        res.json({ "Error": true, "Message": "Error executing MySQL query" });
                    } else {
                        res.json({ "Error": false, "Message": "Appointment Updated", "AppID": result.insertId });
                    }
                });

            }
        });
    },

    appdel: function (connection, req, res ) {
        var query = "DELETE FROM appoint WHERE ID=" + req.query.appid;
        this.executeQuery(connection, res, query, 'Appointment deleted');
    },

    checkslot: function (connection, req, res) {
        var tApp = formatter.format(new Date(req.query.AppTime));
        var tUpto = formatter.format(new Date(req.query.Upto));
        var query = "SELECT ID from Appoint WHERE ((AppTime <='" + tApp + "' AND Upto>'" + tApp + "') OR (AppTime>'" + tApp + "' AND Upto<='" + tUpto + "') OR (AppTime<'" + tUpto + "' AND Upto>'" + tUpto + "')) AND DrName='" + req.query.DrName + "' AND Cancelled=0 AND Status!=2 AND ID !=" + req.query.AppID;
        this.executeQuery(connection, res, query, 'Success', 'AppID');
    },
    checkroom: function (connection, req, res) {
        var tApp = formatter.format(new Date(req.query.AppTime));
        var tUpto = formatter.format(new Date(req.query.Upto));
        var query = "SELECT ID from Appoint WHERE ((AppTime <='" + tApp + "' AND Upto>'" + tApp + "') OR (AppTime>'" + tApp + "' AND Upto<='" + tUpto + "') OR (AppTime<'" + tUpto + "' AND Upto>'" + tUpto + "')) AND " + req.query.field + "='" + req.query.fieldvalue + "' AND Cancelled=0";
        this.executeQuery(connection, res, query, 'Success', 'AppID');
    },
    roomappointments: function (connection, req, res) {
        var start = new Date(req.query.date);
        start.setHours(0, 0, 0, 0);
        var end = new Date(req.query.date);
        end.setHours(23, 59, 59, 0);
        var query = "SELECT * FROM appoint WHERE AppTime > ? AND AppTime < ? AND " + req.query.field + "='" + req.query.fieldvalue + "' ORDER BY AppTime";
        var table = [formatter.format(start), formatter.format(end)];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Appointments');
    },

    waitinglist: function (connection, req, res) {
        var query = "SELECT * FROM appoint WHERE Status = 3 AND DrName=? and Cancelled=0 ORDER BY AppTime";
        var table = [req.query.doc];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Appointments');
    },
    groupmembers: function (connection, req, res) {
        var query = "SELECT * FROM appointgroups WHERE AppID=" + req.query.appid;
        this.executeQuery(connection, res, query, 'Success', 'Members');
    },

    membersave: function (connection, req, res) {
        var query = "INSERT INTO appointgroups (AppID, Regn_No, Name, Mobile) VALUES (?,?,?,?)";
        var table = [req.body.AppID, req.body.Regn_No, req.body.Name, req.body.Mobile];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Session member saved');
    },

    memberupdate: function (connection, req, res) {
        var query = "UPDATE appointgroups SET Arrived=? WHERE ID=?";
        var table = [req.body.arrivestatus, req.body.memberID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Member updated');
    },

    memberdelete: function (connection, req, res) {
        var query = "DELETE FROM appointgroups WHERE ID=" + req.query.memberid;
        this.executeQuery(connection, res, query, 'Member deleted');
    },

    saveapptpurpose: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE jobtypes SET Purpose=?, Duration=?, Department=?, GroupApptt=?,CPTCode=? WHERE ID=?";
            var table = [req.body.Purpose, req.body.Duration, req.body.Department, req.body.GroupApptt, req.body.CPTCode, req.body.ID];
            var message = "Appointment Purpose Updated"
        } else {
            var query = "INSERT INTO jobtypes (Purpose, Duration, Department,GroupApptt,CPTCode) VALUES (?,?,?,?,?)";
            var table = [req.body.Purpose, req.body.Duration, req.body.Department, req.body.GroupApptt, req.body.CPTCode];
            var message = "Appointment Purpose  Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    apptpurposedel: function (connection, req, res) {
        var query = "DELETE FROM jobtypes WHERE ID=" + req.query.id;
        connection.query(query, [req.query.id], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Appointment Purpose Deleted" });
            }
        });
    },
    holiday: function (connection, req, res) {
        var query = "SELECT * FROM holidays WHERE Name='' OR Name='All' OR Name IS Null ORDER BY DateFrom";
        this.executeQuery(connection, res, query, 'Success', 'Holiday');
    },
    saveholiday: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE holidays SET Holiday=?, DateTo=?, DateFrom=?, Holidaysdate=? WHERE ID=?";
            var table = [req.body.Holiday, formatter.format(new Date(req.body.DateTo)), formatter.format(new Date(req.body.DateTo)), formatter.format(new Date(req.body.Holidaysdate)), req.body.ID];
            var message = "Holiday Updated"
        } else {
            var query = "INSERT INTO holidays (Name, Holiday, DateTo, DateFrom, Holidaysdate) VALUES (?,?,?,?,?)";
            var table = [req.body.Name, req.body.Holiday, formatter.format(new Date(req.body.DateTo)), formatter.format(new Date(req.body.DateTo)), formatter.format(new Date(req.body.Holidaysdate))];
            var message = "Holiday  Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    delholiday: function (connection, req, res) {
        var query = "DELETE FROM holidays WHERE ID=" + req.query.id;
        connection.query(query, [req.query.id], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": " Deleted" });
            }
        });
    },
    getFromTo(req) {
        if (req.query.dtFrom > '') {
            dtFrom = new Date(req.query.dtFrom);
        } else {
            dtFrom = new Date();
        }
        dtFrom.setHours(0, 0, 0);
        if (req.query.dtTo > '') {
            dtTo = new Date(req.query.dtTo);
        } else {
            dtTo = new Date();
        }
        dtTo.setHours(0, 0, 0);
    },
    doctitle: function (connection, req, res) {
        var query = "SELECT * FROM users WHERE Name= '" + req.query.docname + "'";
        this.executeQuery(connection, res, query, 'Success', 'Doctitle');
    },
    getsmsuser: function (connection, smsconnection, req, res) {
        var that = this;
        var records = new Array();
        var query = "select * from smslog where AppID >0 and ResponseText is not null and SMSU = '" + req.query.smsuser + "'  and ResponseUpdated = 0"
        smsconnection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                for (var i = 0; i < rows.length; i++) {
                    that.updapptsms(connection, smsconnection, rows[i], res);
                }
            }
        });
    },

    updapptsms: function (connection, smsconnection, req, res) {
        if ((req.ResponseText.toLowerCase().trim() === 'yes') || (req.ResponseText.toLowerCase().trim() === 'y')) {
            var query = "Update appoint set Oper='SMS',Status = 1,ChangeDate = '" + formatter.format(new Date(req.ResponseTime)) + "' where Status < 3 AND ID = '" + req.AppID + "'";
            query += ';INSERT INTO appointmentlog (AppointmentLogDate, Status,AppID, Description, Oper) VALUES(?,1,?,?,?)';
            var table = [formatter.format(new Date(req.ResponseTime)), req.AppID, 'Status Change', 'SMS'];
            query = mysql.format(query, table);
            var appid = '';
            appid = req.AppID;
            connection.query(query, appid, function (err) {
                if (err) {
                    res.json({ "Error": true, "Message": "Error executing MySQL query" });
                } else {
                    var query2 = "UPDATE smslog SET ResponseUpdated = 1 WHERE AppID= '" + appid + "'"
                    query2 = mysql.format(query2);
                    smsconnection.query(query2, function (err, rows) {
                        if (err) {
                            // res.json({ "Error": true, "Message": "Error executing MySQL query" });
                        } else {
                            // res.json({ "Error": false, "Message":  appid });
                        }
                    });
                }
            });
        } else if ((req.ResponseText.toLowerCase().trim() === 'no') || (req.ResponseText.toLowerCase().trim() === 'n')) {
            var appid = '';
            appid = req.AppID;
            var query = "Update appoint set Oper='SMS',Cancelled = 1,ChangeDate = '" + formatter.format(new Date(req.ResponseTime)) + "' where (Status < 3 OR status = 4) AND ID = '" + req.AppID + "'"
            query += ';INSERT INTO appointmentlog (AppointmentLogDate, Status,AppID, Description, Oper) VALUES(?,1,?,?,?)';
            var table = [formatter.format(new Date(req.ResponseTime)), req.AppID, 'Status Change', 'SMS'];
            query = mysql.format(query, table);
            connection.query(query, appid, function (err, req) {
                if (err) {
                    res.json({ "Error": true, "Message": "Error executing MySQL query" });
                } else {
                    var query2 = "UPDATE smslog SET ResponseUpdated = 1 WHERE AppID='" + appid + "'"
                    query2 = mysql.format(query2);
                    smsconnection.query(query2, function (err, rows) {
                        if (err) {
                            //res.json({ "Error": true, "Message": "Error executing MySQL query" });
                        } else {
                        }
                    });
                }
            });
        }
    },

    linkAppoint: function (connection, req, res) {
        var query = "UPDATE appoint SET Regn_No='"+req.body.regn_no+"' WHERE ID="+req.body.appid;
        query += ";UPDATE receipts set Regn_No=" + req.body.regn_no+ " where appID = "+req.body.appid;
        var table = [req.body.regn_no, req.body.appid];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Member updated');
    },
    updateAppointment: function (connection, req, res) {
        var query = "UPDATE appoint SET status=20,oper=? WHERE ID=?";
        var table = [req.body.Oper, req.body.ID];
        query = mysql.format(query, table);

        query += ';INSERT INTO appointmentlog (AppointmentLogDate, Status,AppID, Description, Oper) VALUES(?,20,?,?,?)';
        var table = [formatter.format(new Date(req.body.CurrentDate)), req.body.ID, 'Status Change', req.body.Oper];

        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Appointment Updated');
    },
    appointcount: function (connection, req, res) {
        var query = "SELECT COUNT(id) as count FROM appoint WHERE regn_no=? AND cancelled =0 AND apptime <=? and DrName=? ORDER BY apptime desc";
        var table = [req.query.regn_no, formatter.format(new Date(req.query.apptime)),req.query.drname];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'AppointmentCount');
    },

    updateSMSApptRemarks: function (connection,req, res) {
        var query = "UPDATE appoint set remarks = CONCAT(remarks,'"+ req.body.Remarks+"') WHERE ID=?";
        var table = [req.body.ID];
        query = mysql.format(query, table);

        query += ';INSERT INTO appointmentlog (AppointmentLogDate, Status,AppID, Description, Oper) VALUES(?,?,?,?,?)';
        var table = [formatter.format(new Date(req.body.CurrentDate)), req.body.Status,req.body.ID, 'SMS Sent', req.body.Oper];

        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Appointment Updated');
    },
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }


}
