var mysql = require("mysql");

var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

// var formatter = new Intl.DateTimeFormat([], options);
// var formatter = new Intl.DateTimeFormat([], options);
var formatter = require("./formatter");

module.exports = {
    // Complaints and Vital signs
    complaints: function (connection, req, res) {
        if (req.query.VisitID) {
            var query = "SELECT * FROM complaints WHERE VisitID=" + req.query.VisitID + " order by ComplaintsDate desc"
        } else {
            var query = "SELECT * FROM complaints WHERE Regn_No='" + req.query.regn_no + "' order by ComplaintsDate desc"
        }
        this.executeQuery(connection, res, query, 'Success', 'Complaints');
    },
    complaintsonSameDay: function (connection, req, res) {
        var query = "SELECT * FROM complaints WHERE Regn_No='" + req.query.regn_no + "' and ComplaintsDate like '" + req.query.Visitdate + "%'  order by ComplaintsDate desc"
        this.executeQuery(connection, res, query, 'Success', 'ComplaintsonSameDay');
    },
    savecomplaints: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE Complaints SET VisitID=?,Regn_No=?,ComplaintsDate=?,Complaints=?, Temp=?, Pulse=?, BP=?, Height=?, Weight=?, Resp=?, Spo2=?, Pain=?,HeadCirc=?,Education=?,TreatmentPlan=?,Abuse=?,Oper=?,ProcStart=?,ProcEnd=? WHERE ID=?";
            var message = "Complaints Updated"
        } else {
            var query = "INSERT INTO Complaints (VisitID, Regn_No, ComplaintsDate, Complaints, Temp, Pulse, BP, Height, Weight, Resp, Spo2, Pain, HeadCirc,Education,TreatmentPlan,Abuse, Oper,ProcStart,ProcEnd) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            var message = "Complaints Added"
        }
        var table = [req.body.VisitID, req.body.Regn_No, formatter.format(new Date()), req.body.Complaints, req.body.Temp, req.body.Pulse, req.body.BP, req.body.Height, req.body.Weight, req.body.Resp, req.body.Spo2, req.body.Pain, req.body.HeadCirc, req.body.Education, req.body.TreatmentPlan, req.body.Abuse, req.body.Oper,req.body.ProcStart,req.body.ProcEnd, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    lastWeight: function (connection, req, res) {
        var query = "SELECT Weight FROM complaints WHERE Regn_No='" + req.query.regn_no + "' and Weight IS NOT NULL  order by ComplaintsDate desc LIMIT 1";
        this.executeQuery(connection, res, query, 'Success', 'LastWeight');
    },


    // EMR add page
    pagegroups: function (connection, req, res) {
        var query = "SELECT * FROM pagegroups WHERE ID> 0 AND Active=1 ORDER BY Title";
        this.executeQuery(connection, res, query, 'Success', 'PageGroups');
    },
    getpages: function (connection, req, res) {
        if (req.query.pageID) {
            var query = "SELECT * FROM visitpages WHERE ID=" + req.query.pageID;
        } else {
            var query = "SELECT * FROM visitpages WHERE VisitID=" + req.query.visitID + " ORDER BY ID";
        }
        this.executeQuery(connection, res, query, 'Success', 'Pages');
    },
    savepage: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, PageContent, TemplateType, Operator) VALUES (?,?,?,?,?,?)";
            var message = 'Page Added';
        } else {
            var query = "UPDATE visitpages SET VisitID=?, PageDate=?, PageTitle=?, PageContent=?, TemplateType=?, Operator=? WHERE ID=?";
            var message = 'Page Updated';
        }
        var table = [req.body.VisitID, formatter.format(new Date(req.body.PageDate)), req.body.PageTitle, req.body.PageContent, req.body.TemplateType, req.body.Operator, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    deletepage: function (connection, req, res) {
        var query = "DELETE FROM visitpages WHERE ID=" + req.query.pageID;
        this.executeQuery(connection, res, query, 'Page deleted');
    },

    // Templates
    templates: function (connection, req, res) {
        var query = "SELECT * FROM Templates where templatetype=" + req.query.type + " ORDER BY title";
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.end(JSON.stringify(rows));
            }
        });
    },
    templatename: function (connection, req, res) {
        //var query = "select templates.* from pagegroups join templates on pagegroups.ID=templates.templatetype where pagegroups.Active=1 and pagegroups.id > 0 order by pagegroups.Title" 
        var query = "SELECT Distinct(VisitPages.PageTitle) FROM VisitPages JOIN Visits ON VisitPages.VisitID = Visits.ID WHERE Visits.Regn_No=" + req.query.regn_no + " ORDER BY VisitPages.PageTitle";
        this.executeQuery(connection, res, query, 'Success', 'Templatesname');
    },
    templatesget: function (connection, req, res) {
        var query = "SELECT * FROM Templates where templatetype = ? ORDER BY title";
        var table = [req.query.templatetype];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Templates');
    },
    templatebytitle: function (connection, req, res) {
        var query = "SELECT * FROM Templates where title = ? ORDER BY title";
        var table = [req.query.templatetitle];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Templates');
    },
    templatesave: function (connection, req, res) {
        var query = "INSERT INTO Templates (title, description, content, templatetype) VALUES (?,?,?,?)";
        var table = [req.body.title, req.body.description, req.body.content, req.body.templatetype];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Template added');
    },
    temlatedelete: function (connection, req, res) {
        var query = "DELETE FROM Templates WHERE title = ?";
        var table = [req.query.title];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Template deleted');
    },
    getautocomplete: function (connection, req, res) {
        var query = "SELECT * FROM AutoComplete WHERE Word LIKE '" + req.query.search + "%' ORDER BY Frequency DESC, Word LIMIT 10";
        this.executeQuery(connection, res, query, 'Success', 'Words');
    },
    insertautocomplete: function (connection, req, res) {
        var query = "INSERT INTO AutoComplete (Word, Frequency) VALUES ('" + req.body.search + "',1)";
        this.executeQuery(connection, res, query, 'Word added');
    },
    updateautocomplete: function (connection, req, res) {
        var query = "UPDATE AutoComplete SET Frequency = Frequency + 1 WHERE Word='" + req.body.search + "'";
        this.executeQuery(connection, res, query, 'Frequency updated');
    },
    getallautocomplete: function (connection, req, res) {
        var query = "SELECT * FROM AutoComplete ORDER BY Word"
        this.executeQuery(connection, res, query, 'Success', 'AutoCompleteWords');
    },
    saveautocomplete: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE AutoComplete SET Word=? WHERE ID=" + req.body.ID;
            var message = "Auto SuggestUpdated"
        } else {
            var query = "INSERT INTO AutoComplete (Word,Frequency) VALUES(?,?)";
            var message = "Auto Suggest Added"
        }
        var table = [req.body.Word, 1]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    delAutocomplete: function (connection, req, res) {
        var query = "DELETE FROM AutoComplete WHERE id=" + req.query.id;
        this.executeQuery(connection, res, query, 'Word Deleted');
    },

    // Visit
    createvisit: function (connection, req, res) {
        var query = "INSERT INTO Visits (VisitDate, Regn_No, Consultant, AppointmentID) VALUES (?,?,?,?)";
        var table = [formatter.format(new Date()), req.body.Regn_No, req.body.Consultant, req.body.AppointmentID];
        var query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                var query2 = 'update attachments set VisitID =? where AppID=?'
                var table2 = [result.insertId, req.body.AppointmentID];
                query2 = mysql.format(query2, table2);
                var visitId = result.insertId;
                connection.query(query2, function (err, result, fields) {
                    if (err) {
                        res.json({ "Error": true, "Message": "Error executing MySQL query" });
                    } else {
                        res.json({ "Error": false, "Message": "Visit created", "VisitID": visitId });
                    }
                });

            }
        });
    },
    deletevisit: function (connection, req, res) {
        var query = "DELETE FROM Visits WHERE AppointmentID=" + req.query.appID;
        this.executeQuery(connection, res, query, 'Success');
    },

    visits: function (connection, req, res) {
        if (req.query.regn_no > '') {
            var query = "SELECT * FROM visits WHERE Regn_No='" + req.query.regn_no + "' ORDER BY VisitDate DESC"
        } else {
            var query = "SELECT * FROM visits WHERE ID='" + req.query.visitID + "'"
        }
        this.executeQuery(connection, res, query, 'Success', 'Visits');
    },
    selectedvisits: function (connection, req, res) {
        if (req.query.consultant > '') {
            var query = "SELECT * FROM visits WHERE Regn_No='" + req.query.regn_no + "' and Consultant='" + req.query.consultant + "' ORDER BY ID DESC"
        } else {
            var query = "SELECT * FROM visits WHERE Regn_No='" + req.query.regn_no + "' ORDER BY ID DESC"
        }
        this.executeQuery(connection, res, query, 'Success', 'Visits');
    },
    visitsOnPageFilter: function (connection, req, res) {
        var query = "select visits.* from visits join visitpages on visits.ID = visitpages.VisitID where visitpages.PageTitle like '" + req.query.pagetitle + "%' and visits.Regn_No='" + req.query.regn_no + "' order by visits.VisitDate desc";
        this.executeQuery(connection, res, query, 'Success', 'Visits');
    },
    visitsfromappt: function (connection, req, res) {
        var query = "select * from visits WHERE AppointmentID=" + req.query.appid;
        this.executeQuery(connection, res, query, 'Success', 'Visits');
    },
    visitsOnDurationFilter: function (connection, req, res) {
        if (!req.query.duration || req.query.duration === 'All') {
            var query = "SELECT * FROM visits WHERE Regn_No='" + req.query.regn_no + "' ORDER BY VisitDate DESC"
        } else {
            var dur
            if (req.query.duration === '1 month') {
                dur = 1;
            } else if (req.query.duration === '3 months') {
                dur = 3;
            } else if (req.query.duration === '1 year') {
                dur = 12;
            }
            var query = "SELECT * FROM visits  WHERE date(visits.VisitDate) >= Date(DATE_SUB('" + formatter.format(new Date()) + "', INTERVAL " + dur + " MONTH)) AND date(visits.VisitDate) <= date('" + formatter.format(new Date()) + "') AND regn_no='" + req.query.regn_no + "' ORDER BY VisitDate DESC";
        }
        this.executeQuery(connection, res, query, 'Success', 'Visits');
    },
    visitPages: function (connection, req, res) {
        var query = "SELECT * FROM VisitPages LEFT JOIN Visits ON VisitPages.VisitID = Visits.ID WHERE Visits.Regn_No=" + req.query.regn_no + " ORDER BY PageTitle, PageDate DESC";
        this.executeQuery(connection, res, query, "Success", "VisitPages");
    },

    lastVisit: function (connection, req, res) {
        var query = "SELECT * FROM visits WHERE Regn_No=? AND Consultant=? AND VisitDate < ? ORDER BY VisitDate DESC LIMIT 1";
        var table = [req.query.regn_no, req.query.consultant, formatter.format(new Date(req.query.visitdate))];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Visits');
    },
    lastdiagnosisvisit: function (connection, req, res) {
        var query = "SELECT * FROM diagnosis join  visits on diagnosis.VisitID= visits.ID where  visits.Regn_No=? ORDER BY diagnosis.ID desc";
        var table = [req.query.regn_no];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'Visits');
    },
    visitlog: function (connection, req, res) {
        var query = "SELECT * FROM visitslog where VisitID=? ORDER BY VisitsLogDate";
        var table = [req.query.id];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success', 'VisitLog');
    },
    savevisitlog: function (connection, req, res, method) {
        var query = "INSERT INTO visitslog (VisitsLogDate, VisitID, PageID, Description, Operator,PageTitle) VALUES (?,?,?,?,?,?)";
        var table = [formatter.format(new Date(req.body.CurrentDate)), req.body.VisitID, req.body.ID, req.body.Description, req.body.Operator, req.body.PageTitle];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Visit Log Added');
    },
    savedocnotes: function (connection, req, res) {
        var query = "UPDATE Visits SET DoctorNotes=?, Keywords=?,unlocked=0 WHERE ID=?";
        var table = [req.body.notes, req.body.keywords, req.body.visitID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Notes saved');
    },
    patientconsentsave: function (connection, req, res) {
        var query = "INSERT INTO patientconsents (visitID, regn_no, Date, WitnessName, SigFile, WitnessSig, consentform) VALUES (?,?,?,?, ?, ?, ?)";
        var table = [req.body.visitID, req.body.regn_no, formatter.format(new Date(req.body.Date)), req.body.WitnessName, req.body.SigFile, req.body.WitnessSig, req.body.consentform];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'patientconsents added');
    },
    pasthistory: function (connection, req, res) {
        var query = "select * from pasthistory where  regn_no =" + req.query.regn_no + " and VisitId <= " + req.query.VisitID + " and VisitId in (select max(VisitId) from pasthistory where  regn_no =" + req.query.regn_no + " and VisitId <= " + req.query.VisitID + ")"
        this.executeQuery(connection, res, query, 'Success', 'pasthistory');
    },
    pasthistorysave: function (connection, req, res, method) {
        var LMP = false;
        if (method === 'put') {
            var query = "UPDATE PastHistory SET Family=?, Past=?, BloodGroup=?, Obstetric=?, Gynae=?, Medication=?, AlcoholRem=?, OtherAllergy=?, Surgery=?, Smoker=?, Alcohol=?,Diabetes=?,Hypertension=?,Heartdisease=?,Lipiddisorders=?,Asthma=?, Social=?, VisitID=?,PastHistoryDate=?, Operator=?, Neglect=?, Remarks=?";
            if (req.body.LMPDate > '') {
                query += ", LMPDate=?"
                LMP = true;
            }

            query += " WHERE ID=?";
            var table = [req.body.Family, req.body.Past, req.body.BloodGroup, req.body.Obstetric, req.body.Gynae, req.body.Medication, req.body.AlcoholRem, req.body.OtherAllergy, req.body.Surgery, req.body.Smoker, req.body.Alcohol, req.body.Diabetes, req.body.Hypertension, req.body.Heartdisease, req.body.Lipiddisorders, req.body.Asthma, req.body.Social, req.body.VisitID, formatter.format(new Date(req.body.PastHistoryDate)), req.body.Operator, req.body.Neglect, req.body.Remarks];
            if (LMP) {
                table.push(formatter.format(new Date(req.body.LMPDate)));
            }
            table.push(req.body.ID);
            var message = "Past History Updated"
        } else {
            var query = "INSERT INTO PastHistory (Family, Past, BloodGroup, Obstetric, Gynae, Medication,  AlcoholRem, OtherAllergy,Surgery, Smoker, Alcohol,Diabetes,Hypertension,Heartdisease,Lipiddisorders,Asthma,Social, Regn_No, VisitID, PastHistoryDate, Operator, Neglect, Remarks"
            if (req.body.LMPDate > '') {
                query += ", LMPDate"
                LMP = true;
            }

            query += ")VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
            if (LMP) {
                query += ", ?"
            }

            query += ")";

            var table = [req.body.Family, req.body.Past, req.body.BloodGroup, req.body.Obstetric, req.body.Gynae, req.body.Medication, req.body.AlcoholRem, req.body.OtherAllergy, req.body.Surgery, req.body.Smoker, req.body.Alcohol, req.body.Diabetes, req.body.Hypertension, req.body.Heartdisease, req.body.Lipiddisorders, req.body.Asthma, req.body.Social, req.body.Regn_No, req.body.VisitID, formatter.format(new Date(req.body.PastHistoryDate)), req.body.Operator, req.body.Neglect, req.body.Remarks];
            if (LMP) {
                table.push(formatter.format(new Date(req.body.LMPDate)));
            }
            var message = "Past History  Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    drugallergysave: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE patmas SET Allergies=?,Marital=? WHERE Regn_No=?";
            var message = "Allergies Updated"
        }
        var table = [req.body.Allergies, req.body.Marital, req.body.Regn_No];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    medicationsave: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE pasthistory SET Medication=? WHERE Regn_No=?";
            var message = "Medication Updated"
        }
        var table = [req.query.medication, req.query.regn_no];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    getimages: function (connection, req, res) {
        var query = "SELECT * FROM visitimages WHERE VisitID=" + req.query.visitid;
        this.executeQuery(connection, res, query, 'Success', 'Images');
    },

    //Procedures
    getCPT: function (connection, req, res) {
        var search = req.query.search.split(' ');
        if (req.query.user > '') {
            var query = "SELECT Code, Description FROM favicd WHERE ICDValue='CPT' AND (Description LIKE '%" + search[0] + "%' or Code LIKE '%" + search[0] + "%'"
        } else {
            var query = "SELECT Code, Short_Description FROM cpt WHERE Short_Description LIKE '%" + search[0] + "%' or Code LIKE '%" + search[0] + "%'"
        }
        for (var i = 1; i < search.length; i++) {
            query += " AND Short_Description LIKE '%" + search[i] + "%' or Code LIKE '%" + search[i] + "%'"
        }
        if (req.query.user) {
            query += " AND (Oper='" + req.query.user + "' OR Oper IS NULL))"
        }
        query += " ORDER BY Code LIMIT 100";
        this.executeQuery(connection, res, query, 'Success', 'CPT');
    },
    procedurescpt: function (connection, req, res) {
        var query = "SELECT * FROM procedurescpt WHERE PageID=" + req.query.pageID + " ORDER BY ID";
        this.executeQuery(connection, res, query, 'Success', 'ProceduresCPT');
    },
    saveprocedurescpt: function (connection, req, res) {
        var query = "INSERT INTO procedurescpt (PageID, ProceduresCPTDate, Code, Short_Description) VALUES (?,?,?,?)";
        var table = [req.body.PageID, formatter.format(new Date()), req.body.Code, req.body.Short_Description];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success');
    },
    delprocedurescpt: function (connection, req, res) {
        var query = "DELETE FROM proceduresCPT WHERE PageID=" + req.query.PageID + " AND Code='" + req.query.code + "'";
        this.executeQuery(connection, res, query, 'Code deleted');
    },

    visitcpt: function (connection, req, res) {
        var query = "SELECT * FROM procedurescpt WHERE VisitID=" + req.query.visitID + " ORDER BY ID";
        this.executeQuery(connection, res, query, 'Success', 'VisitCPT');
    },
    savevisitcpt: function (connection, req, res) {
        var query = "INSERT INTO procedurescpt (VisitID, ProceduresCPTDate, Code, Short_Description) VALUES (?,?,?,?)";
        var table = [req.body.VisitID, formatter.format(new Date()), req.body.Code, req.body.Short_Description];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success');
    },
    delvisitcpt: function (connection, req, res) {
        var query = "DELETE FROM proceduresCPT WHERE VisitID=" + req.query.visitID + " AND Code='" + req.query.code + "'";
        this.executeQuery(connection, res, query, 'Code deleted');
    },

    getProcedure: function (connection, req, res) {
        var query = "SELECT * FROM procedures WHERE PageID=" + req.query.pageID;
        this.executeQuery(connection, res, query, 'Success', 'Procedures');
    },
    saveProcedure: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO procedures (ProceduresDate, ProcStart, ProcEnd, Anaesthesia, Notes, OthersRemarks, VisitID, Regn_No,Operator,ASA,Implants) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        } else {
            var query = "UPDATE procedures SET  ProceduresDate=?, ProcStart=?, ProcEnd=?, Anaesthesia=?, Notes=?, OthersRemarks=?, VisitID=?, Regn_No=?, Operator=?,ASA=?,Implants=?  WHERE ID=?";
        }
        var table = [formatter.format(new Date()), req.body.ProcStart, req.body.ProcEnd, req.body.Anaesthesia, req.body.Notes, req.body.Remarks, req.body.VisitID, req.body.Regn_No, req.body.Operator, req.body.ASA, req.body.Implants, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var procedureinsertid = result.insertId
                if (method === 'post') {
                    var query2 = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, TemplateType, Operator,PageContent) VALUES (?,?,?,?,?,?)";
                    var table2 = [req.body.VisitID, formatter.format(new Date()), req.body.title, 97, req.body.Operator, req.body.PageContent];
                    query2 = mysql.format(query2, table2);
                    connection.query(query2, function (err, result, fields) {
                        if (err) {
                            res.json({ "Error": true, "Message": err.sqlMessage });
                        } else {
                            var query3 = "UPDATE procedures set PageID =" + result.insertId + " where id =" + procedureinsertid;
                            connection.query(query3, function (err, result, fields) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                    res.json({ "Error": false, "Message": 'Procedure saved' });
                                }
                            });
                        }
                    });
                } else {
                    var query2 = "UPDATE visitpages set PageContent = ? where id=" + req.body.PageID;
                    var table2 = [req.body.PageContent];
                    query2 = mysql.format(query2, table2);
                    connection.query(query2, function (err, result, fields) {
                        if (err) {
                            res.json({ "Error": true, "Message": err.sqlMessage });
                        } else {
                            res.json({ "Error": false, "Message": 'Procedure Updated' });
                        }
                    });

                }
            }
        });
    },

    // Diagnosis
    diagnosis: function (connection, req, res) {
        if (req.query.visitID) {
            var query = "SELECT * FROM diagnosis WHERE VisitID=" + req.query.visitID + " ORDER BY ID";
        } else {
            var query = "SELECT * FROM diagnosis WHERE Regn_No=" + req.query.regn_no + " ORDER BY ID";
        }
        this.executeQuery(connection, res, query, 'Success', 'Diagnosis');
    },

    lastdiagnosis: function (connection, req, res) {
        var query = "SELECT Max(DiagnosisDate) FROM Diagnosis WHERE Regn_No=" + req.query.regn_no;
        var self = this;
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (rows) {
                    var lastdate = new Date(rows[0]['Max(DiagnosisDate)']);
                    var lastdatestr = lastdate.toISOString().substr(0, 10)
                    var query2 = "SELECT * FROM Diagnosis WHERE Regn_No=" + req.query.regn_no + " AND DiagnosisDate LIKE '" + lastdatestr + "%'";
                    self.executeQuery(connection, res, query2, 'Success', 'Diagnosis');
                }
            }
        });

    },
    // diagnosisname: function(connection, req, res) {
    //     var query = "SELECT * FROM diagnosis WHERE icd=" + req.query.visitID + " ORDER BY ID";
    //     this.executeQuery(connection, res, query,'Success', 'Diagnosis');
    // },
    savediagnosis: function (connection, req, res) {
        var query = "INSERT INTO diagnosis (VisitID, Regn_No, DiagnosisDate, ICD, Diagnosis,DiagType) VALUES (?,?,?,?,?,?)";
        var table = [req.body.VisitID, req.body.Regn_No, formatter.format(new Date()), req.body.ICD, req.body.Diagnosis, req.body.DiagType];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Diagnosis Saved');
    },
    deletediagnosis: function (connection, req, res) {
        var query = "DELETE FROM diagnosis WHERE VisitID=" + req.query.visitID + " AND ICD='" + req.query.icd + "'";
        this.executeQuery(connection, res, query, 'Diagnosis deleted');
    },
    canceldiagnosis: function (connection, req, res) {
        var query = "Update  diagnosis  set Cancelled= 1,CancelledDate='" + formatter.format(new Date()) + "' WHERE VisitID=" + req.body.visitID + " AND ICD='" + req.body.icd + "'";
        this.executeQuery(connection, res, query, 'Diagnosis Cancelled');
    },
    resolvediagnosis: function (connection, req, res) {
        var query = "Update  diagnosis  set resolved= 1,ResolvedDate='" + formatter.format(new Date(req.body.ResolvedDate)) + "' WHERE ID=" + req.body.ID ;
        this.executeQuery(connection, res, query, 'Diagnosis Resolved');
    },
    typeupdatediagnosis: function (connection, req, res) {
        var self = this;
        if (req.body.DiagType ==  'Primary') {
            var query3 = "SELECT * FROM diagnosis WHERE visitid='" + req.body.VisitID + "' and DiagType = 'Primary' and (Cancelled IS NULL OR Cancelled =0 )";
            connection.query(query3, function (err, rows) {
                if (err) {
                    res.json({ "Error": true, "Message": err.sqlMessage });
                } else {
                    if (!rows.length) {
                      self.updateDiagType(connection,req, res,req.body.DiagType, req.body.ID);
                    } else {
                        res.json({ "Error": false, "Message": "Primary Diagnosis Already Entered" });
                    }
                }
            });
        } else {
            self.updateDiagType(connection,req, res,req.body.DiagType, req.body.ID);
        }
    },
    updateDiagType(connection,req, res,diagtype,diagid) {
        var query = "Update  diagnosis  set DiagType= '" + diagtype + "' WHERE ID=" + diagid ;
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Diagnosis Type Updated" });
            }
        });
    },
    getICD: function (connection, req, res) {
        var search = req.query.search.split(' ');
        if (req.query.version === 'ICD-9') {
            var query = "SELECT Code, Description FROM icd9cm WHERE (Description LIKE '%" + search[0] + "%' or Code LIKE '%" + search[0] + "%')"
        } else if (req.query.version === 'ICD-10') {
            var query = "SELECT Code, Description FROM icd10diag WHERE (Description LIKE '%" + search[0] + "%' or Code LIKE '%" + search[0] + "%')"
        } else {
            var query = "SELECT Code, Description FROM favicd WHERE ICDValue='ICD-10' and (Description LIKE '%" + search[0] + "%' or Code LIKE '%" + search[0] + "%')"
        }
        for (var i = 1; i < search.length; i++) {
            query += " AND (Description LIKE '%" + search[i] + "%' or Code LIKE '%" + search[i] + "%')"
        }
        if (req.query.user) {
            query += " AND (Oper='" + req.query.user + "' OR Oper IS NULL)"
        }
        query += " ORDER BY Code LIMIT 100";
        this.executeQuery(connection, res, query, 'Success', 'ICD');
    },
    favICDsave: function (connection, req, res) {
        var query = "INSERT INTO favicd (Code, Description, ICDValue, Oper) VALUES (?,?,?,?)";
        var table = [req.body.Code, req.body.Description, req.body.ICDValue, req.body.Oper];
        var query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Favourite saved');
    },
    favICDdelete: function (connection, req, res) {
        var query = "DELETE FROM favicd WHERE Code=? AND Oper=?";
        var table = [req.query.Code, req.query.Oper];
        var query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Favourite deleted');
    },

    // Pain
    pain: function (connection, req, res) {
        var query = "SELECT * FROM pain WHERE  VisitID= '" + req.query.VisitID + "'"
        this.executeQuery(connection, res, query, 'Success', 'Pain');
    },
    savepain: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE pain SET Regn_No=?, PainDate=?, Consultant=?, Intensity=?, Location=?, PainCharacter=?, Duration=?, Frequency=?, Medication=?, VisitID=?,Aggravation=?, Relief=?, Remarks=? WHERE ID=?";
            var message = "Pain Updated"
        } else {
            var query = "INSERT INTO pain ( Regn_No, PainDate, Consultant, Intensity, Location, PainCharacter, Duration, Frequency, Medication,VisitID, Aggravation, Relief,Remarks ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            var message = "Pain Added"
        }
        var table = [req.body.Regn_No, formatter.format(new Date(req.body.PainDate)), req.body.Consultant, req.body.Intensity, req.body.Location, req.body.PainCharacter, req.body.Duration, req.body.Frequency, req.body.Medication, req.body.VisitID, req.body.Aggravation, req.body.Relief, req.body.Remarks, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "UPDATE complaints SET pain  =? WHERE VisitID  =? ";
                var table2 = [req.body.Intensity, req.body.VisitID];
                query2 = mysql.format(query2, table2);
                connection.query(query2, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": "Error executing MySQL query" });
                    } else {
                        res.json({ "Error": false, "Message": "Pain Added or Updated" });
                    }
                });
            }
        });

    },
    abuse: function (connection, req, res) {
        var query = "SELECT * FROM abuse WHERE  VisitID= '" + req.query.VisitID + "'"
        this.executeQuery(connection, res, query, 'Success', 'Abuse');
    },

    saveabuse: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE abuse SET Regn_No=?, AbuseDate=?, VisitID=?,Consultant=?,Description=?, PhysicalAbuse=?,SexualAbuse=?,PhysicalNeglect=?,MedicalExamination=?,Xrays=?,Photographs=?,PoliceNotification=?,ExaminerNotification=?,OtherActions=?,Remarks=?,Filename=? WHERE ID=?";
        } else {
            var query = "INSERT INTO abuse ( Regn_No, AbuseDate,VisitID, Consultant, Description, PhysicalAbuse, SexualAbuse, PhysicalNeglect, MedicalExamination, Xrays,Photographs, PoliceNotification, ExaminerNotification,OtherActions,Remarks,Filename) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Regn_No, formatter.format(new Date(req.body.AbuseDate)), req.body.VisitID, req.body.Consultant, req.body.Description, req.body.PhysicalAbuse, req.body.SexualAbuse, req.body.PhysicalNeglect, req.body.MedicalExamination, req.body.Xrays, req.body.Photographs, req.body.PoliceNotification, req.body.ExaminerNotification, req.body.OtherActions, req.body.Remarks, req.body.Filename, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Abuse Added or Updated" });
            }
        });
    },

    symptoms: function (connection, req, res) {
        if (req.query.selection) {
            var query = "SELECT * FROM symptoms WHERE systemvalue=" + req.query.selection + " order by id";
        } else {
            var query = "SELECT * FROM symptoms order by id";
        }
        this.executeQuery(connection, res, query, 'Success', 'symptoms');
    },
    systems: function (connection, req, res) {
        if (req.query.selection) {
            var query = "SELECT distinct system FROM symptoms WHERE systemvalue=" + req.query.selection + " order by id";
        } else {
            var query = "SELECT distinct system FROM symptoms order by id";
        }
        this.executeQuery(connection, res, query, 'Success', 'systems');
    },
    systemsfrench: function (connection, req, res) {
        var query = "SELECT distinct system, HistoryExam FROM symptoms order by system"
        this.executeQuery(connection, res, query, 'Success', 'systems');
    },
    symptompatdata: function (connection, req, res) {
        if (req.query.selection) {
            var query = "SELECT * FROM symptomdata WHERE systemvalue=" + req.query.selection + " AND VisitID= '" + req.query.VisitID + "' order by id"
        } else {
            var query = "SELECT * FROM symptomdata WHERE  VisitID= '" + req.query.VisitID + "' order by id"
        }
        this.executeQuery(connection, res, query, 'Success', 'patdata');
    },
    savesysreview: function (connection, req, res) {
        if (req.query.selection) {
            var query = "DELETE FROM symptomdata WHERE VisitID=" + req.body[0].VisitID + " and systemvalue=" + req.query.selection;
        } else {
            var query = "DELETE FROM symptomdata WHERE VisitID=" + req.body[0].VisitID
        }
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO symptomdata (Regn_No, System,NASK,NAD, Comments,Symptom, Symptomval,VisitID,SymptomdataDate,Operator, systemvalue) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    req.body[i].SymptomdataDate = formatter.format(new Date(req.body[i].SymptomdataDate));
                    rec.push(req.body[i].Regn_No);
                    rec.push(req.body[i].system);
                    rec.push(req.body[i].NASK);
                    rec.push(req.body[i].NAD);
                    rec.push(req.body[i].Comments);
                    rec.push(req.body[i].symptom);
                    rec.push(req.body[i].Symptomval);
                    rec.push(req.body[i].VisitID);
                    rec.push(req.body[i].SymptomdataDate);
                    rec.push(req.body[i].Operator);
                    rec.push(req.body[i].systemvalue);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {

                        res.json({ "Error": false, "Message": 'system review saved', '': rows });

                    }
                });
            }
        });
    },

    immun: function (connection, req, res) {
        var query = "SELECT * FROM paed_immun WHERE Regn_No= '" + req.query.regn_no + "' ORDER BY DueDt,Serial "
        this.executeQuery(connection, res, query, 'Success', 'immun');
    },
    stdimmun: function (connection, req, res) {
        var query = "SELECT * From Paed_StdImmun ORDER BY Serial,Age "
        this.executeQuery(connection, res, query, 'Success', 'stdimmun');
    },
    saveStdImmun: function (connection, req, res) {
        var query = "DELETE FROM paed_immun WHERE Regn_No=" + req.body[0].Regn_No;
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO paed_immun (Regn_No, Serial,Vaccine,Dose,Route, DueDt,Related,Age,Days) VALUES ?";

                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    rec.push(req.body[i].Regn_No);
                    rec.push(req.body[i].Serial);
                    rec.push(req.body[i].Vaccine);
                    rec.push(req.body[i].Dose);
                    rec.push(req.body[i].Route);
                    if (req.body[i].DueDt) {
                        req.body[i].DueDt = formatter.format(new Date(req.body[i].DueDt));
                        rec.push(req.body[i].DueDt);
                    } else {
                        rec.push('');
                    }
                    rec.push(req.body[i].Related);
                    rec.push(req.body[i].Age);
                    rec.push(req.body[i].Days);
                    records.push(rec);
                }
                query = mysql.format(query2, [records]);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Immunisation updated', 'Items': rows });
                    }
                });
            }
        });
    },
    saveRegenerateStd: function (connection, req, res) {
        var query = "DELETE FROM paed_immun WHERE Regn_No=" + req.body[0].Regn_No + " and (GivenDt = '0000-00-00' or GivenDt is null )";
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO paed_immun (Regn_No, Serial,Vaccine,Route,Dose, DueDt,Related,Age,Days) VALUES ?";

                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    rec.push(req.body[i].Regn_No);
                    rec.push(req.body[i].Serial);
                    rec.push(req.body[i].Vaccine);
                    rec.push(req.body[i].Route);
                    rec.push(req.body[i].Dose);
                    if (req.body[i].DueDt) {
                        req.body[i].DueDt = formatter.format(new Date(req.body[i].DueDt));
                        rec.push(req.body[i].DueDt);
                    } else {
                        rec.push('');
                    }
                    rec.push(req.body[i].Related);
                    rec.push(req.body[i].Age);
                    rec.push(req.body[i].Days);
                    records.push(rec);
                }
                query = mysql.format(query2, [records]);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Immunisation updated', 'Items': rows });
                    }
                });
            }
        });
    },

    saveImmun: function (connection, req, res) {
        var query = "DELETE FROM paed_immun WHERE Regn_No=" + req.body[0].Regn_No;
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO paed_immun (Regn_No, Serial,Vaccine, Dose,Route, DueDt,Related,Age,Batch,Givenby,ImmunDate,Operator,VisitID,GivenDt,Days) VALUES ?";

                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    rec.push(req.body[i].Regn_No);
                    rec.push(req.body[i].Serial);
                    rec.push(req.body[i].Vaccine);
                    rec.push(req.body[i].Dose);

                    rec.push(req.body[i].Route);
                    if (req.body[i].DueDt === null || req.body[i].DueDt === '0000-00-00') {
                        rec.push('');
                    } else {
                        req.body[i].DueDt = formatter.format(new Date(req.body[i].DueDt));
                        rec.push(req.body[i].DueDt);
                    }
                    rec.push(req.body[i].Related);
                    rec.push(req.body[i].Age);
                    rec.push(req.body[i].Batch);
                    rec.push(req.body[i].Givenby);
                    // if (req.body[i].ImmunDate) {
                    //     req.body[i].ImmunDate = formatter.format(new Date(req.body[i].ImmunDate));
                    //     rec.push(req.body[i].ImmunDate);
                    // } else {
                    //     rec.push('0000-0-0 00:00:00');
                    // }
                    if (req.body[i].ImmunDate === null || req.body[i].ImmunDate === '0000-00-00') {
                        rec.push('');
                    } else {
                        req.body[i].ImmunDate = formatter.format(new Date());
                        rec.push(req.body[i].ImmunDate);
                    }
                    rec.push(req.body[i].Operator);
                    rec.push(req.body[i].VisitID);
                    if (req.body[i].GivenDt === null || req.body[i].GivenDt === '0000-00-00') {
                        rec.push('0000-00-00');
                    } else {
                        req.body[i].GivenDt = formatter.format(new Date(req.body[i].GivenDt));
                        rec.push(req.body[i].GivenDt);
                    }
                    rec.push(req.body[i].Days);
                    records.push(rec);
                }
                query = mysql.format(query2, [records]);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Immunisation updated', 'Items': rows });
                    }
                });
            }
        });
    },
    saveStdmaster: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE paed_stdimmun SET Route=?, Days=?, Repeats=?, Till=?, Vaccine=?, Age=? WHERE ID=?";
            var message = "vaccine Updated"
        } else {
            var query = "INSERT INTO paed_stdimmun ( Route, Days,  Repeats, Till, Vaccine, Age ) VALUES (?,?,?,?,?,?)";
            var message = "vaccine Added"
        }
        var table = [req.body.Route, req.body.Days, req.body.Repeats, req.body.Till, req.body.Vaccine, req.body.Age, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    delstdimmun: function (connection, req, res) {
        var query = "DELETE FROM paed_stdimmun WHERE ID='" + req.query.ID + "'";
        this.executeQuery(connection, res, query, 'vaccine deleted');
    },
    paedgrowth: function (connection, req, res) {
        var query = "SELECT * FROM paed_growth WHERE Regn_No= '" + req.query.regn_no + "' ORDER BY Age "
        this.executeQuery(connection, res, query, 'Success', 'PaedGrowth');
    },
    savegrowchart: function (connection, req, res) {
        var query = "DELETE FROM paed_growth WHERE Regn_No=" + req.body[0].Regn_No + " and  Age >= " + req.query.start + " and Age <= " + req.query.end + " "
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO paed_growth (Regn_No, Date,Age,Weight, Height,MUAC, HC,Operator) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    req.body[i].Date = formatter.format(new Date());
                    rec.push(req.body[i].Regn_No);
                    rec.push(req.body[i].Date);
                    rec.push(req.body[i].Age);
                    rec.push(req.body[i].Weight);
                    rec.push(req.body[i].Height);
                    rec.push(req.body[i].MUAC);
                    rec.push(req.body[i].HC);
                    rec.push(req.body[i].Operator);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {

                        res.json({ "Error": false, "Message": 'Grow Chart saved', '': rows });

                    }
                });
            }
        });
    },
    visitdropdown: function (connection, req, res) {
        var query = "SELECT * FROM visitdropdown  ORDER BY ID "
        this.executeQuery(connection, res, query, 'Success', 'visitdropdown');
    },
    gynaecology: function (connection, req, res) {
        var query = "SELECT * FROM gynae_gynae1 WHERE Regn_No= '" + req.query.regn_no + "' ORDER BY PregnancyNo desc "
        this.executeQuery(connection, res, query, 'Success', 'Gynaecology');
    },
    antenatalChart: function (connection, req, res) {
        var query = " SELECT * FROM gynae_anc2 WHERE Regn_No='" + req.query.regn_no + "' AND PregnancyNo ='" + req.query.maxpregno + "'ORDER BY anc2Date desc  "
        this.executeQuery(connection, res, query, 'Success', 'antenatalChart');
    },
    pregriskfactor: function (connection, req, res) {
        var query = " SELECT * FROM gynae_preg WHERE Regn_No='" + req.query.regn_no + "' AND PregnancyNo ='" + req.query.maxpregno + "'  "
        this.executeQuery(connection, res, query, 'Success', 'riskfactor');
    },
    saveAntechart: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO gynae_anc2 (Regn_No, Fundal, FHS, Presentation, FM, Fetuses, Remarks, PregnancyNo, anc2Date, US, weight, bp, usug,RiskFactor, Operator,hb) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        } else {
            var query = "UPDATE gynae_anc2 SET Regn_No=?, Fundal=?, FHS=?, Presentation=?, FM=?, Fetuses=?, Remarks=?, PregnancyNo=?, anc2Date=?, US=?, weight=?, bp=?, usug=?, RiskFactor=? , Operator=?, hb=? WHERE ID=?";
        }
        var table = [req.body.Regn_No, req.body.Fundal, req.body.FHS, req.body.Presentation, req.body.FM, req.body.Fetuses, req.body.Remarks, req.body.PregnancyNo, formatter.format(new Date(req.body.anc2Date)), req.body.US, req.body.weight, req.body.bp, req.body.usug, req.body.RiskFactor, req.body.Operator, req.body.hb, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                if (req.body.Riskcontent === 1) {
                    var query2 = 'UPDATE gynae_preg SET Complications=? WHERE Regn_No=? and PregnancyNo=?';
                } else {
                    var query2 = 'INSERT INTO gynae_preg (Complications, Regn_No, PregnancyNo)  VALUES (?,?,?)';
                }

                var table2 = [req.body.RiskFactor, req.body.Regn_No, req.body.PregnancyNo];
                query2 = mysql.format(query2, table2);
                connection.query(query2, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": "Error executing MySQL query" });
                    } else {
                        res.json({ "Error": false, "Message": "Antenatal Updated" });
                    }
                });

            }
        });
    },
    saveGynaecology: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE gynae_gynae1 SET PregnancyNo=?, Lmp=?, IVF=?, ParityChilds=?, ParityAbortions=?, MarriedYrs=?, Pregnant=?, PregTest=?, Edd=?, Scan=?, Confirm=? , Risk=?, Regn_No=? ,Operator =?, Menarche=?,Cycle=?,Regular=?,Duration=?,Flow=?,Category=?,PMS=?,Dysmen=?,menorhag=?,bleeding=?,postmeno=?,Type=?,ContraFrom=?,ContraTo=?,ContraRem=? WHERE ID=?";
            var message = "Gynaecology Updated"
        } else {
            var query = "INSERT INTO gynae_gynae1 ( PregnancyNo, Lmp, IVF,  ParityChilds, ParityAbortions, MarriedYrs, Pregnant, PregTest, Edd, Scan, Confirm, Risk,Regn_No,Operator,Menarche,Cycle,Regular,Duration,Flow,Category,PMS,Dysmen,menorhag,bleeding,postmeno,Type,ContraFrom,ContraTo,ContraRem) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            var message = "Gynaecology Added"
        }
        if (req.body.Lmp === null || req.body.Lmp === '0000-00-00' || req.body.Lmp === '') {
            req.body.Lmp = null;
        } else {
            req.body.Lmp = formatter.format(new Date(req.body.Lmp));
        }
        if (req.body.IVF === null || req.body.IVF === '0000-00-00' || req.body.IVF === '') {
            req.body.IVF = null;
        } else {
            req.body.IVF = formatter.format(new Date(req.body.IVF));
        }
        if (req.body.PregTest === null || req.body.PregTest === '0000-00-00' || req.body.PregTest === '') {
            req.body.PregTest = null;
        } else {
            req.body.PregTest = formatter.format(new Date(req.body.PregTest));
        }
        if (req.body.Edd === null || req.body.Edd === '0000-00-00' || req.body.Edd === '') {
            req.body.Edd = null;
        } else {
            req.body.Edd = formatter.format(new Date(req.body.Edd));
        }
        if (req.body.Scan === null || req.body.Scan === '0000-00-00' || req.body.Scan === '') {
            req.body.Scan = null;
        } else {
            req.body.Scan = formatter.format(new Date(req.body.Scan));
        }
        if (req.body.Confirm === null || req.body.Confirm === '0000-00-00' || req.body.Confirm === '') {
            req.body.Confirm = null;
        } else {
            req.body.Confirm = formatter.format(new Date(req.body.Confirm));
        }
        var table = [req.body.PregnancyNo, req.body.Lmp, req.body.IVF, req.body.ParityChilds, req.body.ParityAbortions, req.body.MarriedYrs, req.body.Pregnant, req.body.PregTest, req.body.Edd, req.body.Scan, req.body.Confirm, req.body.Risk, req.body.Regn_No, req.body.Operator, req.body.Menarche, req.body.Cycle, req.body.Regular, req.body.Duration, req.body.Flow, req.body.Category, req.body.PMS, req.body.Dysmen, req.body.menorhag, req.body.bleeding, req.body.postmeno, req.body.Type, req.body.ContraFrom, req.body.ContraTo, req.body.ContraRem, req.body.ID];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },

    getGynaeExam: function (connection, req, res) {
        if (req.query.visitid) {
            var query = "SELECT * FROM gynae_gynae2 WHERE VisitID=" + req.query.visitid + " order by id desc";
        } else {
            var query = "SELECT * FROM gynae_gynae2 WHERE PageID=" + req.query.pageid;
        }
        this.executeQuery(connection, res, query, 'Success', 'GynaeExam');
    },

    savegynaeExam: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE gynae_gynae2 SET Gynae2Date='" + formatter.format(new Date(req.body.Gynae2Date)) + "',Regn_No=?,VisitID=?,Consultant=?, BreastInspection=?, BreastPalpation=?, LymphNodes=?, AbdoInspection=?, AbdoPalpation=?, AbdoAuscultation=?, Inspection=?, Speculum=?, PV=?, PR=?  WHERE ID=?";
        } else {
            var query = "INSERT INTO gynae_gynae2 (Gynae2Date,Regn_No,VisitID,Consultant,BreastInspection,BreastPalpation,LymphNodes,AbdoInspection,AbdoPalpation,AbdoAuscultation,Inspection,Speculum,PV,PR) VALUES ('" + formatter.format(new Date()) + "',?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Regn_No, req.body.VisitID, req.body.Consultant, req.body.BreastInspection, req.body.BreastPalpation, req.body.LymphNodes,
        req.body.AbdoInspection, req.body.AbdoPalpation, req.body.AbdoAuscultation, req.body.Inspection,
        req.body.Speculum, req.body.PV, req.body.PR, req.body.ID];
        var query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var gynaeexamid = result.insertId
                if (method === 'post') {
                    var query2 = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, TemplateType, Operator) VALUES (?,?,?,?,?)";
                    var table2 = [req.body.VisitID, formatter.format(new Date()), 'Gynae Exam', 114, req.body.Consultant];
                    query2 = mysql.format(query2, table2);
                    connection.query(query2, function (err, result, fields) {
                        if (err) {
                            res.json({ "Error": true, "Message": err.sqlMessage });
                        } else {
                            // res.json({ "Error": false, "Message": result.insertId});
                            var query3 = "UPDATE gynae_gynae2 set PageID =" + result.insertId + " where id =" + gynaeexamid;
                            connection.query(query3, function (err, result, fields) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                    res.json({ "Error": false, "Message": 'Gynae Exam Saved' });
                                }
                            });
                        }
                    });
                }
            }
        });
    },

    getsurgicalreqform: function (connection, req, res) {
        var query = "SELECT * FROM surgeryreqform WHERE  PageID= '" + req.query.pageid + "' "
        this.executeQuery(connection, res, query, 'Success', 'surgerydetails');
    },
    savesurgicalreqform: function (connection, req, res, method) {
        if (method === 'put') {
            lDOS = false;
            lDOA = false;
            var query = "UPDATE surgeryreqform SET Regn_No=?,VisitID=?,Surgeon=?,Assistant=?,PrefHospital=?,SelfPay=?,Insurance=?,SameDayAdmit=?,ELOS=?,DayCaseSurgery=?,SurgeryDesc=?,AsstReq=?,AsstName=?,PatPos=?,PatPosOthers=?,Equipment=?,Anesthesia=?,SpecialInstruct=?,Intra=?,CArm=?,Arthroscope=?,Neuro=?,Microscope=?,Ultrasound=?,LabSpecial=?,Physiotherapist=?,PhyTherapy=?,HomeNursing=?,Hours=?,FA=?,RegularArmSling=?,Neutral=?,Knee=?,OtherAsstDevice=?,Postop=?,Operator=?,ProcedureDuration=?,Nondirectinsurance=?,PostOpPresc=?, CurrentDate=?, PageID=?"
            if (req.body.DOS > '') {
                query += ", DOS=?"
                lDOS = true;
            }
            if (req.body.DOA > '') {
                query += ", DOA=?"
                lDOA = true;
            }
            query += " WHERE ID=?";
            var message = "Surgery Request form updated"
            var table = [req.body.Regn_No, req.body.VisitID, req.body.Surgeon, req.body.Assistant, req.body.PrefHospital, req.body.SelfPay, req.body.Insurance, req.body.SameDayAdmit,
            req.body.ELOS, req.body.DayCaseSurgery, req.body.SurgeryDesc, req.body.AsstReq, req.body.AsstName, req.body.PatPos, req.body.PatPosOthers, req.body.Equipment, req.body.Anesthesia, req.body.SpecialInstruct, req.body.Intra, req.body.CArm, req.body.Arthroscope, req.body.Neuro, req.body.Microscope, req.body.Ultrasound, req.body.LabSpecial, req.body.Physiotherapist, req.body.PhyTherapy, req.body.HomeNursing, req.body.Hours, req.body.FA, req.body.RegularArmSling, req.body.Neutral, req.body.Knee, req.body.OtherAsstDevice, req.body.Postop, req.body.Operator, req.body.ProcedureDuration, req.body.Nondirectinsurance, req.body.PostOpPresc, formatter.format(new Date()), req.body.PageID]
            if (lDOS) {
                table.push(formatter.format(new Date(req.body.DOS)));
            }
            if (lDOA) {
                table.push(formatter.format(new Date(req.body.DOA)));
            }
            table.push(req.body.ID);
            query = mysql.format(query, table);
            this.executeQuery(connection, res, query, message);
        } else {
            lDOS = false;
            lDOA = false;
            var query = "INSERT INTO surgeryreqform (Regn_No,VisitID,Surgeon,Assistant,PrefHospital,SelfPay,Insurance,SameDayAdmit,ELOS,DayCaseSurgery,SurgeryDesc,AsstReq,AsstName,PatPos,PatPosOthers,Equipment,Anesthesia,SpecialInstruct,Intra,CArm,Arthroscope,Neuro,Microscope,Ultrasound,LabSpecial,Physiotherapist,PhyTherapy,HomeNursing,Hours,FA,RegularArmSling,Neutral,Knee,OtherAsstDevice,Postop,Operator,ProcedureDuration,Nondirectinsurance,PostOpPresc,CurrentDate, PageID";
            if (req.body.DOS > '') {
                query += ", DOS"
                lDOS = true;
            }
            if (req.body.DOA > '') {
                query += ", DOA"
                lDOA = true;
            }
            query += ")VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
            if (lDOS) {
                query += ", ?"
            }
            if (lDOA) {
                query += ", ?"
            }
            query += ")";
            var message = "Surgery Request form Added"

            var table = [req.body.Regn_No, req.body.VisitID, req.body.Surgeon, req.body.Assistant, req.body.PrefHospital, req.body.SelfPay, req.body.Insurance, req.body.SameDayAdmit,
            req.body.ELOS, req.body.DayCaseSurgery, req.body.SurgeryDesc, req.body.AsstReq, req.body.AsstName, req.body.PatPos, req.body.PatPosOthers, req.body.Equipment, req.body.Anesthesia, req.body.SpecialInstruct, req.body.Intra, req.body.CArm, req.body.Arthroscope, req.body.Neuro, req.body.Microscope, req.body.Ultrasound, req.body.LabSpecial, req.body.Physiotherapist, req.body.PhyTherapy, req.body.HomeNursing, req.body.Hours, req.body.FA, req.body.RegularArmSling, req.body.Neutral, req.body.Knee, req.body.OtherAsstDevice, req.body.Postop, req.body.Operator, req.body.ProcedureDuration, req.body.Nondirectinsurance, req.body.PostOpPresc, formatter.format(new Date()), req.body.PageID]
            if (lDOS) {
                table.push(formatter.format(new Date(req.body.DOS)));
            }
            if (lDOA) {
                table.push(formatter.format(new Date(req.body.DOA)));
            }
            query = mysql.format(query, table);
            this.executeQuery(connection, res, query, message);
        }
    },
    getfallrisk: function (connection, req, res) {
        var query = "SELECT * FROM fallriskassessment WHERE  PageID= '" + req.query.pageid + "' "
        this.executeQuery(connection, res, query, 'Success', 'FallRisk');
    },

    savefallrisk: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE fallriskassessment SET Regn_No=?,VisitID=?,dtExam=?,history=?,diagnosis=?,ambu=?,iv=?,gait=?,mental=?,morsetotal=?,morserisk=?,old60=?,medic=?,morseaction=?, PageID=?, Operator=?,comments=? WHERE ID=" + req.body.ID;
            var message = "Fall Risk Assessment form updated"
        } else {
            var query = "INSERT INTO fallriskassessment (Regn_No,VisitID,dtExam,history,diagnosis,ambu,iv,gait,mental,morsetotal,morserisk,old60,medic,morseaction, PageID, Operator,comments) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            var message = "Fall Risk Assessment form Added"
        }
        var table = [req.body.Regn_No, req.body.VisitID, formatter.format(new Date()), req.body.history, req.body.diagnosis, req.body.ambu, req.body.iv, req.body.gait, req.body.mental, req.body.morsetotal,
        req.body.morserisk, req.body.old60, req.body.medic, req.body.morseaction, req.body.PageID, req.body.Operator, req.body.comments]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    getcontraceptives: function (connection, req, res) {
        var query = "SELECT * FROM contraceptives WHERE  Category= '" + req.query.category + "' "
        this.executeQuery(connection, res, query, 'Success', 'contra');
    },
    getInsAppCPT: function (connection, req, res) {
        var search = req.query.search.split(' ');
        var query = "SELECT Code, Short_Description FROM cpt WHERE  Code = '" + search[0] + "' ORDER BY Code"
        this.executeQuery(connection, res, query, 'Success', 'insCPT');
    },


    getMobileNotes: function (connection, req, res) {
        if (req.query.pageid) {
            var query = "SELECT * FROM visitpages WHERE ID=" + req.query.pageid;
        } else {
            var query = "SELECT * FROM visitpages WHERE VisitID=" + req.query.visitID + " and TemplateType=" + req.query.TempType + " ";
        }
        this.executeQuery(connection, res, query, 'Success', 'MobileNotes');
    },
    savemobilenotes: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE visitpages SET VisitID=?,PageDate=?,PageTitle=?,PageContent=?,TemplateType=?,Operator=?  WHERE ID=" + req.body.ID;
            var message = "Mobile Notes updated"
        } else {
            var query = "INSERT INTO visitpages (VisitID,PageDate,PageTitle,PageContent,TemplateType,Operator) VALUES(?,?,?,?,?,?)";
            var message = "Mobile Notes Added"
        }
        var table = [req.body.VisitID, formatter.format(new Date()), req.body.PageTitle, req.body.PageContent, req.body.TemplateType, req.body.Operator]
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    medications: function (connection, req, res) {
        var query = "SELECT * FROM medications WHERE Regn_No='" + req.query.regn_no + "' order by MedicationsDate desc"
        this.executeQuery(connection, res, query, 'Success', 'Medications');
    },
    saveMedications: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO Medications (Regn_No, Operator,MedicationsDate,MedicationsTime,Medicinename,Unit,Strength,Dose,Route,FrequencyName,Days,Instructions,Administered,Prescribed,Past) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        } else {
            var query = "UPDATE Medications SET Regn_No=?, Operator=?,MedicationsDate=?,MedicationsTime=?,Medicinename=?,Unit=?,Strength=?,Dose=?,Route=?,FrequencyName=?,Days=?,Instructions=?,Administered=?,Prescribed=?,Past=? WHERE ID=?";
        }
        var table = [req.body.Regn_No, req.body.Operator, formatter.format(new Date(req.body.MedicationsDate)), req.body.MedicationsTime, req.body.Medicinename, req.body.Unit, req.body.Strength, req.body.Dose, req.body.Route, req.body.FrequencyName, req.body.Days, req.body.Instructions, req.body.Administered, req.body.Prescribed, req.body.Past, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Medications Added or Updated" });
            }
        });
    },
    delmedication: function (connection, req, res) {
        var query = "DELETE FROM Medications WHERE id=" + req.query.id;
        this.executeQuery(connection, res, query, 'Medications Deleted');
    },

    getdental: function (connection, req, res) {
        var query = "SELECT * FROM DentalChart WHERE Regn_No=" + req.query.regn_no + " ORDER BY DentalChartDate, ToothNo, dentalstatus";
        this.executeQuery(connection, res, query, 'Success', 'DentalChart');
    },
    savedental: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE DentalChart SET Regn_No=?, DentalChartDate=?, ToothNo=?, PrimaryTooth=?, SurfaceD=?, SurfaceL=?, SurfaceM=?, SurfaceO=?, SurfaceB=?, ActivityCode=?, Description=?, Notes=?, DentalStatus=?, Roots=?, RootsDescription=?, Mobility=?, Gums=?, Operator=?,Plaque=? WHERE ID=?";
        } else {
            var query = "INSERT INTO DentalChart (Regn_No, DentalChartDate, ToothNo, PrimaryTooth, SurfaceD, SurfaceL, SurfaceM, SurfaceO, SurfaceB, ActivityCode, Description, Notes, DentalStatus, Roots, RootsDescription, Mobility, Gums, Operator,Plaque) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Regn_No, formatter.format(new Date()), req.body.ToothNo, req.body.PrimaryTooth, req.body.SurfaceD, req.body.SurfaceL, req.body.SurfaceM, req.body.SurfaceO, req.body.SurfaceB, req.body.ActivityCode, req.body.Description, req.body.Notes, req.body.DentalStatus, req.body.Roots, req.body.RootsDescription, req.body.Mobility, req.body.Gums, req.body.Operator, req.body.Plaque, req.body.ID];
        var query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Chart marking saved');
    },

    deletedental: function (connection, req, res) {
        var query = "DELETE FROM DentalChart WHERE ID=" + req.query.id;
        this.executeQuery(connection, res, query, 'Detail deleted');
    },

    getdentalprocedures: function (connection, req, res) {
        var query = "SELECT * FROM DentalProcedures WHERE Regn_No=" + req.query.regn_no + " ORDER BY DentalProceduresDate DESC, ToothNo";
        this.executeQuery(connection, res, query, 'Success', 'DentalProcedures');
    },

    getdentalprocedure: function (connection, req, res) {
        var query = "SELECT * FROM DentalProcedures WHERE ID=" + req.query.id;
        this.executeQuery(connection, res, query, 'Success', 'DentalProcedure');
    },

    savedentalprocedure: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE DentalProcedures SET Regn_No=?, DentalProceduresDate=?, ToothNo=?, ActivityCode=?, ProcedureName=?, TimeStart=?, TimeEnd=?, AnesthesiaType=?, AnesthesiaArea=?, Anesthetic=?, Carpules=?, Needle=?, Material=?, Hygiene=?, Notes=?, Operator=? WHERE ID=?";
        } else {
            var query = "INSERT INTO DentalProcedures (Regn_No, DentalProceduresDate, ToothNo, ActivityCode, ProcedureName, TimeStart, TimeEnd, AnesthesiaType, AnesthesiaArea, Anesthetic, Carpules, Needle, Material, Hygiene, Notes, Operator) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Regn_No, formatter.format(new Date()), req.body.ToothNo, req.body.ActivityCode, req.body.ProcedureName, req.body.TimeStart, req.body.TimeEnd, req.body.AnesthesiaType, req.body.AnesthesiaArea, req.body.Anesthetic, req.body.Carpules, req.body.Needle, req.body.Material, req.body.Hygiene, req.body.Notes, req.body.Operator, req.body.ID];
        var query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Procedures saved');
    },

    deletedentalprocedure: function (connection, req, res) {
        var query = "DELETE FROM DentalProcedures WHERE ID=" + req.query.id
        this.executeQuery(connection, res, query, 'Procedure deleted');
    },

    dentaloptions: function (connection, req, res) {
        var query = "SELECT * FROM DentalOptions ORDER BY Category, Code";
        this.executeQuery(connection, res, query, 'Success', 'DentalOptions');
    },
    dentalcategory: function (connection, req, res) {
        var query = "SELECT DISTINCT(category) AS Category FROM DentalOptions ORDER BY Category";
        this.executeQuery(connection, res, query, 'Success', 'DentalCategory');
    },

    ophthalmicexam: function (connection, req, res) {
        if (req.query.visitdate) {
            var query = "SELECT * FROM ophthalmic WHERE Regn_No='" + req.query.regn_no + "' and date(dtExam) = date('" + formatter.format(new Date(req.query.visitdate)) + "') and  EXAMINER ='" + req.query.consultant + "' order by dtExam desc"
        } else {
            var query = "SELECT * FROM ophthalmic WHERE Regn_No='" + req.query.regn_no + "' order by dtExam desc"
        }
        this.executeQuery(connection, res, query, 'Success', 'ophthalmic');
    },
    saveophthalmic: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE ophthalmic SET dtExam='" + formatter.format(new Date(req.body.dtExam)) + "', Regn_No=?, lids=?, GLASSESRE=?, GLASSESADD=?, pupils=?, GLASSESLE=?, GLASSESLEADD=?, TAKRE=?, TAPRE=?, ASTIGRE=?, TAKLE=?, TAPLE=?, ASTIGLE=?, VSCRE=?, VSCCCRE=?, PHRE=?, NSCRE=?,NSCADD=?,NSCCCRE=?,VSCLE=?,VSCCCLE=?,PHLE=?,NSCLE=?,NSCLEADD=?,CCLEADD=?,RetRE=?,CycRE=?,RetLE=?,CycLE=?,RXRE=?, headposture=?, RXREADD=?,BUTRE=?,IPD=?, RXLE=?, BUT=?,RXLEADD=?,BUTLE=?,TARE=?,GonRE=?, ocularmot=?,TALE=?, GonLE=?, EXAMINER=?, OrbitRE=?, OrbitLE=?, LacrimalRE=?, LacrimalLE=?, TearFilmRE=?, TearFilmLE=?, LidsRE=?, LidsLE=?, ConjuctivaRE=?, ConjuctivaLE=?, CorneaRE=?, CorneaLE=?, ACRE=?, ACLE=?, IrisRE=?, IrisLE=?, PupilRE=?, PupilLE=?, LensRE=?, LensLE=?, VitreousRE=?, VitreousLE=?, remarks=? WHERE ID=?";
        } else {
            var query = "INSERT INTO ophthalmic (dtExam,Regn_No,lids, GLASSESRE, GLASSESADD, pupils, GLASSESLE, GLASSESLEADD, TAKRE, TAPRE, ASTIGRE, TAKLE, TAPLE, ASTIGLE, VSCRE, VSCCCRE,PHRE,NSCRE,NSCADD,NSCCCRE,VSCLE,VSCCCLE,PHLE,NSCLE,NSCLEADD,CCLEADD,RetRE,CycRE,RetLE,CycLE,RXRE, headposture, RXREADD,BUTRE,IPD, RXLE, BUT,RXLEADD,BUTLE,TARE,  GonRE, ocularmot,TALE, GonLE, EXAMINER,OrbitRE,OrbitLE,LacrimalRE,LacrimalLE,TearFilmRE,TearFilmLE,LidsRE,LidsLE,ConjuctivaRE,ConjuctivaLE,CorneaRE,CorneaLE,ACRE,ACLE,IrisRE,IrisLE,PupilRE,PupilLE,LensRE,LensLE,VitreousRE,VitreousLE,remarks) VALUES ('" + formatter.format(new Date()) + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Regn_No, req.body.lids, req.body.GLASSESRE, req.body.GLASSESADD, req.body.pupils, req.body.GLASSESLE, req.body.GLASSESLEADD, req.body.TAKRE, req.body.TAPRE, req.body.ASTIGRE, req.body.TAKLE, req.body.TAPLE, req.body.ASTIGLE, req.body.VSCRE, req.body.VSCCCRE, req.body.PHRE, req.body.NSCRE, req.body.NSCADD, req.body.NSCCCRE, req.body.VSCLE, req.body.VSCCCLE, req.body.PHLE, req.body.NSCLE, req.body.NSCLEADD, req.body.CCLEADD, req.body.RetRE, req.body.CycRE, req.body.RetLE, req.body.CycLE, req.body.RXRE, req.body.headposture, req.body.RXREADD, req.body.BUTRE, req.body.IPD, req.body.RXLE, req.body.BUT, req.body.RXLEADD, req.body.BUTLE, req.body.TARE, req.body.GonRE, req.body.ocularmot, req.body.TALE, req.body.GonLE, req.body.EXAMINER, req.body.OrbitRE, req.body.OrbitLE, req.body.LacrimalRE, req.body.LacrimalLE, req.body.TearFilmRE, req.body.TearFilmLE, req.body.LidsRE, req.body.LidsLE, req.body.ConjuctivaRE, req.body.ConjuctivaLE, req.body.CorneaRE, req.body.CorneaLE, req.body.ACRE, req.body.ACLE, req.body.IrisRE, req.body.IrisLE, req.body.PupilRE, req.body.PupilLE, req.body.LensRE, req.body.LensLE, req.body.VitreousRE, req.body.VitreousLE, req.body.remarks, req.body.ID];
        var query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Ophthalmic exam saved');
    },
    consentsigned: function (connection, req, res) {
        var query = "SELECT * FROM visitpages JOIN visits ON visitpages.VisitID = visits.ID where  visits.Regn_No='" + req.query.regn_no + "' AND visitpages.TemplateType =1 AND visitpages.SignedDate IS NOT  NULL";
        this.executeQuery(connection, res, query, 'Success', 'ConsentSigned');
    },
    commonnotes: function (connection, req, res) {
        var query = "SELECT * FROM notes WHERE Regn_No='" + req.query.regn_no + "' order by NotesDate desc,NotesTime desc"
        this.executeQuery(connection, res, query, 'Success', 'Notes');
    },
    saveCommonNotes: function (connection, req, res, method) {
        if (method === 'post') {
            var query = "INSERT INTO notes (Regn_No, Operator,NotesDate,NotesTime,Notes) VALUES (?,?,?,?,?)";
        } else {
            var query = "UPDATE notes SET Regn_No=?, Operator=?,NotesDate=?,NotesTime=?,Notes=? WHERE ID=?";
        }
        var table = [req.body.Regn_No, req.body.Operator, formatter.format(new Date(req.body.NotesDate)), req.body.NotesTime, req.body.Notes, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                res.json({ "Error": false, "Message": "Notes Added or Updated" });
            }
        });
    },
    deleteCommonNotes: function (connection, req, res) {
        var query = "DELETE FROM notes WHERE ID=" + req.query.ID;
        connection.query(query, [req.query.ID], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": "Notes Deleted" });
            }
        });
    },

    getLab(connection, req, res) {
        if (req.query.regn_no) {
            if (req.query.doctor) {
                var query = "SELECT * FROM LabTests WHERE Regn_No=" + req.query.regn_no + " and Consultant = '" + req.query.doctor + "' ORDER BY LabTestsDate DESC";
            } else {
                var query = "SELECT * FROM LabTests WHERE Regn_No=" + req.query.regn_no + " ORDER BY LabTestsDate DESC";
            }
        } else {
            if (req.query.doctor) {
                var query = "SELECT LabTests.*, Patmas.Name FROM LabTests LEFT JOIN Patmas ON LabTests.Regn_No = Patmas.Regn_No WHERE LabStatus < 5 and labtests.Consultant = '" + req.query.doctor + "' ORDER BY LabTestsDate";
            } else {
                var query = "SELECT LabTests.*, Patmas.Name FROM LabTests LEFT JOIN Patmas ON LabTests.Regn_No = Patmas.Regn_No WHERE LabStatus < 5 ORDER BY LabTestsDate";
            }
        }
        this.executeQuery(connection, res, query, 'Success', 'LabTests');
    },

    saveLab(connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE LabTests SET LabTestsDate=?, Regn_No=?, Tests =?, LabStatus=?, StatusDate=?, Notes=?, Consultant=?";
        } else {
            var query = "INSERT INTO LabTests (LabTestsDate, Regn_No, Tests, LabStatus, StatusDate, Notes, Consultant) VALUES (?,?,?,?,?,?,?)"
        }
        var table = [formatter.format(new Date()), req.body.Regn_No, req.body.Tests, req.body.LabStatus, formatter.format(new Date(req.body.StatusDate)), req.body.Notes, req.body.Consultant];
        query = mysql.format(query, table);
        self = this;
        connection.query(query, function (err, result) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                self.saveLabItems(connection, req, res, result.insertId);
            }
        });
    },

    saveLabItems(connection, req, res, labid) {
        var query = "DELETE FROM LabItems WHERE LabID=" + labid + ";INSERT INTO LabItems (LabID, Description, ActivityCode, RateNo) VALUES ?"
        var records = new Array();
        for (var i = 0; i < req.body.Items.length; i++) {
            var rec = new Array();
            rec.push(labid);
            rec.push(req.body.Items[i].Description);
            rec.push(req.body.Items[i].ActivityCode);
            rec.push(req.body.Items[i].RateNo);
            records.push(rec);
        }
        connection.query(query, [records], function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": labid, 'Items': rows });
                var query2 = "INSERT INTO LabStatus (LabStatusDate, LabID, LabStatus, Operator) VALUES (?,?,?,?)";
                var table2 = [formatter.format(new Date()), labid, 0, req.body.Consultant];
                query2 = mysql.format(query2, table2);
                connection.query(query2, function (err, rows) {
                    if (err) {
                        console.log(err);
                    }
                });
            }
        });
    },

    getLabAttachment: function (connection, req, res) {
        var query = "SELECT * FROM Attachments WHERE LabID=" + req.query.labid;
        this.executeQuery(connection, res, query, 'Success', 'Attachments');
    },

    getLabStatus: function (connection, req, res) {
        var query = "SELECT * FROM LabStatus WHERE LabID=" + req.query.ID + " ORDER BY LabStatusDate";
        this.executeQuery(connection, res, query, 'Success', 'LabStatus');
    },
    getLabItems: function (connection, req, res) {
        var query = "SELECT * FROM labitems WHERE LabID=" + req.query.ID + " ORDER BY id ";
        this.executeQuery(connection, res, query, 'Success', 'LabItems');
    },
    updateLabStatus(connection, req, res) {
        var query = "INSERT INTO LabStatus (LabStatusDate, LabID, LabStatus, Operator) VALUES (?,?,?,?)";
        query += ";UPDATE LabTests SET StatusDate= NOW(), LabStatus =" + req.body.NewStatus + " WHERE ID=" + req.body.ID;
        var table = [formatter.format(new Date()), req.body.ID, req.body.NewStatus, req.body.Consultant];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Success');
    },

    getLabTemplates: function (connection, req, res) {
        var query = "SELECT * FROM LabTemplates WHERE InsuranceID=" + req.query.insurance + " ORDER BY Description";
        this.executeQuery(connection, res, query, 'Success', 'LabTemplates');
    },

    getLabTemplateItems: function (connection, req, res) {
        if (req.query.insurance > 0) {
            var query = "SELECT LabTemplateItems.RateNo, InsuranceRate.* FROM LabTemplateItems LEFT JOIN InsuranceRate ON LabTemplateItems.RateNo = InsuranceRate.ID WHERE LabTemplateItems.TemplateID=" + req.query.id + " ORDER BY Description";
        } else {
            var query = "SELECT LabTemplateItems.RateNo, Ratemaster.* FROM LabTemplateItems LEFT JOIN Ratemaster ON LabTemplateItems.RateNo = Ratemaster.RateNo WHERE LabTemplateItems.TemplateID=" + req.query.id + " ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'LabTemplateItems');
    },

    saveLabTemplate: function (connection, req, res) {
        var query = "INSERT INTO LabTemplates (InsuranceID, Description, Operator) VALUES (?, ?, ?)";
        var table = [req.body.InsuranceID, req.body.Description, req.body.Operator];
        query = mysql.format(query, table);
        connection.query(query, function (err, result) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO LabTemplateItems (TemplateID, Serial, ActivityCode, RateNo) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.Items.length; i++) {
                    var rec = new Array();
                    rec.push(result.insertId);
                    rec.push(req.body.Items[i].Serial);
                    rec.push(req.body.Items[i].ActivityCode);
                    rec.push(req.body.Items[i].RateNo);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.json({ "Error": false, "Message": 'Template saved', 'Items': rows });
                    }
                });

            }
        });
    },

    deleteLabTemplate: function (connection, req, res) {
        var query = "DELETE FROM LabTemplates WHERE ID=" + req.query.templateID + ";DELETE FROM LabTemplateItems WHERE TemplateID=" + req.query.templateID;
        this.executeQuery(connection, res, query);
    },

    cptcode: function (connection, req, res) {
        var query = "SELECT * FROM procedures JOIN procedurescpt ON procedures.PageID = procedurescpt.PageID  WHERE procedures.Regn_No =" + req.query.Regn_no;
        this.executeQuery(connection, res, query, 'Success', 'CPTCode');
    },

    encounter: function (connection, req, res) {
        var query = "SELECT visits.ID AS VisitID,visits.Regn_No,diagnosis.DiagnosisDate,diagnosis.ICD,diagnosis.Diagnosis,complaints.Complaints,visits.Consultant,visits.VisitDate FROM visits left JOIN diagnosis ON visits.ID = diagnosis.VisitID left JOIN complaints ON visits.ID=complaints.VisitID WHERE visits.Regn_No =" + req.query.Regn_no;
        this.executeQuery(connection, res, query, 'Success', 'Encounters');
    },

    glassespresc: function (connection, req, res) {
        if (req.query.pageid !== 'false') {
            var query = "SELECT * FROM glassespresc WHERE PageID='" + req.query.pageid + "'"
        } else {
            if (req.query.visitdate) {
                var query = "SELECT * FROM glassespresc WHERE Regn_No='" + req.query.regn_no + "' and date(GlassesprescDate) = date('" + formatter.format(new Date(req.query.visitdate)) + "') and  Consultant ='" + req.query.consultant + "' order by GlassesprescDate desc"
            } else {
                var query = "SELECT * FROM glassespresc WHERE Regn_No='" + req.query.regn_no + "' order by GlassesprescDate desc"
            }
        }
        this.executeQuery(connection, res, query, 'Success', 'Glassespresc');
    },
    saveglasses: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE glassespresc SET GlassesprescDate='" + formatter.format(new Date(req.body.GlassesprescDate)) + "', Regn_No=?, Consultant=?, RDShp=?, RDCyl=?, RDAxis=?, RDVisual=?,LDShp=?, LDCyl=?, LDAxis=?, LDVisual=?, RNShp=?, RNCyl=?, RNAxis=?, RNVisual=?, LNShp=?,LNCyl=?, LNAxis=?,LNVisual=?,RAdd=?,LAdd=?,RDec=?,LDec=?,Form=?,Tint=?,LensType=?,SegDetails=?,IPD=?,LensSize=?,LensShape=?,DistanceCenters=?,NearCenters=?, Remarks=?,Power=?,Diameter=?, BaseCurve=?, ContactLensType=? WHERE ID=?";
        } else {
            var query = "INSERT INTO glassespresc (GlassesprescDate,Regn_No,Consultant, RDShp, RDCyl, RDAxis, RDVisual, LDShp, LDCyl, LDAxis, LDVisual, RNShp, RNCyl, RNAxis, RNVisual, LNShp,LNCyl,LNAxis,LNVisual,RAdd,LAdd,RDec,LDec,Form,Tint,LensType,SegDetails,IPD,LensSize,LensShape,DistanceCenters, NearCenters, Power,Diameter,BaseCurve, ContactLensType) VALUES ('" + formatter.format(new Date()) + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Regn_No, req.body.Consultant, req.body.RDShp, req.body.RDCyl, req.body.RDAxis, req.body.RDVisual, req.body.LDShp, req.body.LDCyl, req.body.LDAxis, req.body.LDVisual, req.body.RNShp, req.body.RNCyl, req.body.RNAxis, req.body.RNVisual, req.body.LNShp, req.body.LNCyl, req.body.LNAxis, req.body.LNVisual, req.body.RAdd, req.body.LAdd, req.body.RDec, req.body.LDec, req.body.Form, req.body.Tint, req.body.LensType, req.body.SegDetails, req.body.IPD, req.body.LensSize, req.body.LensShape, req.body.DistanceCenters, req.body.NearCenters, req.body.Remarks, req.body.Power, req.body.Diameter, req.body.BaseCurve, req.body.ContactLensType, req.body.ID];
        var query = mysql.format(query, table);
        // this.executeQuery(connection, res, query, 'Glasses Prescription saved');
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var glassesid;
                if (method === 'put') {
                    glassesid = req.body.ID;
                } else {
                    glassesid = result.insertId;
                    var query2 = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, TemplateType, Operator) VALUES (?,?,?,?,?)";
                    var table2 = [req.body.VisitID, formatter.format(new Date()), 'Glasses Prescription', 112, req.body.Consultant];
                    query2 = mysql.format(query2, table2);
                    connection.query(query2, function (err, result, fields) {
                        if (err) {
                            res.json({ "Error": true, "Message": err.sqlMessage });
                        } else {
                            // res.json({ "Error": false, "Message": result.insertId});
                            var query3 = "UPDATE glassespresc set PageID =" + result.insertId + " where id =" + glassesid;
                            connection.query(query3, function (err, result, fields) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                    res.json({ "Error": false, "Message": glassesid });
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    ophbaseexam: function (connection, req, res) {
        if (req.query.pageid) {
            var query = "SELECT * FROM ophth_baseexam WHERE PageID='" + req.query.pageid + "'";
        } else if (req.query.visitid) {
            var query = "SELECT * FROM ophth_baseexam WHERE visitid='" + req.query.visitid + "'";
        } else {
            if (req.query.visitdate) {
                var query = "SELECT * FROM ophth_baseexam WHERE Regn_No='" + req.query.regn_no + "' and date(OphBaseExamDate) = date('" + formatter.format(new Date(req.query.visitdate)) + "') and  Consultant ='" + req.query.Consultant + "' order by OphBaseExamDate desc"
            } else {
                var query = "SELECT * FROM ophth_baseexam WHERE Regn_No='" + req.query.regn_no + "' order by OphBaseExamDate desc"
            }
        }
        this.executeQuery(connection, res, query, 'Success', 'OphBaseExam');
    },

    saveophbaseexam: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE ophth_baseexam SET OphBaseExamDate='" + formatter.format(new Date(req.body.OphBaseExamDate)) + "', Regn_No=?,VisitID=?,Consultant=?, vamethod=?, rdistsc=?, ldistsc=?, rdistcc=?, ldistcc=?, rdistphsc=?, ldistphsc=?, rdistphcc=?, ldistphcc=?, rnearsc=?, lnearsc=?, rnearcc=?, lnearcc=?, rwearingrxsph=?, rwearingrxcyl=?, rwearingrxaxis=?, lwearingrxsph=?, lwearingrxcyl=?, lwearingrxaxis=?, rmanifestautosph=?, rmanifestautocyl=?, rmanifestautoaxis=?, rmanifestautodistva=?, rmanifestautoadd=?, lmanifestautosph=?, lmanifestautocyl=?, lmanifestautoaxis=?, lmanifestautodistva=?, lmanifestautoadd=?, rmanifestsph=?, rmanifestcyl=?, rmanifestaxis=?, rmanifestdistva=?, rmanifestadd=?, lmanifestsph=?, lmanifestcyl=?, lmanifestaxis=?, lmanifestdistva=?, lmanifestadd=?, rcycloplegicautosph=?, rcycloplegicautocyl=?, rcycloplegicautoaxis=?, rcycloplegicautodistva=?, rcycloplegicautoadd=?, rcycloplegicautonearva=?, lcycloplegicautosph=?, lcycloplegicautocyl=?, lcycloplegicautoaxis=?, lcycloplegicautodistva=?, lcycloplegicautoadd=?, lcycloplegicautonearva=?, rcycloplegicsph=?, rcycloplegiccyl=?, rcycloplegicaxis=?, rcycloplegicdistva=?, rcycloplegicadd=?, rcycloplegicnearva=?, lcycloplegicsph=?, lcycloplegiccyl=?, lcycloplegicaxis=?, lcycloplegicdistva=?, lcycloplegicadd=?, lcycloplegicnearva=?, rfinalrxsph=?, rfinalrxcyl=?, rfinalrxaxis=?, rfinalrxdistva=?, rfinalrxadd=?, rfinalrxnearva=?, lfinalrxsph=?, lfinalrxcyl=?, lfinalrxaxis=?, lfinalrxdistva=?, lfinalrxadd=?, lfinalrxnearva=?, rrk1=?, rrk1axis=?, rrk2=?, rrk2axis=?, lrk1=?, lrk1axis=?, lrk2=?, lrk2axis=?, ramsler=?, lamsler=?, rpachymetry=?, lpachymetry=?, colormethod=?, rcolor=?, lcolor=?, rexternal=?, lexternal=?, rextraocular=?, lextraocular=?, rlids=?, llids=?, rconjunctiva=?, lconjunctiva=?, rcornea=?, lcornea=?, ranterior=?, lanterior=?, riris=?, liris=?, rlens=?, llens=?, rvitreous=?, lvitreous=?, rdisc=?, ldisc=?, rcdratio=?, lcdratio=?, rmacula=?, lmacula=?,rvessels=?,lvessels=?,rperiphery=?,lperiphery=?,rpressure=?,lpressure=?,tonometry=?,rdyedisappearance=?,ldyedisappearance=?,rnasolacrimalduct=?,lnasolacrimalduct=?,rpunctulupper=?,lpunctulupper=?, rpunctullower=?, lpunctullower=?, rtearmeniscus=?, ltearmeniscus=?, rtbut=?, ltbut=?, rschirmers=?, lschirmers=?,methoddate=?  WHERE ID=?";
        } else {
            var query = "INSERT INTO ophth_baseexam (OphBaseExamDate,Regn_No,VisitID,Consultant,vamethod,rdistsc,ldistsc,rdistcc,ldistcc,rdistphsc,ldistphsc,rdistphcc,ldistphcc,rnearsc,lnearsc,rnearcc,lnearcc,rwearingrxsph,rwearingrxcyl,rwearingrxaxis,lwearingrxsph,lwearingrxcyl,lwearingrxaxis,rmanifestautosph,rmanifestautocyl,rmanifestautoaxis,rmanifestautodistva,rmanifestautoadd,lmanifestautosph,lmanifestautocyl,lmanifestautoaxis,lmanifestautodistva,lmanifestautoadd,rmanifestsph,rmanifestcyl,rmanifestaxis,rmanifestdistva,rmanifestadd,lmanifestsph,lmanifestcyl,lmanifestaxis,lmanifestdistva,lmanifestadd,rcycloplegicautosph,rcycloplegicautocyl,rcycloplegicautoaxis,rcycloplegicautodistva,rcycloplegicautoadd,rcycloplegicautonearva,lcycloplegicautosph,lcycloplegicautocyl,lcycloplegicautoaxis,lcycloplegicautodistva,lcycloplegicautoadd,lcycloplegicautonearva,rcycloplegicsph,rcycloplegiccyl,rcycloplegicaxis,rcycloplegicdistva,rcycloplegicadd,rcycloplegicnearva,lcycloplegicsph,lcycloplegiccyl,lcycloplegicaxis,lcycloplegicdistva,lcycloplegicadd,lcycloplegicnearva,rfinalrxsph,rfinalrxcyl,rfinalrxaxis,rfinalrxdistva,rfinalrxadd,rfinalrxnearva,lfinalrxsph,lfinalrxcyl,lfinalrxaxis,lfinalrxdistva,lfinalrxadd,lfinalrxnearva,rrk1,rrk1axis,rrk2,rrk2axis,lrk1,lrk1axis,lrk2,lrk2axis,ramsler,lamsler,rpachymetry,lpachymetry,colormethod,rcolor,lcolor,rexternal,lexternal,rextraocular,lextraocular,rlids,llids,rconjunctiva,lconjunctiva,rcornea,lcornea,ranterior,lanterior,riris,liris,rlens,llens,rvitreous,lvitreous,rdisc,ldisc,rcdratio,lcdratio,rmacula,lmacula,rvessels,lvessels,rperiphery,lperiphery,rpressure,lpressure,tonometry,rdyedisappearance,ldyedisappearance,rnasolacrimalduct,lnasolacrimalduct,rpunctulupper,lpunctulupper,rpunctullower,lpunctullower,rtearmeniscus,ltearmeniscus,rtbut,ltbut,rschirmers,lschirmers,methoddate) VALUES ('" + formatter.format(new Date()) + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
        var table = [req.body.Regn_No, req.body.VisitID, req.body.Consultant, req.body.vamethod, req.body.rdistsc, req.body.ldistsc, req.body.rdistcc, req.body.ldistcc, req.body.rdistphsc, req.body.ldistphsc,
        req.body.rdistphcc, req.body.ldistphcc, req.body.rnearsc, req.body.lnearsc, req.body.rnearcc, req.body.lnearcc, req.body.rwearingrxsph, req.body.rwearingrxcyl,
        req.body.rwearingrxaxis, req.body.lwearingrxsph, req.body.lwearingrxcyl, req.body.lwearingrxaxis, req.body.rmanifestautosph, req.body.rmanifestautocyl, req.body.rmanifestautoaxis,
        req.body.rmanifestautodistva, req.body.rmanifestautoadd, req.body.lmanifestautosph, req.body.lmanifestautocyl, req.body.lmanifestautoaxis, req.body.lmanifestautodistva,
        req.body.lmanifestautoadd, req.body.rmanifestsph, req.body.rmanifestcyl, req.body.rmanifestaxis, req.body.rmanifestdistva, req.body.rmanifestadd, req.body.lmanifestsph,
        req.body.lmanifestcyl, req.body.lmanifestaxis, req.body.lmanifestdistva, req.body.lmanifestadd, req.body.rcycloplegicautosph, req.body.rcycloplegicautocyl,
        req.body.rcycloplegicautoaxis, req.body.rcycloplegicautodistva, req.body.rcycloplegicautoadd, req.body.rcycloplegicautonearva, req.body.lcycloplegicautosph,
        req.body.lcycloplegicautocyl, req.body.lcycloplegicautoaxis, req.body.lcycloplegicautodistva, req.body.lcycloplegicautoadd, req.body.lcycloplegicautonearva,
        req.body.rcycloplegicsph, req.body.rcycloplegiccyl, req.body.rcycloplegicaxis, req.body.rcycloplegicdistva, req.body.rcycloplegicadd, req.body.rcycloplegicnearva,
        req.body.lcycloplegicsph, req.body.lcycloplegiccyl, req.body.lcycloplegicaxis, req.body.lcycloplegicdistva, req.body.lcycloplegicadd, req.body.lcycloplegicnearva,
        req.body.rfinalrxsph, req.body.rfinalrxcyl, req.body.rfinalrxaxis, req.body.rfinalrxdistva, req.body.rfinalrxadd, req.body.rfinalrxnearva, req.body.lfinalrxsph,
        req.body.lfinalrxcyl, req.body.lfinalrxaxis, req.body.lfinalrxdistva, req.body.lfinalrxadd, req.body.lfinalrxnearva, req.body.rrk1, req.body.rrk1axis, req.body.rrk2,
        req.body.rrk2axis, req.body.lrk1, req.body.lrk1axis, req.body.lrk2, req.body.lrk2axis, req.body.ramsler, req.body.lamsler, req.body.rpachymetry, req.body.lpachymetry,
        req.body.colormethod, req.body.rcolor, req.body.lcolor, req.body.rexternal, req.body.lexternal, req.body.rextraocular, req.body.lextraocular, req.body.rlids, req.body.llids,
        req.body.rconjunctiva, req.body.lconjunctiva, req.body.rcornea, req.body.lcornea, req.body.ranterior, req.body.lanterior, req.body.riris, req.body.liris, req.body.rlens,
        req.body.llens, req.body.rvitreous, req.body.lvitreous, req.body.rdisc, req.body.ldisc, req.body.rcdratio, req.body.lcdratio, req.body.rmacula, req.body.lmacula,
        req.body.rvessels, req.body.lvessels, req.body.rperiphery, req.body.lperiphery, req.body.rpressure, req.body.lpressure, req.body.tonometry, req.body.rdyedisappearance,
        req.body.ldyedisappearance, req.body.rnasolacrimalduct, req.body.lnasolacrimalduct, req.body.rpunctulupper, req.body.lpunctulupper, req.body.rpunctullower,
        req.body.lpunctullower, req.body.rtearmeniscus, req.body.ltearmeniscus, req.body.rtbut, req.body.ltbut, req.body.rschirmers, req.body.lschirmers, formatter.format(new Date()), req.body.ID];
        var query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var baseeyeexamid = result.insertId
                if (method === 'post') {
                    var query2 = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, TemplateType, Operator) VALUES (?,?,?,?,?)";
                    var table2 = [req.body.VisitID, formatter.format(new Date()), 'Base Eye Exam', 111, req.body.Consultant];
                    query2 = mysql.format(query2, table2);
                    connection.query(query2, function (err, result, fields) {
                        if (err) {
                            res.json({ "Error": true, "Message": err.sqlMessage });
                        } else {
                            // res.json({ "Error": false, "Message": result.insertId});
                            var query3 = "UPDATE ophth_baseexam set PageID =" + result.insertId + " where id =" + baseeyeexamid;
                            connection.query(query3, function (err, result, fields) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                    res.json({ "Error": false, "Message": 'Base Eye Exam Saved' });
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    getSickleave: function (connection, req, res) {
        var query = "SELECT * FROM Sickleave WHERE PageID=" + req.query.pageID;
        this.executeQuery(connection, res, query, 'Success', 'SickLeave');
    },
    saveSickleave(connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE Sickleave SET Regn_No=?,SickleaveDate=?,VisitID=?,diagnosis=?,StartDate=?,FromDate=?,Signatory=?,Days=? where id =" + req.body.ID;
        } else {
            var query = "INSERT INTO Sickleave (Regn_No,SickleaveDate,VisitID,diagnosis,StartDate,FromDate,Signatory,Days) VALUES (?,?,?,?,?,?,?,?)"
        }
        var table = [req.body.Regn_No, formatter.format(new Date()), req.body.VisitID, req.body.diagnosis, formatter.format(new Date(req.body.StartDate)), formatter.format(new Date(req.body.FromDate)), req.body.Signatory, req.body.Days];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var sickleaveid = result.insertId
                if (method === 'post') {
                    var query2 = "INSERT INTO visitpages (VisitID, PageDate, PageTitle, TemplateType, Operator) VALUES (?,?,?,?,?)";
                    var table2 = [req.body.VisitID, formatter.format(new Date()), 'Sick Leave', 113, req.body.Signatory];
                    query2 = mysql.format(query2, table2);
                    connection.query(query2, function (err, result, fields) {
                        if (err) {
                            res.json({ "Error": true, "Message": err.sqlMessage });
                        } else {
                            // res.json({ "Error": false, "Message": result.insertId});
                            var query3 = "UPDATE Sickleave set PageID =" + result.insertId + " where id =" + sickleaveid;
                            connection.query(query3, function (err, result, fields) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                    res.json({ "Error": false, "Message": 'Sick Leave Saved' });
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    allergyseverity: function (connection, req, res) {
        if (req.query.allergyseverity) {
            var query = "select Description FROM allergyseverity where Code='" + req.query.allergyseverity + "'";
        } else {
            var query = "SELECT Description FROM allergyseverity ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'AllergySeverity');

    },
    complaintsMore: function (connection, req, res) {
        var query = "select * FROM complaintsmore where VisitID='" + req.query.visitid + "'";
        this.executeQuery(connection, res, query, 'Success', 'ComplaintsMore');

    },
    savecomplaintsMore(connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE ComplaintsMore SET Regn_No=?,ComplaintsMoreDate=?,VisitID=?,Complaints=?,Oper=? where id =" + req.body.ID;
        } else {
            var query = "INSERT INTO ComplaintsMore (Regn_No,ComplaintsMoreDate,VisitID,Complaints,Oper) VALUES (?,?,?,?,?)"
        }
        var table = [req.body.Regn_No, formatter.format(new Date()), req.body.VisitID,req.body.Complaints,req.body.Oper];
        query = mysql.format(query, table);
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": 'Complaints Saved' });
            }
        });
    },
    deleteComplaintsMore: function (connection, req, res) {
        var query = "DELETE FROM ComplaintsMore WHERE ID=" + req.query.ID +" and visitId ="+req.query.visitID;
        this.executeQuery(connection, res, query, 'Complaints Deleted');
    },

    visitprepared: function (connection, req, res) {
        var query = "UPDATE Visits SET PreparedBy = '" + req.body.preparedby + "' WHERE id='" + req.body.visitid + "'";
        this.executeQuery(connection, res, query, 'PreparedBy updated');
    },
    printallvisits: function (connection, req, res) {
        var self = this;
        var query ="SELECT visits.ID,visits.VisitDate,visits.Regn_No,visits.Consultant,visits.DoctorNotes,pain.PainDate,pain.Intensity,pain.Location,pain.PainCharacter,pain.Duration,pain.Frequency,pain.Medication,pain.Aggravation,pain.Relief,pain.Remarks as painRemarks,complaints.ComplaintsDate,complaints.Complaints,complaints.Temp,complaints.Pulse,complaints.BP,complaints.Height,complaints.Weight,complaints.SPO2,complaints.Resp,diagnosis.DiagnosisDate,diagnosis.Cancelled,GROUP_CONCAT(CONCAT(ICD,' - ', diagnosis,'<br>') SEPARATOR ' ' ) AS DiagnosisAll FROM visits left JOIN complaints ON visits.ID = complaints.VisitID  left JOIN diagnosis ON visits.ID =diagnosis.VisitID LEFT JOIN pain ON visits.ID = pain.VisitID  WHERE visits.Regn_No='" + req.query.Regn_no + "' GROUP BY visits.id ORDER BY visitdate desc"
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                let visits = new Array();
                visits = rows;
                var query2 = "select * from pasthistory where  regn_no =" + req.query.Regn_no + " and VisitId <= " + visits[0].ID + " and VisitId in (select max(VisitId) from pasthistory where  regn_no =" + req.query.Regn_no + " and VisitId <= " + visits[0].ID + ")"
                connection.query(query2, function (err, pastrows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        var query3 = "SELECT * FROM gynae_gynae1 WHERE pregnant = 1 and Edd >= CURDATE() AND Regn_No= '" + req.query.Regn_no + "' ORDER BY PregnancyNo desc ";
                        connection.query(query3, function (err, gyanerows) {
                         if (err) {
                             res.json({ "Error": true, "Message": err.sqlMessage });
                         } else {
                            res.json({ "Error": false,  'Visits': visits ,'Past':pastrows,'PresentPreg':gyanerows});
                         }
                        });
                    }
                });
            }
        });
    },

    openvisit: function (connection, req, res) {
        var query = "UPDATE visits SET unlocked=1 WHERE ID=" +req.body.visitID;
        query += ";INSERT INTO visitslog (VisitsLogDate, VisitID, PageID, Description, Operator,PageTitle) VALUES (?,?,?,?,?,?)";
        var table = [formatter.format(new Date()), req.body.visitID, 0, 'Visit Unlocked('+ req.body.reason +')'  , req.body.Operator, 'Consultation'];
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, 'Visit Unlocked');
    },

    allergyseverity: function (connection, req, res) {
        if (req.query.desc) {
            var query = "SELECT * FROM allergyseverity where Code='" + req.query.desc + "'";
        } else {
            var query = "SELECT * FROM allergyseverity ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'AllergySeverity');
    },
    allergytype: function (connection, req, res) {
        if (req.query.desc) {
            var query = "SELECT * FROM allergytype where Code='" + req.query.desc + "'";
        } else {
            var query = "SELECT * FROM allergytype ORDER BY Description";
        }
        this.executeQuery(connection, res, query, 'Success', 'AllergyType');
    },

    allergy: function (connection, req, res) {
        var query = "SELECT * FROM patallergies WHERE Regn_No='" + req.query.regn_no + "' order by PatallergiesDate desc"
        this.executeQuery(connection, res, query, 'Success', 'PatAllergies');
    },
    saveAllergy: function (connection, req, res, method) {
        var self = this;
        if (method === 'post') {
            var query = "INSERT INTO patallergies (Regn_No, Operator,PatallergiesDate,AllergyType,Description,AllergySeverity) VALUES (?,?,?,?,?,?)";
        } else {
            var query = "UPDATE patallergies SET Regn_No=?, Operator=?,PatallergiesDate=?,AllergyType=?,Description=?,AllergySeverity=? WHERE ID=?";
        }
        var table = [req.body.Regn_No, req.body.Operator, formatter.format(new Date()), req.body.AllergyType, req.body.Description, req.body.AllergySeverity, req.body.ID];
        query = mysql.format(query, table);
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": "Error executing MySQL query" });
            } else {
                self.deleteNotAvailable(connection,req, res,  req.body.Regn_No);
                if (req.body.AllergyType != 'Not Available') {
                    var query2 = "update patmas set Allergies ='Allergies Present' where Regn_No=" + req.body.Regn_No + " and  (LOWER(Allergies) IN ('nkda','nka','none'))";
                    connection.query(query2, function (err, result, fields) {
                        if (err) {
                            console.log(err)
                        } else {
                            console.log('Patmas Allergies Updated');
                        }
                    });
                }
            }
        });

    },

    deleteNotAvailable(connection,req, res, regno) {
        var query3 = "SELECT * FROM patallergies WHERE Regn_No='" + regno + "' and AllergyType <> 'Not Available'";
        connection.query(query3, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (rows.length) {
                    var query4 = "DELETE FROM patallergies WHERE Regn_No='" + regno + "' and AllergyType = 'Not Available'";
                    connection.query(query4, function (err, rows) {
                        if (err) {
                            res.json({ "Error": true, "Message": "Error executing MySQL query" });
                        } else {
                            res.json({ "Error": false, "Message": "Allergies Added or Updated" });
                        }
                    });
                } else {
                    res.json({ "Error": false, "Message": "Allergies Added or Updated" });
                }      
            }
        });
    },
    delallergy: function (connection, req, res) {
        var query = "DELETE FROM patallergies WHERE id=" + req.query.id;
        this.executeQuery(connection, res, query, 'Allergy Deleted');
    },
    diagcheck: function (connection, req, res) {
        var query = "SELECT * FROM diagnosis JOIN Visits ON diagnosis.VisitID = Visits.ID WHERE visits.AppointmentID ="+ req.query.ID +" AND diagnosis.DiagType='Primary' AND (diagnosis.Cancelled IS NULL OR diagnosis.Cancelled =0 )"
        this.executeQuery(connection, res, query, 'Success', 'Diagnosis');
    },
    mchatmaster: function (connection, req, res) {
        var query = "SELECT * FROM mchatmaster order by id";
        this.executeQuery(connection, res, query, 'Success', 'MchatMaster');
    },
    savemchatdata: function (connection, req, res) {
       var query = "DELETE FROM mchatdata WHERE PageID=" + req.body[0].PageID;
        connection.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                var query2 = "INSERT INTO mchatdata (Regn_No,MasterID,Score,VisitID,PageID,MchatdataDate, Operator) VALUES ?";
                var records = new Array();
                for (var i = 0; i < req.body.length; i++) {
                    var rec = new Array();
                    req.body[i].MchatdataDate = formatter.format(new Date(req.body[i].MchatdataDate));
                    rec.push(req.body[i].Regn_No);
                    rec.push(req.body[i].ID);
                    rec.push(req.body[i].Score);
                    rec.push(req.body[i].VisitID);
                    rec.push(req.body[i].PageID);
                    rec.push(req.body[i].MchatdataDate);
                    rec.push(req.body[i].Operator);
                    records.push(rec);
                }
                connection.query(query2, [records], function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {

                        res.json({ "Error": false, "Message": 'M-Chat Saved', '': rows });

                    }
                });
            }
        });
    },
    patmchatdata: function (connection, req, res) {
        var query = "SELECT * FROM mchatdata WHERE  PageID= '" + req.query.pageid + "' and Regn_No='"+req.query.regnno+"' order by masterid"
        this.executeQuery(connection, res, query, 'Success', 'PatMchatData');
    },
    patpasthistory: function (connection, req, res) {
        var query = "select * from patpasthistory where  regn_no =" + req.query.regn_no 
        this.executeQuery(connection, res, query, 'Success', 'patpasthistory');
    },
    patpasthistorymaris: function (connection, req, res) {
        var query = "select * from patpasthistorymaris where  regn_no =" + req.query.regn_no 
        this.executeQuery(connection, res, query, 'Success', 'patpasthistory');
    },
    getCovidQuestionaire: function (connection, req, res) {
        var query = "SELECT * FROM covidquestionaire WHERE PageID=" + req.query.pageID;
        this.executeQuery(connection, res, query, 'Success', 'CovidQuestionaire');
    },
    patpasthistorymarissave: function (connection, req, res, method) {
        if (method === 'put') {
            var query = "UPDATE patpasthistorymaris SET Regn_No=?,PatpasthistoryDate=?,Operator=?,SkinHeal=?,SunExposure=?,LabialHerpes=?,OldTattoo=?,FacialTreatment=?,Smoker=?,RecentBotox=?,Other=?,Drugs=?,Food=?,Medicines=?,OthersAllergy=?,Anticoagulant=?,BloodThinning=?,Roaccutane=?,Hormone=?,OthersMedication=?,Diabetes=?,HeartProblem=?,HormonalProblem=?,Pregnant=?,Breastfeeding=?,FacialFillers=?,HighBP=?,Hepatitis=?,Dermatitis=?,Pacemaker=?,Cancer=?,Claustrophobia=?,ActiveInfection=?,Lupus=?,Keloid=?,OthersMedical=? WHERE ID=?";
            var table = [req.body.Regn_No,formatter.format(new Date(req.body.PatpasthistoryDate)) , req.body.Operator, req.body.SkinHeal, req.body.SunExposure, req.body.LabialHerpes, req.body.OldTattoo, req.body.FacialTreatment, req.body.Smoker, req.body.RecentBotox, req.body.Other, req.body.Drugs, req.body.Food, req.body.Medicines, req.body.OthersAllergy, req.body.Anticoagulant, req.body.BloodThinning, req.body.Roaccutane, req.body.Hormone, req.body.OthersMedication, req.body.Diabetes, req.body.HeartProblem, req.body.HormonalProblem, req.body.Pregnant, req.body.Breastfeeding, req.body.FacialFillers, req.body.HighBP, req.body.Hepatitis, req.body.Dermatitis, req.body.Pacemaker, req.body.Cancer, req.body.Claustrophobia, req.body.ActiveInfection, req.body.Lupus, req.body.Keloid, req.body.OthersMedical, req.body.ID];
            var message = "Clinical History Updated"
        } else {
            var query = "INSERT INTO patpasthistorymaris (Regn_No,PatpasthistoryDate,Operator,SkinHeal,SunExposure,LabialHerpes,OldTattoo,FacialTreatment,Smoker,RecentBotox,Other,Drugs,Food,Medicines,OthersAllergy,Anticoagulant,BloodThinning,Roaccutane,Hormone,OthersMedication,Diabetes,HeartProblem,HormonalProblem,Pregnant,Breastfeeding,FacialFillers,HighBP,Hepatitis,Dermatitis,Pacemaker,Cancer,Claustrophobia,ActiveInfection,Lupus,Keloid,OthersMedical)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            var table = [req.body.Regn_No, formatter.format(new Date(req.body.PatpasthistoryDate)) , req.body.Operator, req.body.SkinHeal, req.body.SunExposure, req.body.LabialHerpes, req.body.OldTattoo, req.body.FacialTreatment, req.body.Smoker, req.body.RecentBotox, req.body.Other, req.body.Drugs, req.body.Food, req.body.Medicines, req.body.OthersAllergy, req.body.Anticoagulant, req.body.BloodThinning, req.body.Roaccutane, req.body.Hormone, req.body.OthersMedication, req.body.Diabetes, req.body.HeartProblem, req.body.HormonalProblem, req.body.Pregnant, req.body.Breastfeeding, req.body.FacialFillers, req.body.HighBP, req.body.Hepatitis, req.body.Dermatitis, req.body.Pacemaker, req.body.Cancer, req.body.Claustrophobia, req.body.ActiveInfection, req.body.Lupus, req.body.Keloid, req.body.OthersMedical ];
            var message = "Clinical History Added"
        }
        query = mysql.format(query, table);
        this.executeQuery(connection, res, query, message);
    },
    
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });

            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }

}

